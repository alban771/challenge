## Advent Of Code 2019

Advent of Code is a very exciting set of programmind puzzles, released each year in December, with a ranking of the top 100 developers to solve each problem. 
I did not manage to appear in this classification, but plunging into a beautiful imaginary voyage in space offers thoughtfull and stimulating questions.

Embarque yourself : https://adventofcode.com/


## The code

last problem solved : [day-17](day-17)
in progress: [day-18](day-18)[day-22](day-22)

## Puzzle Index 

[day-01](day-01) : Simple serial math. 

[day-02](day-02) : Reverse a black box function.
+ First IntCode compiler increment, instructions terminate, + and \*.
+ Parallel computation with STL execution policy.
    
[day-03](day-03) : Find the closest intersection of wires.

[day-04](day-04) : Count integers with one pair of identical adjacent digits and otherwise crescent digits.  

[day-05](day-05) : Implement and test the IntCode compiler.
+ Unit test on instructions print, scan, goto_if_equal, goto_if_null.

[day-06](day-06) : Evaluate distance in an orbit map.

[day-07](day-07) : Configure a chain of 5 amplificators in open or in closed loop. 
+ IO synchronization with STL mutex, condition variable.

[day-08](day-08) : Compose an image from fragments.

[day-09](day-09) : Final implementation of the IntCode.

[day-10](day-10) : Find which position has the most objects in sight, on a 2D grid.  
+ STL complex for geometry and trigonometry 

[day-11](day-11) : Follow the pencil until it finishes the picture. 

[day-12](day-12) : In a 4 bodies system, submitted to elastic interactions, evaluate the time to return to initial position.

[day-13](day-13) : Pong game.
+ Display and game thread synchronization.
+ TUI interface.

[day-14](day-14) : Reduce a list of chemical reactions. 

[day-15](day-15) : In a 2D maze, find the shortest path to the exit.
+ A* algorithm
+ TUI interface

[day-16](day-16) : Process a long incomming signal.
+ Analogy with fourrier transformation

[day-17](day-17) : Explore the entire maze with only 3 commands. 
+ String compression
+ Visitor design pattern

[day-18](day-18) : Find the shortest to get all keys
+ Optimization

-----

-----

-----

[day-22](day-22) : Shuffle an overwhelming deck of cards and find the card in position 2020. 
+ Biginteger lib
+ Z/nZ Arithmetic

-----

-----

-----







