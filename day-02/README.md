## Day-02

  This is the first [IntCode](../lib/IntCode) compiler iteration.


### Silver

  A programme for the IntCode compiler contains a numerical function taking two integers as input.
  It must run with the fixed input: 12, 2 to succeed silver grade.

### Gold


  A programme for the IntCode compiler contains a numerical function taking two integers as input.
  Specific value of the function on intervalle [0,100]x[0,100] must be found.
  Crible is parallelised.

__STL parallel execution policy__
    <br/>

  On every parallelizable algorithms of std library, like find_if in this exemple, the execution policy refers to the synchronization of every task.

  + seq : single threaded computation, althought blocks are not necessarily computed in order. 
  + unseq : single threaded computation, and operations can operate across several blocks ( vectorization ). 
  + par : threads are implicitly created ( paralellization )to compute blocks in parallel, not ordering blocks within a thread.
  + par_unseq : parallelization occurs, and tasks may migrate from one thread to another, and operations are vectorized. 

On my computer, the command :
<code>sh -c 'time ( i=0 ; while (( i++ < 10 )); do ./day-2.gld >/dev/null ; done; )'</code> 
results in the following benchmark :

| policy | real (ms) | sys (ms) | user (ms) |  
|:----|:----:|:----:|:----:|
| seq | 314 | 241 | 114 |
| unseq | 315 | 238 | 118 |
| par | 120 | 433 | 188 |
| par_unseq | 115 | 423 | 183 |
  
  considering real time ellapsed, policy rank are : 
  
| # | policy | real time gain (%) |
|:----:|:----|:----:|
| 1 | par_unseq | 63 |
| 2 | par | 61 | 
| 3-4 | seq == unseq | 0 |  

