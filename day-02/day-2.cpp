#include <iostream>
#include <fstream>
#include "IntCode.h"

#ifdef GOLD 

#include <array>
#include <algorithm>
#include <execution>

#define MAX_NOON        (unit) 100
#define MAX_VERB        (unit) 100
#define N_THREAD        20            // number of threads 
#define EXPECTED_VALUE  19690720

#endif



using namespace std;


typedef long long unit; 
typedef array<unit,3> crible_elt;


// IntCode by copy, reset the initial state
static constexpr auto f_crible = [] (const unit noon, const unit verb, IntCode<unit> cp) -> crible_elt { 
  cp.set(1, noon);
  cp.set(2, verb);
  cp.compute();
  cp.join();
  return {cp.get_t_value(), noon, verb};
};


#ifdef GOLD

template<int expected>
constexpr bool test(const crible_elt& c) { 
  return c[0] == expected; 
};


// 2D indice (i,j) in seq 
//   5
//   2 4
//   0 1 3 ...
struct coord {
  unit noon, verb;

  coord( unit n, unit v ):noon(n),verb(v){}
  coord(unit o) {
    unit n = 0;
    // invariant:
    // n diagonals are filled before o
    //while ( ++n * (n+1)  < 2 * ( o+1 ) );  // warning : undefined sequence order
    while ( ++n, n * (n+1)  < 2 * ( o+1 ) );  // o=0 n=1; o=1 n=2; o=2 n=2;
    unit i = o - n * ( n-1 ) / 2;     // o=0 i=0; o=1 i=0; o=2 i=1;
    noon = i;
    verb = n-i-1;
  }; 

  coord &operator++() {
    if ( verb ){
      verb--;
      noon++;
    } else {
      verb = noon+1;
      noon = 0;
    }
    return *this;
  }
  
  friend
  ostream &operator<<( ostream & os, const coord & c ){
    return os << c.noon * MAX_VERB + c.verb;
  }
};
 

#endif


int main(){

  IntCode<unit> cp("day-2.input"); 

#ifndef GOLD

  cout << "indicate noon and verb" << endl;
  unit noon, verb;
  cin >> noon >> verb;
  cout << "Solution slv:" << f_crible(noon, verb, cp)[0] << endl;

#else

  array<crible_elt,N_THREAD> a;
  atomic<int> i=0;

  // range of indices to treat in the ii th thread
  constexpr auto range_th = [] (int ii, unit tot) -> array<unit,2> {
    return { tot * ii / N_THREAD, tot * ( ii+1 ) / N_THREAD };
  };

  // ref execution policy
  // https://en.cppreference.com/w/cpp/algorithm/execution_policy_tag_t
  // here : seq (default) < unseq < par < par_unseq (in execution time)
  auto s = find_if( execution::seq, a.begin(), a.end(), [&] ( crible_elt& c ) -> bool { 
      const int ii = i++;
      auto r_noon = range_th(ii, MAX_NOON * MAX_VERB);
      coord p(r_noon[0]);
      bool b = false;

      do { 
        c = f_crible( p.noon, p.verb, cp); 
        b = test<EXPECTED_VALUE>(c);
        ++p;
      } while(  !b && ++r_noon[0] < r_noon[1] );
    
      return b; 
    });

  cout << "Solution gld:" << coord(s->at(1),s->at(2)) << endl; 

#endif

  return 0;
}
