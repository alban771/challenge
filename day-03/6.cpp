#include <iostream>
#include <algorithm>
#include <vector>
#include <cstring>
#include <cassert>
#include <cstdlib>
#include <iterator>


using namespace std;


struct Segment {
  bool h;
  vector<int>::iterator x, y, y1;
  int l;

  Segment( vector<int>* H, vector<int>* V, bool h, pair<int, int> p ):h(h),l(0){

    cerr << "find segment in position x:" << p.first << " y:" << p.second << endl;

    x = ( h ? H->begin() : V->begin());
    y = h ? V->begin() : H->begin();
    y1 = y;
    y1++;

    cerr << "x:" << *x << " y:" << *y << " y1:" << *y1 << " l:" << l << endl;

    while( 
        y1 != V->end() && 
        y1 != H->end() && 
        ( 
         ( !h && // seg vertical, x = p.first, p.second <: [y,y1]  
           !( *x == p.first && ( *y-p.second ) * ( *y1-p.second ) <= 0 ))
         || ( h && // seg horizontal, x = p.second, p.first <: [y,y1]  
           !( *x == p.second && ( *y-p.first ) * ( *y1-p.first ) <= 0 ))
        )
        ){
     h = !h;
     l += abs( *y1-*y );
     y = x;
     x = y1;
     y1 = y;
     y1++;

      cerr << "x:" << *x << " y:" << *y << " y1:" << *y1 << " l:" << l << endl;
    }

    if ( y1 == V->end() || y1 == H->end()){
      cerr << "cannot find intersection." << endl;
      l = 0;
    } else {
      l += abs(h ?  p.first - *y : p.second - *y); // if seg is horizontal, distance on last segment is on axe x. 
      cerr << "seg finished with l:" << l << endl;
    }

  }
};



int main(){
  
  vector<vector<int>*> TH, TV; // TH holds Y coordinate of horizontal segments, 
                              // symetric for TV 
  vector<bool> BH;
  string s; 
  char* d;

  while(  getline(cin, s) ){

    vector<int> *h=new vector<int>({0}), *v=new vector<int>({0});

    d = strtok(const_cast<char*>( s.c_str() ), ",");
    BH.push_back( *d == 'R' || *d == 'L' );
    while (d != NULL){
      switch (*d){
        case 'R':
          v->push_back(  v->back() + atoi(++d) );
          break;
        case 'L':
          v->push_back(  v->back() - atoi(++d));
          break;
        case 'U':
          h->push_back( h->back() + atoi(++d));
          break;
        case 'D':
          h->push_back( h->back() - atoi(++d));
          break;
        default:
          cerr << "Wrong format." << *d << endl;
      } 
      d = strtok( NULL, ",");
    }

    TH.push_back(h);
    TV.push_back(v);
  }


  //cerr << "TH[0]:" << TH[0]->size() << endl;
  //cerr << "TV[0]:" << TV[0]->size() << endl;
  //copy( TV[0]->begin(), TV[0]->end(), ostream_iterator<int>(cerr, " "));

  vector<pair<int, int>> I;

  /*
   * test every combination of the TH.size() trajectories  
   */
  for ( int n = 0; n < TH.size(); n ++){
    for ( auto m = 0; m < TH.size(); m++){
      if ( n == m ){
        continue;
      }

      //cerr << endl;
      //cerr << endl;
      //cerr << "n:" << n << " m:" << m << endl;
      //cerr << "BH[n] :: " << BH[n] << endl;
      //cerr << "TH[n] :: [ ";   
      //copy( TH[n]->begin(), TH[n]->end(), ostream_iterator<int>(cerr, ", "));
      //cerr << "] " << endl << "TV[n] :: [ "; 
      //copy( TV[n]->begin(), TV[n]->end(), ostream_iterator<int>(cerr, ", "));
      //cerr << "] " << endl;
      //cerr << "BH[m] :: " << BH[m] << endl << "TH[m] :: [ ";
      //copy( TH[m]->begin(), TH[m]->end(), ostream_iterator<int>(cerr, ", "));
      //cerr << "] " << endl << "TV[m] :: [ "; 
      //copy( TV[m]->begin(), TV[m]->end(), ostream_iterator<int>(cerr, ", "));
      //cerr << "] " << endl;
      /*
       * inv: 
       *    for every segment horizontal of trajectory n,
       *    [iv, iv1], i are the coordinates of this segment.
       */
      vector<int>::iterator i = TH[n]->begin(), iv=TV[n]->begin(), iv1=iv;
      iv1++;
      if ( !BH[n] ){ i++; }
      do {
        /*
         * inv: 
         *    for every segment vertical of trajectory m,
         *    jv, [j, j1] are the coordinates of this segment.
         */     
        vector<int>::iterator j = TH[m]->begin(), j1=j, jv=TV[m]->begin();
        j1++;
        if ( BH[m] ){ jv++; }
        do {
          /*
           * test if segments are crossing.
           */
          if ( ( *j1 - *i ) * ( *j - *i) <= 0  ){
            if (( *iv1 - *jv ) * ( *iv - *jv ) <= 0 ){
              if ( *jv != 0 || *i != 0 ){
                I.push_back(make_pair(*jv, *i));
                cerr << "( x:" << *jv << ", y:" << *i << " ) ";
              }
            }
          }
        } while( j++, jv++, ++j1 != TH[m]->end() );
      } while ( i++, iv++, ++iv1 != TV[n]->end() );
      cerr << endl;
    }
  }


  cerr << "I:" << I.size() << endl;
  vector<int> a = vector<int>(I.size()) ;
  transform( I.begin(), I.end(), a.begin(), [TV,TH,BH] ( pair<int,int> p) {

      int l = 0;
      for ( int i = 0; i < TH.size(); i++ ){
        l += Segment(TH[i],TV[i],BH[i],p).l;
      }
      return l;
      });

  cerr << endl << "a[I] :";
  copy(a.begin(), a.end(), ostream_iterator<int>(cerr, " "));
  cerr << endl;
  
  cout << *min_element(a.begin(), a.end()) << endl;
}
