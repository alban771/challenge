#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <cmath>



// for this puzzle input are these 2 integers 
#define BOUNDARY_INF 248345
#define BOUNDARY_SUP 746315



using namespace std;


const int r1 = BOUNDARY_INF, r2 = BOUNDARY_SUP;


struct Comb{

  int a, X, n; 

  Comb(const int r):a(r),X(0),n(0){ 

    cerr << "create " << endl;
    cerr << "a:" << a << " X:" << X << " n:" << n << endl;

    findXn();
  }

  void findXn(){
    int i1;
    for( int i=0, b=a; b>0; i++, b/=10 ){
      if ( X <= b%10 ){
        X = b%10; 
        i1 = i;
      }
    }
    n+=++i1;
    a /= pow(10,i1);

    cerr << "a:" << a << " X:" << X << " n:" << n << endl;
  }

  bool next(){
    if ( a  > 0 ){
      a++; 
      X=0;
      findXn();
      return true;
    }
    return false;
  }

  int cardinal(bool strict){

    return cardinal(X, n, strict);
  }

  //// ! \\\\\\ wrong for strict case.
  int cardinal( int XX, int nn, bool strict) {

    if ( nn== 0 || nn==1 && strict ) {
      return 1;
    }

    int res=0;
    for (int i = strict ? XX+1: XX; i < 10; i++){
      res += cardinal(i, nn-1, strict);
    }

    if ( XX == X && nn == n){
      cerr << "cardinal:" << res << " for X:" << XX << " n:" << nn << " strict:" << strict << endl;
    }
    return res;
  }


  int correction(){
    int s=0;
    s-=cardinal(true);

    cerr << " correction:" << s << endl;
    return s;
  }
};


int main(){


  cerr << "go" << endl;

  int s=0;
  Comb c1(BOUNDARY_INF), c2(BOUNDARY_SUP);

  cerr << endl;
  do { 

    cerr << "  s:" << s << endl;
    s+=c1.cardinal(false); 
    cerr << "  s:" << s << endl;
    s+=c1.correction(); 

  } while ( c1.next() );
  

  cerr << endl;
  do { 

    cerr << "  s:" << s << endl;
    s-=c2.cardinal(false); 
    cerr << "  s:" << s << endl;
    s-=c2.correction(); 

  } while ( c2.next() );
  
  cout << s << endl;
}
