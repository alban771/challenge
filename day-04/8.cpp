#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;


const int r1 = 248345, r2 = 746315;


struct Comb{

  int a, X, n; 

  Comb(const int r):a(r),X(0),n(0){ 

    cerr << "create " << endl;
    cerr << "a:" << a << " X:" << X << " n:" << n << endl;
    findXn();
  }

  void findXn(){
    int i1;
    for( int i=0, b=a; b>0; i++, b/=10 ){
      if ( X <= b%10 ){
        X = b%10; 
        i1 = i;
      }
    }
    n+=++i1;
    a /= pow(10,i1);

    cerr << "a:" << a << " X:" << X << " n:" << n << endl;
  }

  bool next(){
    if ( a  > 0 ){
      a++; 
      X=0;
      findXn();
      return true;
    }
    return false;
  }

  int croissants(bool strict){

    return croissants(X, n, strict);
  }

  int croissants( int XX, int nn, bool strict){

    int s =  rec_croissants( strict? XX-1:XX, nn, strict);
    cerr << "combinaison croissantes: "<< s << " X:" << XX << " n:" << nn << " strict:" << strict << endl;
    return s;
  }

  int rec_croissants ( int XX, int nn, bool strict){

    if ( nn < 0 ){
      return 0;
    }

    if ( nn == 0 ) {
      return 1;
    }

    int res=0;
    for (int i = strict ? XX+1: XX; i < 10; i++){
      res += rec_croissants(i, nn-1, strict);
    }

    return res;
  }

  int comb(int nn, int k){ // cherche k parmis n
    
    if ( k > nn ){
      return 0;
    }

    int s=1;
    for ( int i = 0; i < k; i++){
        s*=(nn-i);
    }

    int kk = k;
    while( s/=k, --k>0 ){};

    cerr << "comb k:" << kk<< " parmis:" << nn  << " s:" << s << endl;
    return s;
  }

  bool double_in_a(){
    int b = a;
    while ( b > 0 ){
      if ( !(b % 100 % 11) ){
        return true;
      }
      b/=10;
    }
    return false;
  }


  int cardinal(){
    int s = 0;

    cerr << " get cardinal a:" << a << " X:" << X << " n:" << n << endl;

    if ( double_in_a() ){

      s+= croissants(false);
      cerr << "prefix has double. s with full strict croissant: " << s << endl;

    } else {

      if ( n>2 ){
        s += croissants( X, n-1, true) * comb(n-1,1); // cas 1 double
        cerr << "s with 1 double s:" << s << endl;
      }

      if ( n>3 ){
      s += croissants( X, n-2, true) * comb(n-2,2); // cas 2 doubles
      cerr << "s with 2 double s:" << s << endl;
      }

      if ( n>4){
      s += croissants( X, n-3, true) *(  comb(n-3,3) + 2*comb(n-3,2)); // cas 1 double & 1 triple, ou 3 doubles
      cerr << "s with 3 doubles ou 1 triple, 1 double s:" << s << endl;
      }

      if ( n>5){
      s += croissants( X, n-4, true) * 2 * comb(n-4,2); // cas 1 double & 1 quadruple
      cerr << "s with 1 quadruple, 1 double s:" << s << endl;
      }
      // autres cas non realisables avec une range < 10^7.
    } 
    return s;
  }




};


int main(){


  cerr << "go" << endl;

  int s=0;
  Comb c1(r1), c2(r2);

  cerr << endl;
  do { 
    s+=c1.cardinal(); 
    cerr << "  s:" << s << endl;

  } while ( c1.next() );
  

  cerr << endl;
  do { 
    s-=c2.cardinal(); 
    cerr << "  s:" << s << endl;

  } while ( c2.next() );
  
  cout << s << endl;
}
