#include <iostream>
#include "IntCode.h"

#ifdef GOLD
#define INPUT 5
#else
#define INPUT 1
#endif

using namespace std;

int main(){

  IntCode<int> TEST("day-05.input");

  TEST.compute();

  TEST.in->put(INPUT);

  int k;
  while ( k = TEST.out->get(), !k ) {};

  cout << k;
}
