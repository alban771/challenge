#include <iostream>
#include <map>
#include <vector>
#include <algorithm>



using namespace std;


struct Orbit {
  Orbit *c;
  bool end;
  Orbit():end(true){}
  Orbit( int diff, Orbit* q):end(false){
    c = q;
  }

  int chain(){
    cerr << "c";
    return end ? 0 : 1 + c->chain();
  }

  void addqueue(Orbit* q){
    end = false;
    c = q;
  }
  
};



int main() {


  map<string,Orbit*> map;
  Orbit *oc;

  string C,A;
  int p;
  while ( cin >> A ){

    p = A.find(')');
    C = A.substr(0,p);
    A = A.substr(p+1);

    cerr << " new line C:" << C << " A:" << A << endl ;

    auto ic = map.find(C);
    if ( ic == map.end() ){
      oc = new Orbit(); 
      map.insert( make_pair( C , oc) );
    } else {
      oc = ic->second; 
    }

    ic = map.find(A);
    if ( ic == map.end()){
      Orbit *oa = new Orbit();
      oa->addqueue(oc);
      map.insert( make_pair(A, oa) );
    } else {
      ic->second->addqueue( oc ); 
    }
  }

  int a=0;
  for_each( map.begin(), map.end(), [&a] ( pair<string, Orbit*> pp) {

    int cha = pp.second->chain();
    a+=cha;
    cerr << pp.first << " chain:" << cha << " a:" << a << endl; 
      });

  cout << a << endl;

}
