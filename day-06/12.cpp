#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
#include <iterator>



using namespace std;


struct Orbit {
  Orbit *c;
  string name;
  bool end;
  Orbit( string n):name(n),end(true){}

  int chain(){
    return end ? 0 : 1 + c->chain();
  }

  vector<string>* chain_vect(){
    if ( end ){
      return new vector<string>({name});
    }
    vector<string> *v = c->chain_vect();
    v->push_back(name);
    return v;
  }

  void addqueue(Orbit* q){
    end = false;
    c = q;
  }
  
};



int main() {


  map<string,Orbit*> map;
  Orbit *oc;

  string C,A;
  int p;
  while ( cin >> A ){

    p = A.find(')');
    C = A.substr(0,p);
    A = A.substr(p+1);

    cerr << " new line C:" << C << " A:" << A << endl ;

    auto ic = map.find(C);
    if ( ic == map.end() ){
      oc = new Orbit(C); 
      map.insert( make_pair( C , oc) );
    } else {
      oc = ic->second; 
    }

    ic = map.find(A);
    if ( ic == map.end()){
      Orbit *oa = new Orbit(A);
      oa->addqueue(oc);
      map.insert( make_pair(A, oa) );
    } else {
      ic->second->addqueue( oc ); 
    }
  }

  int a=0;
  
  vector<string> *you_obts = map.find("YOU")->second->chain_vect();
  vector<string> *san_obts = map.find("SAN")->second->chain_vect();

  auto you_i = you_obts->rbegin();
  auto san_i = san_obts->begin();
  while( 
      san_i = find( san_obts->begin(), san_obts->end(), *you_i), 
      san_i == san_obts->end() ){
    you_i++;
  } 

  cerr << "orbits from YOU" << endl;
  copy( you_obts->rbegin(), you_i + 1, ostream_iterator<string>(cerr, " ") );
  cerr << endl << "orbits to SAN" << endl;
  copy( san_i, san_obts->end(), ostream_iterator<string>(cerr, " ") );
  cerr << endl;
  a = ( you_i - you_obts->rbegin() -1 ) + ( san_obts->end() - san_i -2 ); 
  cout << a << endl;

}
