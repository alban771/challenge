#include <iostream>
#include <cstdio>
#include <iterator>
#include <algorithm>
#include <vector>
#include "IntCode.h"




using namespace std;

struct Perm {

  const vector<int>::iterator pos;
  int i; 
  Perm *p;
  vector<int> *v;



  Perm( vector<int> *v, vector<int>::iterator pp ):v(v),pos(pp){
    i = (v->end() - pos) - 1;
    if ( i > 0 ){
      p = new Perm(v, ++pp);
    }
  }

  bool next_perm(){
    
    
    // base
    if ( v->end() - pos == 1 ){
      return false;
    }

    // case every permutations were performed on this level. 
    if ( i == 0 ){ 
      return p->next_perm();
    }

    // case every sub-permutations were performed. action on this level.
    if ( !( p->next_perm() ) ){
      
      // invariant:   
      //
      // before any permutation:   [ ... x1, x2, ... xn-1, xn ]
      //                                 ^ pos
      // if n is even, xn takes successively every values in: [x2, .. xn] 
      // when applying recursive algorithm. 
      // 
      // if n is odd, x(n+1/2) is invariant, and xn takes values in:
      // [x2, .. x(n-1/2),x(n+3)/2, .. xn]
      //
      // in this case we inject a new numbers on invariant point at last cycle. 
      //
      if ( i == 1 && !(( v->end() - pos ) % 2) ){
        vector<int>::iterator pos2 = pos; 
        iter_swap( pos, pos2 + (( v->end() - pos ) / 2 ) );
      } else {
        iter_swap( pos, --v->end() );
      }
       
      cerr << "new perm : [";
      copy( v->begin(), v->end(), ostream_iterator<int>(cerr, ","));
      cerr << "]" << endl;

      vector<int>::iterator pp = pos; 
      p = new Perm(v, ++pp );
      i--;
      
    }

    return true;

  }

};



int compute_amp( IntCode *cp, vector<int> *v ){
  int in[]={0,0};
  for_each( v->begin(), v->end(), [ cp, &in] (int a) {
      in[0] = a;
      in[1] = cp->compute( in );
    });
  return in[1];
}

int main(){


  IntCode *cp = new IntCode();
  vector<int> v(5), r(120);

  cin >> cp;
  int a=0;
  generate( v.begin(), v.end(),  [&a] ()-> int { return a++; });

  Perm p(&v, v.begin());

  vector<int> debug={4,3,2,1,0};

  generate( r.begin(), r.end(),  [ cp, &p, &debug ] ()-> int { 
      cerr << endl << "try sequence : ["; 
      copy(p.v->begin(), p.v->end(), ostream_iterator<int>(cerr, ","));
      cerr << "]" << endl;
      
      int s = compute_amp(cp, p.v);
      if ( equal( p.v->begin(), p.v->end(), debug.begin()) ){
        cout << "amplified signal is: " << s << endl;
      }
      p.next_perm();
      return s; });


  cerr << endl << "generated vector : ["; 
  copy(r.begin(), r.end(), ostream_iterator<int>(cerr, ","));
  cerr << "]" << endl;

  cout << *max_element( r.begin(), r.end() ) << endl;

}









