#include <iostream>
#include <cstdio>
#include <iterator>
#include <algorithm>
#include <vector>
#include <memory>
#include <thread>
#include <future>

#define MAIN
#include "Perm.h"
#include "IntCode.h"

#define AMP_N 5

#define GOLD

#ifdef SILVER
#define PHASES {0,1,2,3,4} 
#define LOOP 0
#endif

#ifdef GOLD
#define PHASES {5,6,7,8,9} 
#define LOOP 1
#endif


using namespace std;


void amplificator_unit( unique_ptr<IntCode> *cp,  int debug){
  try {
    if ( LOOP ){
      (*cp)->compute();
    } else {
      (*cp)->next_result();
    }

  } catch ( ... ) {
    cerr << "exception thrown " << endl;
    (*cp)->out->put(0);
  }
  //cerr << "finished t:" << debug << endl;
}


long compute_amp( vector<long>& heap, vector<int>& ph ){
  
  vector<shared_ptr<Channel>> ch(AMP_N+1);

  int i=-1;
  generate( ch.begin(), ch.end(), [&i, &ph] () { 
      return ++i - AMP_N ? make_shared<Channel>(ph[i]) : make_shared<Channel>();
      });

  
  vector<unique_ptr<IntCode>> cpv(AMP_N);
  
  i=0;
  generate( cpv.begin(), cpv.end(), [&heap, &ch, &i]  { 
      return make_unique<IntCode>( heap, ch[i++], LOOP ? ch[(i+1) % AMP_N] : ch[i+1] );
      });

  //cpv[3]->verbose=2;


  vector<thread> t(5);
  i=-1;
  generate( t.begin(), t.end(), [&cpv, &ch, &ph, &i] () {
      return thread( amplificator_unit, &cpv[++i], i ); 
      });

  cpv[0]->in->put(0);

  for ( auto& th : t ) th.join();

  
  return cpv[AMP_N-1]->out->get();
}


int main(){


  IntCode cp = IntCode();
  cin >> cp;
  vector<IntCode> cpv(AMP_N);
  fill(cpv.begin(), cpv.end(), cp);
  vector<int> v(PHASES);
  Perm p( &v );
  vector<int> r(p.card());



  
  generate( r.begin(), r.end(),  [ &cp, &p ] ()-> long 
      { 
        cerr << endl << "try sequence : ["; 
        copy(p.v->begin(), p.v->end(), ostream_iterator<int>(cerr, ","));
        cerr << "]" << endl;
        
        long s = compute_amp( cp.heap, *(p.v) );
        cerr << "amplified signal is:" << s << endl;

        p.next_perm();
        return s; 
      });


  cerr << endl << "generated vector : ["; 
  copy(r.begin(), r.end(), ostream_iterator<long>(cerr, ","));
  cerr << "]" << endl;

  cout << *max_element( r.begin(), r.end() ) << endl;
}









