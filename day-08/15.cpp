#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>


#define HFRAME 6
#define WFRAME 25
#define SFRAME ( HFRAME * WFRAME)

using namespace std;

struct Frame {

  int zeros;
  char rows[SFRAME];

  Frame(){};
  Frame(const Frame& other){
    zeros = other.zeros;
    copy( other.rows, other.rows + SFRAME, this->rows);
  }
};

istream& operator>> ( istream& in, Frame& f ){
  char c;
  in.get( f.rows, SFRAME+1);
  f.zeros = count( f.rows, f.rows + SFRAME + 1, '0');
  return in;
}

int main(){

  Frame f;
  vector<Frame> vf;
  int nf=-1;

  while( cin >> f ){
    nf++;
    if ( nf % 5 == 5 & nf < 10 ){
      cerr << " nf:" << nf << endl;  
      copy( f.rows , f.rows+SFRAME+1, ostream_iterator<char>(cerr, ""));
      cerr << endl;
    }

    vf.push_back(f);
  }


  cerr << "compare zeros [ ";
  auto r = min_element(vf.begin(), vf.end(), [] (Frame ff1, Frame ff2) ->int { 
      cerr << ff1.zeros << ",";
      return ff1.zeros <= ff2.zeros; } );
  cerr << vf.rbegin()->zeros << " ]" << endl; 


  cerr << "first with zeros:" << vf.begin()->zeros << endl;
  copy( vf[0].rows, vf[0].rows+SFRAME, ostream_iterator<char>(cerr, " ") ); 
  cerr << endl;

  cerr << "r with zeros:" << r->zeros << endl;
  copy( r->rows, r->rows+SFRAME, ostream_iterator<char>(cerr, " ") ); 
  cerr << endl;

  cerr << "last with zeros:" << vf.rbegin()->zeros << endl;
  copy( vf.rbegin()->rows, vf.rbegin()->rows+SFRAME, ostream_iterator<char>(cerr, " ") ); 
  cerr << endl;

  int ones, twos; 
  ones = count( r->rows, r->rows + SFRAME, '1' );
  twos = count( r->rows, r->rows + SFRAME, '2' );
  cerr << "r with ones:" << ones << endl;
  cerr << "r with twos:" << twos << endl;

  cout << ones * twos << endl ;

}
