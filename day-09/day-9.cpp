#include <iostream>
#include <vector>
#include <thread>


#include "IntCode.h"

#define INPT 1
#define GOLD 

#ifdef GOLD
#define INPT 2
#endif

using namespace std;





int main (){

  IntCode cp;

  cp.verbose = 2;

  cin >> cp;


  { 
    thread t = thread( [&cp] { cp.in->put(INPT); cp.compute(); } );
    thread res = thread( [&cp] { while( true ){cp.out->get();}} );
    t.join();
    res.detach();
  }
}


