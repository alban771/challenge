#include <algorithm>





template<typename... A>
int ppcm( int& a0, const A&&... a){
  int pass[] {([&](){ a0 = ppcm( a0, a); }, 1)...};
  return a0;
}

void pass();



long long pgcd( const long long aa, const long long bb){
  int a,b,q,r;
  // TODO use sort alg 
  a = std::max(abs( aa),abs( bb));
  b = std::min(abs( aa),abs( bb));
  if ( a == 0 ) return 0;
  if ( b == 0 ) return a;
  while( q=a/b, r=a%b, r>0){ a=b; b=r; }
  return b;
}


long long ppcm( const long long aa, const long long bb){
  long long p = pgcd(aa, bb);
  return ( aa / p ) * bb;
}


/*
#include <iostream>
int main(){
  std::cout << ppcm( 4, 14, 5);
}
*/
