#include <map>
#include <iostream>
#include <cmath>
#include <unordered_set>
#include <vector>
#include <algorithm>
#include <iterator>

#define PI 3.1415926535

using namespace std;


int pgcd( const int aa, const int bb){
  int a,b,q,r;
  // TODO use sort alg 
  a = max(abs( aa),abs( bb));
  b = min(abs( aa),abs( bb));
  if ( a == 0 ) return 0;
  if ( b == 0 ) return a;
  while( q=a/b, r=a%b, r>0){ a=b; b=r; }
  return b;
}


struct Point {
  int c,r;
  Point(){};
  Point(int a, int b):c(a),r(b){}

  Point operator-( const Point other){
    int a,b,d;
    a = c - other.c;
    b = r - other.r; 
    d = pgcd(a,b);
    if ( d == 0 ) return Point(0, 0);
    return Point( a/d, b/d);
  }


  tuple<float, float> radial(){
    float l, th;
    l = sqrt( c*c + r*r );
    if ( c == 0 &&  r == 0 ){ return make_tuple(0,0); }
    th = r > 0 ? 
      c > 0 ? asin( r/l ) : PI - asin( r/l )
      : c > 0 ? 2*PI-asin( -r/l) : PI+asin( -r/l );  
    return make_tuple(l,th);
  }
};

Point operator-=( const Point& first, const Point& other){
    return Point( first.c-other.c, first.r-other.r);
  }

bool operator<( const Point& first, const Point& other){
  return abs( first.r ) + abs( first.c ) < abs( other.r) + abs(other.c);
}

bool closerTo( const Point& first, const Point& second, const Point& center){
  return ( first -= center)  < ( second -= center );
}

bool operator==( const Point& first, const Point& other){
  return first.r==other.r && first.c==other.c;
}

//ref https://en.wikipedia.org/wiki/Unordered_associative_containers_(C%2B%2B)#Custom_hash_functions
namespace std {
  template<>
    class hash<Point> {
      public:
        size_t operator()(const Point& p) const{
          return hash<int>()( p.r << 8 + p.c);
        }
    };
}



ostream& operator<<( ostream& out, const Point& p ){
  out << "(" << p.c << "," << p.r << ")";
  return out;
}

struct Asteroid {
  Point p;
  unordered_set<Point> s;

  Asteroid(){};

  Asteroid(Point pp, const vector<Point>& m):p(pp){
    for ( Point a : m ) {
      s.insert(a - p);
    }
    s.erase(Point(0,0));
  }
};



void calibration( tuple<float, float>& t ){
  /* axes are x --->
   *            |
   *          y *
   * hence trigonometric direction is clockwise. 
   * 15min must be added to get the coordinates from noon.  
   */
  get<1>(t) = get<1>(t) > 3 * PI / 2 ? get<1>(t) - 3*PI/2 : get<1>(t) + PI/2;
}


struct lessRadial {
  bool operator()(const Point& p1, const Point& p2) {
    Point q1=p1, q2=p2;
    auto t1 = q1.radial(), t2 = q2.radial(); 
    calibration(t1);
    calibration(t2);
    return get<1>(t1) < get<1>(t2);
  }
};


int main(){


  vector<Point> m;
  string s;
  int i = -1,j = -1;

  while ( cin >> s ){
    i++;
    j=-1;
    for_each( s.begin(), s.end(), [&i, &j, &m] (char c) {
        j++;
        if ( c == '#' ) m.push_back(Point(j,i));
        });
  }

  
  vector<Asteroid> a(m.size());
  i = 0;
  generate( a.begin(), a.end(), [&m, &i] {
      return Asteroid( m[i++], m );
      });

  auto mm = max_element( a.begin(), a.end(), [] (Asteroid a1, Asteroid a2) {
    return a1.s.size() <= a2.s.size();
    });

  multimap<Point,Point,lessRadial> grp_mm;

  m.erase( find( m.begin(), m.end(), mm->p ) );

  for ( Point p : m ) {
      Point pp = p - mm->p;
      grp_mm.insert( pair<Point,Point>( pp, p));
  };

  for( auto re = grp_mm.begin(); re != grp_mm.end(); re ++){
    cerr << re->first << " "  <<  re->second  <<  endl;
  }


  /*
  auto re = grp_mm.equal_range( *(++(++ mm->s.begin() ) ) );
  for_each( re.first, re.second, [&mm] (auto e) { 
      cerr << e.first << " " << (  e.second -= mm->p ) << endl;  });
  cerr << endl;
 
  */
 
  
  sort( m.begin(), m.end(), [&mm, &grp_mm](Point p1, Point p2) {
    Point pp1 = p1 - mm->p, pp2 = p2 - mm->p;
    auto r1 = grp_mm.equal_range(pp1), r2 = grp_mm.equal_range(pp2);
    auto c = grp_mm.key_comp();
    int c1 = -1, c2 = -1;

    for_each( r1.first, r1.second, [&c1, &mm, &p1] (auto rp) {
        c1 += !closerTo(p1, rp.second, mm->p);
        });

    for_each( r2.first, r2.second, [&c2, &mm, &p2] (auto rp) {
        c2 += !closerTo(p2, rp.second, mm->p);
        });

    return c1 == c2 ? c( pp1, pp2 ) : c1 < c2;
    });

  /*copy( m.begin(), m.end(), ostream_iterator<Point>(cerr, " "));
  cerr << endl;

  int ss = 2;
  if ( m.size() > 200 ){
    ss = 200 - 1;
  } 
  cerr << *( m[ss]);
  */

  cout << m[199].c * 100 + m[199].r << endl;
}


