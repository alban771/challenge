#include <iostream>
#include <algorithm>
#include <iterator>
#include <unordered_map>
#include <thread>

#include "IntCode.h"
#include "../miscellaneous/Point.h"


using namespace std;

int main(){

  IntCode cp;
  cin >> cp;



  unordered_map<Point, bool> paint;
  int wM=0, wm=0, hM=0, hm=0;

  paint.insert(make_pair(Point(0,0), true));
  
  {
    thread cpt = thread( [&cp] { cp.compute(); });

    thread fillt = thread( [&cp, &paint, &wM, &wm, &hM, &hm] {
        Point current = Point(0,0);
        Point dir = Point(0,-1);

        while(true){
          if( paint.find(current) == paint.end() ){
            //cerr << "new pane:" << current << endl;
            wM=current.c > wM ? current.c : wM;
            wm=current.c < wm ? current.c : wm;
            hM=current.r > hM ? current.r : hM;
            hm=current.r < hm ? current.r : hm;
            paint.insert( make_pair(current, false));
          }

          cp.in->put(paint[current]);
          //cerr << "current pane is:" << ( paint[current] ? "black": "white") << endl;
          paint[current]=cp.out->get();
          //cerr << "painting pane:" << current << " in " << ( paint[current] ? "black": "white") << endl;
          if ( !cp.out->get() ){
            //cerr << "turn left" << endl;
            dir.turnLeft();
          } else {
            //cerr << "turn right" << endl;
            dir.turnRight();
          }
          current+=dir;
        }
        });

    cpt.join();
    cerr << "f";
    fillt.detach();
    cerr << "o";
  }
  cerr << "o" << endl;
  
  vector<char> line;
  for ( int i = hm-1; i<= hM+1; i++){
    line = vector<char>(wM-wm, '.');
    for_each( paint.begin(), paint.end(), [&i, &line] (auto c) { 

        if( c.first.r == i ){ 
          //cerr << "baz";
          line[c.first.c] = ( c.second ? '#': '.');
          }
        });
    
    cout << endl;
    copy( line.begin(), line.end(), ostream_iterator<char>(cout));
  }

  return 0;
}
