### Day - 13 : Pong game

Space traveler also has some moments of peace. Instead of going across all the ship to find the game arcade, we are proposed to create our own game arcade, to play with an [IntCode](../lib/IntCode) program. 

full statement [here](STATEMENT.md).

## Silver

First step toward the game. 

<details>
<summary>Pretty termination</summary>

The game program is computed by a background thread <code>(Incode) cp.compute()</code>. 

Then two threads are workers on it : <code>screen_thread</code> and <code>joystic_thread</code>.

To terminate these worker thread when the background is finished, I pass a shared_future as termination signal.

<pre>
<code>
  promise<void> cp_shutdown;
  shared_future<void> sd(cp_shutdown.get_future());

  thread screen_thread = thread( [&cp, &P, sd, &m, &cv, &ready] {
      int i=0;
      while ( sd.wait_for(chrono::milliseconds(1)) == future_status::timeout ) { ... }  
  });
  
  thread joystic_thread = thread( [&cp, &P, sd, &m, &cv, &ready] {
      while ( sd.wait_for(chrono::milliseconds(1)) == future_status::timeout ) { ... }
  });
 
  // shutdown worker threads
  cp_shutdown.set_value();
</code>
</pre>

Advantages of this solution is that promise ownership is isolated from workers, and workers can share a single future object.

</details>


## Gold

Just finish the game. 

<details>
<summary>A very dummy bot player</summary>

Eagerly follows the ball: 
<pre>
<code>
  template<>
  void play<0>(const IntCode<int>& cp, const Pong& P)  
  {
    cerr << "time to play! paddle:" << P.paddle << " ball:"  << P.ball << endl;
    cp.in->put( P.ball.c < P.paddle.c ? -1 : P.ball.c != P.paddle.c );
  }
</code>
</pre>

Player is now fast enough to create a data race with the screen update thread. It may responds without the game to be fully updated, and miss the ball! 

That is why you may see inside joystic thread :
<pre>
<code>

        { 
          unique_lock<mutex> lk(m);
          cv.wait_for( lk, chrono::milliseconds(20), [&ready] { return ready; });

          play<INTERACTIVE>(cp, P);
          ready = false;
        }
        cv.notify_one();

</code>
</pre>
</details>



![](game_play.mp4)
