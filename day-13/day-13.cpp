#include <iostream>
#include <map>
#include <iterator>
#include <algorithm>
#include <thread>
#include <future>
#include <complex>


#include "IntCode.h"

#define COL 23
#define ROW 45

typedef complex<int> point;




#ifdef SILVER

#define INTERACTIVE 1     // activate user input 
#define SAMPLE_FRAME 1    // do not skip frame
#define REFRESH_RATE 60   // wait before display

#else 

#ifdef HEADLESS

#define INTERACTIVE 0 
#define SAMPLE_FRAME 100000
#define REFRESH_RATE 0

#else 

#define INTERACTIVE 0 
#define SAMPLE_FRAME 10
#define REFRESH_RATE 6

#endif
#endif

#define INTCODE_TO 5    // before acquisition timeout 


using namespace std;

typedef size_t row_n;
typedef size_t col_n;
typedef multimap<row_n,col_n> pixels;
typedef pair<row_n,col_n> px;

struct Pong {
  int fw, fh, s;
  point ball;
  point paddle;
  pixels walls; 
  pixels blocks; 
    
  /* initialise a fresh game */
  Pong(IntCode<int>& cp):s(0){

    bool loaded = false;
    int x,y,k;

    while (!loaded){

      x = cp.out->get( INTCODE_TO, loaded );
      if (!loaded){
        y = cp.out->get();
        k = cp.out->get();
        switch( k ){
          case 1:
            walls.insert(make_pair(y,x));
            break;
          case 2:
            blocks.insert(make_pair(y,x));
            break;
          case 3:
            paddle = point(x,y);
            break;
          case 4:
            ball = point(x,y);
        }
      }
    }

    fw = max_element(walls.begin(), walls.end(), []( px px1, px px2 ){ 
        return px1.second < px2.second; })->second;
    fh = max_element(walls.begin(), walls.end(), []( px px1, px px2 ){ 
        return px1.first < px2.first; })->first;
  };


  bool try_update_score( const point& p, const IntCode<int>& cp){
    const bool b = p == point(-1, 0);
    if (b) {
      s = cp.out->get();
      cerr << "update score:" << s << endl; 
    }
    return b;
  }

  
  void break_block( const point& p){

    const auto range = blocks.equal_range(p.imag()); 

    const auto fi = find_if( range.first, range.second, [&p] ( auto p2 ){
        return p2.second == (col_n) p.real();
        });

    if ( fi == range.second ){
      cerr << "no block to break at:" << p << endl;

    } else {  
      cerr << "break block at:" << p << endl;
      blocks.erase( fi );
    }
  }


  bool update( const IntCode<int>& cp){ 

    bool to;
    point p = cp.out->get(INTCODE_TO, to);
    if ( to )
      return to;
    p += point( 0, cp.out->get() );

    if ( !try_update_score(p, cp) ){

      const int k = cp.out->get();
      switch( k ){
        case 0:
          break_block(p);
          break;

        case 3:
          cerr << "update paddle at:" << p << endl;
          paddle = p;
          break;

        case 4:
          cerr << "update ball at:" << p << endl;
          ball = p;
          break;

        default:
          cerr << "unexpected update code" << endl;
          throw "unexpected update code";
      }
    }

    return to;
  }

};


/* print frame */
ostream& operator<<( ostream& out, const Pong& p ){

  cout << endl << "         SCORE : " << p.s << endl << endl;
  vector<char> line(p.fw+1); 
  for ( int i = 0; i < p.fh; i++){
    fill( line.begin(), line.end(),' ');

    if( p.ball.imag() == i ){ line[p.ball.real()] = 'o';} 
    if( p.paddle.imag() == i ){ line[p.paddle.real()] = 'T';} 

    auto rwal = p.walls.equal_range( i );
    auto rblo = p.blocks.equal_range( i );

    
    for_each( rwal.first, rwal.second, [&line] ( pair<row_n,col_n> px ){
        line[px.second] = '|';
        });

    for_each( rblo.first, rblo.second, [&line] ( pair<row_n,col_n> px ){
        line[px.second] = 'V';
        });

    copy( line.begin(), line.end(), ostream_iterator<char>(cout));
    cout << endl;
  }

  return out;
}




template<bool is_interactive>
void play(const IntCode<int>& cp, const Pong& P);


template<>
void play<0>(const IntCode<int>& cp, const Pong& P) {
  cerr << "time to play! paddle:" << P.paddle << " ball:"  << P.ball << endl;
  cp.in->put( P.ball.real() < P.paddle.real() ? -1 : P.ball.real() != P.paddle.real() );
}


template<>
void play<1>(const IntCode<int>& cp, const Pong& P) {
  string kk;
  cout << "move with k <   > l " << endl;
  cin >> kk; 

  while ( this_thread::sleep_for( chrono::milliseconds(10) ),
          kk.size() == 0 ) 
    cin >> kk;

  cp.in->put( kk == "k" ? -1 : kk == "l");
}



int main(){

  IntCode<int> cp("day-13.input");

  cp.set(0, 2);                                 // insert coins
  cp.compute();                                 // power up the arcade
  Pong P(cp);                                   // start the game 


#ifndef HEADLESS
  cout << P;
#endif

  // shutdown signal
  // ref: https://thispointer.com/c11-how-to-stop-or-terminate-a-thread/
  promise<void> cp_shutdown;
  shared_future<void> sd(cp_shutdown.get_future());

  // sychonisation 
  // could be avoided with single thread, where is the fun in that ? 
  mutex m;
  condition_variable cv;    
  bool ready;

  thread screen_thread = thread( [&cp, &P, sd, &m, &cv, &ready] {                // refresh the screen
      int i=0;
      while ( sd.wait_for(chrono::milliseconds(1)) == future_status::timeout ) {  

        unique_lock<mutex> lk(m);
        cv.wait_for( lk, chrono::milliseconds(20), [&ready] { return !ready; }); // delay in case input thread is shutdown

        while( ! P.update(cp) )
        {
          i++;
#ifndef HEADLESS
          if ( !(i % SAMPLE_FRAME ))
          {
            cout << P << endl;       
          }
#endif
        }

        ready = true;
        lk.unlock();
        cv.notify_one();
      }
  });


  thread joystic_thread = thread( [&cp, &P, sd, &m, &cv, &ready] {                // joystic driver 
      while ( sd.wait_for(chrono::milliseconds(1)) == future_status::timeout ) 
      {  
        { 
          unique_lock<mutex> lk(m);
          cv.wait_for( lk, chrono::milliseconds(20), [&ready] { return ready; });

          play<INTERACTIVE>(cp, P);
          ready = false;
        }

        cv.notify_one();
        this_thread::sleep_for( chrono::milliseconds(REFRESH_RATE));
      }});


  cp.join();
  // shutdown worker threads
  cp_shutdown.set_value();
  screen_thread.join();
  joystic_thread.join();

  cout << "final score : " << P.s << endl;

  return 0;
}
