#include <iostream>
#include <iterator>
#include <map>
#include <list>
#include <algorithm>
#include <cstring>
#include <cmath>

#define CARGO 1'000'000'000'000

using namespace std;

struct Chemical {
  string n;
  int q;

  Chemical(){};
  Chemical(string name, int quantity):n(name),q(quantity){}

};

bool operator<( const Chemical& c1, const Chemical& c2){
  return c1.n < c2.n;
}

bool operator==( const Chemical& c1, const Chemical& c2){
  return c1.n == c2.n;
}

ostream& operator<<( ostream& out, const Chemical& c ){
  out << c.q << "-" << c.n;
  return out;
}


typedef list<Chemical> ingredients; 

void igdt_add( ingredients& igdt, Chemical c){
  auto i = find( igdt.begin(), igdt.end(), c );
  if ( i == igdt.end()){
    igdt.push_back(c);
  } else {
    i->q += c.q;
  }
}

struct Reaction {
  Chemical p;
  ingredients r;

  Reaction(){};
  Reaction( Chemical product, ingredients reactifs ):p(product),r(reactifs){}
  Reaction( map<Chemical, ingredients>::iterator pair_p_r ):p(pair_p_r->first),r(pair_p_r->second){}

  void merge( Reaction sub, ingredients& lo ){
    auto i = find( r.begin(), r.end(), sub.p );
    if ( i != r.end() ){

      //invariant 
      //  \-/ reactif r de la reaction sub visite
      //  r est ajoute en quantite suffisante pour produire Qp*sub.p     
      //  ou Qp est la quantite voulue de sub.p en tant que reactif.

      auto loo = find( lo.begin(), lo.end(), sub.p ); 
      if ( loo != lo.end() ){
        int m = min( i->q, loo->q );
        i->q -= m;
        loo->q -= m;
      }

      int eff = ceil( (double) i->q / sub.p.q );
      sub.p.q *= eff;
      sub.p.q -= i->q;
      igdt_add( lo, sub.p );
      r.erase(i);
      cerr << "Left over : [";
      copy( lo.begin(), lo.end(), ostream_iterator<Chemical>(cerr, ", "));
      cerr << "]" << endl;

      for( Chemical rc : sub.r  ){
        rc.q *= eff;
        loo = find(lo.begin(), lo.end(), rc);
        if ( loo != lo.end() ){
          int m = min( rc.q, loo->q );
          cerr << "found left over for reactif:" << rc.n  << endl;
          rc.q -= m;
          loo->q -= m;
        }
        igdt_add( r, rc );
      }
    }
  }
};

ostream& operator<<( ostream& out, const Reaction& reaction ){
  out << "product:" << reaction.p << " ingredients: [ " ; 
  copy( reaction.r.begin(), reaction.r.end(), ostream_iterator<Chemical>(out, ", "));
  out << "]" << endl;
  return out;
}

int main(){

  map<Chemical, ingredients> reactions;

  int q;
  string line,n;
  ingredients igdt;
  char *tok;

  while ( getline(cin, line, '>'), cin.ignore(), cin >> q, cin >> n ){

    tok = strtok( const_cast<char*>(line.c_str()), " ,=" );

    while ( tok != NULL ){
      igdt.push_back(Chemical(string(strtok(NULL," ,=")),atoi(tok)));
      tok = strtok(NULL, " ,=");
    }

    reactions.insert(make_pair( Chemical(n,q), igdt));
    igdt.clear();
  }


  Reaction r0,r1;
  ingredients left_over;
  long ores=0;
  
  r0 = Reaction( reactions.find(Chemical("FUEL",1)));
  auto ir0 = r0.r.begin();
  while( ir0 != r0.r.end() ){

    if ( ir0->n != "ORE" ){
      r1 = Reaction( reactions.find( *(ir0++) ) );
      r0.merge(r1, left_over );

    } else {
      ir0++;
    }
  }

  ores+=r0.r.front().q;
  cout << "Need that much ORE to create one FUEL:"; 
  cout << ores << endl;

}
