#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <cstring>
#include <cmath>

#define CARGO 1'000'000'000'000LL

using namespace std;

struct Chemical {
  string n;
  long long q;

  Chemical(){};
  Chemical(string name):n(name){};
  Chemical(string name, int quantity):n(name),q(quantity){}

  Chemical operator*( long long coeff ){ return Chemical(n, q*coeff); }
  void operator*=( long long coeff ){  q*=coeff; }
  void operator+=( Chemical c ){  q+=c.q; }
};

bool operator<( const Chemical& c1, const Chemical& c2){
  return c1.n < c2.n;
}

bool operator==( const Chemical& c1, const Chemical& c2){
  return c1.n == c2.n;
}

ostream& operator<<( ostream& out, const Chemical& c ){
  out << c.q << "-" << c.n;
  return out;
}


typedef vector<Chemical> ingredients; 

struct Reaction {
  ingredients r;
  Chemical p;

  Reaction(){};
  Reaction( string pn ):p(Chemical(pn)){};
  Reaction( Chemical product ):p(product){};
  Reaction( Chemical product, ingredients reactifs ):r(reactifs),p(product){}

  void merge( const Reaction& sub, Reaction& v ){
    auto i = find( r.begin(), r.end(), sub.p );
    if ( i != r.end() ){
      
      int coeff = ceil( (double) i->q / sub.p.q );
      i->q -= coeff * sub.p.q;  
      if ( ! i->q ) r.erase(i); 
      v.r.push_back( Chemical( sub.p.n, coeff) );
      
      //invariant 
      //  \-/ reactif r de la reaction sub visite
      //  r est ajoute en quantite nominale * coeff 
      //  aux reactifs de la reaction courrante     
      //
      for( Chemical rc : sub.r  ){
        add_reactif( rc * coeff );
      }
    }
  }

  void add_reactif( Chemical c ){
    auto i = find( r.begin(), r.end(), c );
    if ( i == r.end()){
      r.push_back(c);
    } else {
      i->q += c.q;
    }
  }

  const Chemical operator[](const string& name){
    Chemical c (name, 0);
    auto ri = find( r.begin(), r.end(), c );
    return ri == r.end() ? c : *ri;
  }

  Reaction operator*( long long scalar ){
    Reaction rr(p, r);
    rr *= scalar;
    return rr;
  }

  void operator*=( long long scalar ){
    p *= scalar;
    for ( Chemical& c : r ){ c *= scalar; }
  }

  void operator+=( const Reaction& other ){
    if ( other.p == p ) { 
      p += other.p;
    } else {
      Chemical mp = Chemical( other.p.n, -1 * other.p.q );
      add_reactif( mp );
    }
    for ( const Chemical& rc : other.r ){
      add_reactif( rc );
    }
  }

};


bool operator<(const Reaction& r0, const Reaction& r1){
  return find( r0.r.begin(), r0.r.end(), r1.p ) != r0.r.end();
}

bool operator==(const Reaction& r0, const Reaction& r1){
  return  r0.p == r1.p;
}



ostream& operator<<( ostream& out, const Reaction& reaction ){
  out << "product:" << reaction.p << " ingredients: [ " ; 
  copy( reaction.r.begin(), reaction.r.end(), ostream_iterator<Chemical>(out, ", "));
  out << "]" << endl;
  return out;
}



void print_A( const vector<Reaction>& A ){

  for ( Reaction a : A ){
    cerr << a << endl;
  }


  /*
  vector<int> line(A.size());
  int i = 0;
  for ( auto ai = A.begin(); ai != A.end(); ai++ ){
    fill(line.begin(), line.end(), 0);
     line[i++] = ai->p.q;
     for ( auto ri = ai->r.begin(); ri != ai->r.end(); ri++ ){
       if ( ri->n == "ORE"){ continue; }
       auto aj = ai;
       int j = i;
       while ( j++, ! ( (++aj)->p == *ri) && aj != A.end() ){}
       if ( aj == A.end() ){ throw "should have found reactif"; }
       line[j] = ri->q;
     }
     cerr << " | ";
     copy( line.begin(), line.end(), ostream_iterator<int>(cerr, " "));
     cerr << " | " << endl;
  }
  */
}





int main(){

  // input set of every reactions
  // map need to be ordered
  // so that we triangularize the matrix;
  // Reaction is a line.
  vector<Reaction> A;
  // v is the base to change from A to e 
  // e is the matrix twith which we solve linear minimisation,
  // Reaction is a column
  // A.v = e;
  // dim e < dim A
  vector<Reaction> v,e;

  /* parsing strategy 
   * set order ensure sort of matrix rows.
   * */
  int q;
  string line,n;
  ingredients igdt;
  char *tok;

  while ( getline(cin, line, '>'), cin.ignore(), cin >> q, cin >> n ){

    tok = strtok( const_cast<char*>(line.c_str()), " ,=" );
    while ( tok != NULL ){
      igdt.push_back(Chemical(string(strtok(NULL," ,=")),atoi(tok)));
      tok = strtok(NULL, " ,=");
    }
    A.push_back(Reaction( Chemical(n,q), igdt));
    igdt.clear();
  }
  /* end parsing */

  //print_A( A);


  /* sort matrix colums */

  auto ai = A.begin();
  while ( ai != A.end()){
    auto aj = find_if( ai, A.end(), [&ai] (Reaction& ra) { return ra < *ai; }); 
    if ( aj != A.end() ){ 
      iter_swap( ai, aj); 
    } else {
      ai++;
    }
  }

  /* end sort  matrix columns */

  //print_A( A);


  /* build base v */ 
   
  ai = A.begin(); 
  while ( ai != A.end() ){
    e.push_back( *ai);
    v.push_back( Reaction( ai->p ));

    for ( auto aj = ai; aj != A.end(); aj++ ){
      e.back().merge( *aj, v.back() );
    }

    ai == find_if( ++ai, A.end(), [&e] ( Reaction r ) {
        return e.back() < r;
        }); 
  }

  /* base v built */

  //print_A( e);


  /* get max fuels for C ores */
  
  Reaction C( Chemical( "FUEL", 0));
  cerr << C;
  cerr << e[0];

  while ( CARGO - C["ORE"].q >= e[0]["ORE"].q ) {

    long long coef = ((CARGO - C["ORE"].q) / e[0]["ORE"].q );
    C += e[0] * ( coef + 1 );
    cerr << "before savings" << endl;
    cerr << C;

    for ( auto ei = (e.begin())++ ; ei != e.end(); ei++  ){

      long long bar  =  ceil( C[ei->p.n].q / ei->p.q );
      C += ( ( *ei ) * bar);
    }

    cerr << "after savings" << endl;
    cerr << C;
  }
  /* Ore consumption is done */

  cout << "can do that much fuel:" << C.p.q - 1 << endl;
}
