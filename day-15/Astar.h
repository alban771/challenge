#include <vector>
#include <list>
#include "../day-11/Point.h"

#ifndef ASTAR
#define ASTAR


struct Node {
  int l;
  Point coord;
  Node* origin;
  std::vector<Point> extensions;

  Node();
  Node( Node* origin, Point ext );
  Node( const Node& );

  /* path goes from this to node
   *
   */
  std::vector<Point> pathTo( const Node& dest );

  Point adjacent( const Node& n ) const;

  int calculate_length();
  
  bool operator==( const Node& n );

};


class Astar {

public:
  std::list<Node> extensible;
  std::list<Node> visited;

private:
  void eval_new_length();

public:

  Astar();

  Node& choose_one();

  void extend( Node *n, bool success );

  void new_origin( Node *n, int offset);
};


#endif


