#include <iostream>
#include <list>
#include <fstream>
#include <thread>
#include <map>
#include <iterator>
#include <algorithm>
#include <cassert>


#include "IntCode.h"
#include "Astar.h"

#define COL 60
#define ROW 20
#define REFRESH_RATE 0

using namespace std;


typedef size_t row_n;
typedef size_t col_n;
typedef multimap<row_n,col_n> pixels;


pair<row_n,col_n> to_pixel( const Point& p){
  return make_pair( p.r, p.c);
}

struct Area {

  int fh, fw, tl;
  Point droid, oxygn_systm;
  pixels walls; 
  pixels trace;

  Area(int a, int b):fh(a),fw(b),tl(0),droid(b/2,a/2){};
  Area(const Area& other):fh(other.fh),fw(other.fw),tl(0),droid(other.fw/2,other.fh/2){}


  // check if not present
  void insert_trace( const Point& p ){
    auto rtra = trace.equal_range( p.r );
    if ( find_if( rtra.first, rtra.second, [&p] (auto tra) {
          return (int) tra.second == p.c;
          }) == rtra.second ){
      trace.insert( to_pixel(p) );
    }
  }
  
};




ostream& operator<<( ostream& out, const Area& a ){

  out << endl;
  cout << "Trace : " << a.tl << endl;
  vector<char> line(a.fw+1); 
  for ( int i = 0; i < a.fh; i++){
    fill( line.begin(), line.end(),' ');

    auto rwal = a.walls.equal_range( i + ( a.droid.r - a.fh/2));
    auto rtra = a.trace.equal_range( i + ( a.droid.r - a.fh/2));
    
    for_each( rtra.first, rtra.second, [&line, &a] ( pair<row_n,col_n> px ){
        int d = px.second - ( a.droid.c - a.fw/2 );
        if ( 0 < d && d < a.fw+1  ) line[d] = '.';
        });

    for_each( rwal.first, rwal.second, [&line, &a] ( pair<row_n,col_n> px ){
        int d = px.second - ( a.droid.c - a.fw/2 );
        if ( 0 < d && d < a.fw+1 ) line[d] = '#';
        });

    if( a.fh/2 == i ) line[a.fw/2] = 'D'; 

    if( ! ( Point() == a.oxygn_systm )  && a.oxygn_systm.r == i + ( a.droid.r - a.fh/2) ) {
      int d = a.oxygn_systm.c - a.droid.c + a.fw/2;
      if ( d > 0 && d < a.fw+1 ) line[d] = 'X'; 
    }

    
    copy( line.begin(), line.end(), ostream_iterator<char>(out));
    out << endl;

  }

  return out;
}



int set_next_dir(IntCode<>* cp, Point p){

    if( p == EAST ){
      cerr << "EAST" << endl;
      cp->in->put(3);
    }
    else if( p == WEST ){
      cp->in->put(4);
      cerr << "WEST" << endl;
    }
    else if( p == NORTH ){
      cp->in->put(1);
      cerr << "NORTH" << endl;
    }
    else if( p == SOUTH ){
      cp->in->put(2);
      cerr << "SOUTH" << endl;
    }

    return cp->out->get();
}

 /* 
struct Path {

  IntCode<> cp;
  Area* A;
  thread t;

  Node pos; //pos of the droid


  Path(IntCode<> cp, Area* A):cp(cp),A(A){
    t thread( [] { cp.compute(); });
  }

  Path(const Path& other):A(other.A),cp(other.cp){
    t = thread( [] { cp.compute();} );
    for ( Point p : other.path ){
      extend(p);
    }
  }


  Node bestPath( Point p ){
    Point obj = pos.coord + p;
    Node* bestPos = pos.origin;
    while( find( bestPos->neighbours.begin(), bestPos.neighbours.end(), 
          obj - bestPos->coord ) == bestPos->neightbours.end() )

  }

  int extend( Point p ){

    if( p == EAST ){
      cp.in.put(3);
    }
    else if( p == WEST ){
      cp.in.put(4);
    }
    else if( p == NORTH ){
      cp.in.put(1);
    }
    else if( p == SOUTH ){
      cp.in.put(2);
    }

    switch( cp.out.get()){
      case 0:
        A->walls.insert( to_pixel( p + pos.coord ));
        pos.neighbours.erase(p);
      case 1:
        A->droid+=p;
        pos = pos + p;
    }

    return cp.out.get();
  } 
};


bool operator<( const Path& p1, const Path& p2 ){
  return p1.path.size() < p2.path.size();
}

*/

void astar_main( IntCode<>* cp, Area* A, Astar* _astar){
  /* a start algorithm
   * chose a path to extends whose weight is minimum.
   *
   * */

  Node pos;

  while( _astar->extensible.size() ){

    Node& next = _astar->choose_one();
    vector<Point> cmds = pos.pathTo( next );

    // Go to last known position
    for_each( cmds.begin(), --cmds.end(), [&cp] (Point p) {
        assert( set_next_dir(cp, p) );
      });
    
    this_thread::sleep_for( chrono::milliseconds( REFRESH_RATE ) );

    // Test new node
    int ret = set_next_dir( cp, cmds.back() ); 

    if ( ret == 2 ) {

      pos = *next.origin;
      A->oxygn_systm = next.coord;
#ifndef GOLD
      _astar->extensible.clear();
#else
      _astar->new_origin(&pos, 0); // Oxygen instantaneously fills the first position
#endif

    } else if ( ret == 1 ) {

      pos = next;
      A->insert_trace( pos.origin->coord );
      _astar->extend( &next, true );

    } else if ( ret == 0 ) {
      
      pos = *next.origin;
      A->walls.insert( to_pixel( next.coord ));
      _astar->extend( &next, false );

    }

    A->droid = pos.coord;
#ifndef HEADLESS
    cout << *A;
#endif
  } 

}


int main(){

  IntCode<> cp("day-15.input");
  Astar _astar;
  Area A(COL,ROW);

  {
    cp.compute();
    thread i ( astar_main, &cp, &A, &_astar);
    i.join();
    cp.in->put(0);
    cp.join();
    cerr << "finished mapping" << endl;
  }

  list<Node*> head; 
  for_each( _astar.visited.begin(), _astar.visited.end(), [&head] (Node& n) {
      if ( n.extensions.size() == 0 ) {
        head.push_back(&n);
      }
  });

  cout << "Max length: " << (
      *max_element( head.begin(), head.end(), [] ( Node* n1, Node* n2) { 
          return n1->l < n2->l; 
          })
      )->l << endl;

  return 0;
}
