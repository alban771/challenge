## Day 16

Incomming is signal must be processed as exlained in the [problem statement](STATEMENT.md).
Silver and Gold problem differ essentially in the length of the signal, with 4 order of magnitude greater in gold problem.  


### Silver

The algorithm name : FFT for Flawed Frequency Transmission - a.k.a. FFT_aoc - led me to try to adapt the efficient Fast Fourier Transformation -a.k.a FFT_0 -  factorization algorithm to the algorithm described here.
However, I came up with the conclusion that, despite the fact that the pattern [0, 1, 0 -1] is a the Imaginary part of the complex number e^{i\*k\*Pi/2} with k integer, the shape of the discrete Fourier transformation :        

<pre>
  S(k) = sum_{n=0}^{N-1} s(n) e^{-2*i*\Pi*n*k/N}
</pre>

and on the other hand the Flawed Frequency Transmission :

<pre>
  S(k) = sum_{n=0}^{N-1} s(n) w[n/k]

  with  w is the pattern [ 0, 1, 0, -1 ] 
        <=>
        w[n/k] = Imag( e^{i*Pi*(n+1) / k} ) > 0 - Real( e^{i*Pi*(n+1) / k} ) > 0  
</pre>

can't be treated with similar method, since serie of exponent in FFT_aoc ~ ( 1, 1/2 .. 1/k ) is not relationed with exponent series ( 1/N, 2/N ... k/N ) of FFT_0.
   
Ultimatly, computation is performed with quadriatic cost.

### Gold

The gold signal is too long to perform 100 times the computation. I could nevertheless answer the question because the n-th digit from the tail of the FFT_aoc, only requires n+1 digits of the signal. It comes that the tail of the FTT_aoc is easy to compute whatever the length of the signal, even 100 times.

Another participant proposed efficient way to compute the FFT_aoc for the hole signal. See [this post](https://www.reddit.com/r/adventofcode/comments/ebxz7f/2019_day_16_part_2_visualization_and_hardmode)  
