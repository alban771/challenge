#include <iostream>
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>
#include <numeric>



#define GOLD 0
#define SIGNAL_REPEAT 10'000




using namespace std;


static const vector<int> pattern { 0, 1, 0, -1 };

class Phase {

  vector<int> a;

  int compute_digit( const vector<int>& aa, const int n ){
    int i = 0;
    const auto op =  [ &i, &n ] ( int s, int x ) { 
      return ( s + pattern[ ++i/(n+1) % pattern.size() ] * x ); 
    };
    return abs( accumulate( aa.begin(), aa.end(), 0, op) % 10);
  }
  
public:

  Phase(string a0):a(a0.size()){
    transform(a0.begin(), a0.end(), a.begin(), [] ( const char& c) { 
        return (int)c-48;});
  }
  

  vector<int> next( int n ){

    while (n--){
      const vector<int> aa = a;
      int i = 0;
      generate( a.begin(), a.end(), [&] () { 
          return compute_digit( aa, i++ );
      }); 
    }

    return a;
  };


  vector<int> next( int n, const int loc ){

    int c = SIGNAL_REPEAT * a.size() - loc;
    if ( c > loc )
      throw domain_error("expected 2nd half");

    vector<int> cumsum( c );

    int d=0;
    size_t i=a.size();
    generate( cumsum.rbegin(), cumsum.rend(), [&] () { 
        if  ( !i-- ) i = a.size() - 1;
        d += a[i];
        return d % 10;
        });

    while (--n){

      int d=0;
      transform( cumsum.rbegin(), cumsum.rend(), cumsum.rbegin(), [&d] ( int v ) { 
          d+=v;
          return d % 10;
          });
    }
    return cumsum;
  }
};



int main(){

  string a;
  vector<int> p100;

  cin >> a;

  Phase p(a);

  if ( GOLD ) {
    p100 = p.next(100, stoi( a.substr(0,7))); 

  } else {
    p100 = p.next(100); 
  }

  copy( p100.begin(), p100.begin()+8, ostream_iterator<int>(cout));
}



