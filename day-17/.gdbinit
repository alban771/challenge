b day-17-impl.cpp:BimapMaze::BimapMaze(LayoutMaze const&)
b day-17-impl.cpp:VacuumExplorer::VacuumExplorer(BimapMaze const&, std::pair<std::complex<double>, std::complex<double> >)
b day-17-impl.cpp:VacuumExplorer::after(std::pair<std::complex<double>, std::complex<double> > const&)
b day-17-impl.cpp:BimapMaze::add_intersection(std::complex<double> const&)
b day-17-impl.cpp:BimapMaze::remove_intersection(std::complex<double> const&)
b Grove<36, 14>::Grove(std::vector<std::complex<double>, std::allocator<std::complex<double> > > const&, std::complex<double> const&)
b day-17.cpp:Grove<36, 18>::next_path(BimapMaze&)

b day-17-impl.cpp:optimize(d17_char const&)
#8
b day-17-impl.cpp:success() 
b day-17-impl.cpp:factorize(d17_char const&, std::__cxx11::basic_string<d17_char, std::char_traits<d17_char>, std::allocator<d17_char> > const&)
#10 if ( vv.success() )
b day-17.cpp:230 

#11 vacuum_e_pool = next_pool;
b day-17.cpp:236

disable 1 2 3 4 5 6 7 8 9 10 11
enable 11  


p "grove 1 cycle 1:"

p "i 0 : add_intersection"
p "  nsct: 22,12   k: 3"
p "    22,18"
p "    28,18"
p "    28,12"
p "  nsct"
p "i 1 : add_intersection"
p "  nsct: 28,14   k: 5"
p "    34,14"
p "    34,8"
p "    22,8"
p "    22,12"
p "    28,12"
p "  nsct"
p "i 2 : add_intersection"
p "  nsct: 24,18   k: 9"
p "    24,14"
p "    28,14"
p "    28,12"
p "    22,12"
p "    22,8"
p "    34,8"
p "    34,14"
p "    28,14"
p "    28,18"
p "  nsct"
p "i 3 : remove_intersection"
p "  nsct: 22,12   k: 6"
p "    16,12"
p "  nsct"
p "    28,12"
p "    28,14"
p "    24,14"
p "    24,18"
p "    22,18"
p "  nsct"
p "    22,8"

#disable 1 2 3 6
#disable 4 5 
#condition 7 n_cycle > 7
#command 7 
#enable 4 5
#
#end




p ""
p " first call to after : R10, R12, L6, L10, R6, L6 ..."


command 8
p to_string(sentence)
p "A : " 
p to_string(dictionary[key('A')])
p "B : " 
p to_string(dictionary[key('B')])
p "C : "  
p to_string(dictionary[key('C')])
end



#condition 7 dictionary[key('B')].back().first == 'L' & dictionary[key('A')].size() <= 2 
#condition 7  !( base_cmd(*this)[9].first == 'L' & base_cmd(*this)[9].second == 4)

#condition 7 to_string(sentence).size() <= 20 & to_string(sentence).find('C',5) < 20 
            
#condition 8 !dictionary[key('A')].size()

#condition 10 g.n_cycle == 8


#disable 11

command 11
p "grove : "
p g.n_cycle
p to_string(vv.sentence)
p "A : " 
p to_string(vv.dictionary[key('A')])
p "B : " 
p to_string(vv.dictionary[key('B')])
p "C : "  
p to_string(vv.dictionary[key('C')])
end



condition 8 dictionary[key('C')].size() > 0


