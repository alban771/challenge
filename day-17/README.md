## Day 17

Our sapceship faces a giant solar flare. We must resecue drones working in space on the hull.     
The camera aiming at the hull has a poor resolution, it cannot detect the position of the drones to be rescued. However, a bright cleaning robot is visible and can transmit the alerte signal.  

[full statement](STATEMENT.md)


### SILVER 

First the map of the scaffold, that support drones is given in an IntCode program. 

For the first time, the input program requires more memory than the program given in input, leading to a new [IntCode revision]().

Today's maze is :
<pre>


                          ###########
                          #         #
                          #     #############
                          #     #   #       #
                          #     #   # #############
                          #     #   # #     #     #
                        ############# #     #     #
                        # #     #     #     #     #
                        # #     #     #     #     #
                        # #     #     #     #     #
                        # #     #     ########### #
                        # #     #           #   # #
                        # #######           #   # #
                        #                   #   # #
                      #######               #######
                      # #   #                   #
                      # #   #           ####### #
                      # #   #           #     # #
^##########           # ###########     #     # #
          #           #     #     #     #     # #
          #     #############     #     #     # #
          #     #     #           #     #     # #
          #     #     #           # #############
          #     #     #           # #   #     #
          #     #     ############# #   #     #
          #     #                   #   #     #
          #     #           #############     #
          #     #           #       #         #
          #     #           #       ###########
          #     #           #
          #######           #
                            #
                            #
</pre>

Silver question is to detect every intersections and to compute a hash of their position.

<details>
  <summary>Visitor pattern</summary>

  Since silver and gold questions on the maze may be totally different, I wanted to implement [visitor design pattern]() so that I can use on one hand polymorphism on maze data model, on the other hand I can specialize visitors with very different aims and structure, to perform computation on the maze.      

__Abstraction__

<pre>
<code>

  class MazeExplorer; 


   * \class Maze
   * \brief Polymorphic data model for a 2D maze 
   *
   *  Agnostic on the process being performed.
   *  Object in the Visitor design pattern, gives a shared pointer of itself to visiting a MazeExplorer.
   //Note : public inheritance is necessary to share pointers from devrivated classes
  class Maze : public enable_shared_from_this&lt;Maze&gt;{
  protected:
    friend class MazeExplorer;
    virtual void accept( MazeExplorer &) const;
  };


   * \class MazeExplorer 
   * \brief Polymorphic processor on maze
   *
   * May handle several maze representation
   * Visitor class in the visitor design pattern, stores a weak_ptr of visited maze, that will be lock on concrete computation. 
   * This way, MazeExplorer is not owner of any Maze, the latter can then be wiped out once computation is performed.    
  class MazeExplorer {
  protected:
    friend class Maze;
    weak_ptr&lt;const Maze&gt; maze;
    virtual void visit( const Maze &);

  public:
    MazeExplorer(){}
    MazeExplorer( const MazeExplorer & me ):maze(me.maze){}
  };

</code>
</pre>

__Implementation__

  Firstly, I use a dense view of the maze : a [Layout](../lib/Graph) containing every scaffold position indicated by '#'. 
  On this structure, the <code>IntersectionFinder</code> class, a concrete MazeExplorer, will detect the cross representing intersections: 

<pre>
 #  
###
 # 
</pre>

<pre>
<code>


 *  \class LayoutMaze
 *  \brief Dense maze accessible positions 
class LayoutMaze : public Maze {

  shared_ptr&lt;Layout[]&gt; layout_data;

public:
  LayoutMaze( const Frame& f ):layout_data(f.layout){}

  const Layout &layout() const; // merge all layouts in one
};


 * \class IntersectionFinder
 * \brief Identifies intersections and perform hash on the list 
class IntersectionFinder : public MazeExplorer {

protected:
  vector&lt;point&gt; intersections;

public:
  IntersectionFinder(){}
  IntersectionFinder( const LayoutMaze & lm){ visit( lm );}

  virtual void visit( const LayoutMaze & ); // find intersections
  const vector&lt;point&gt; & get_intersections(); // getter
  int alignment_parameter(); // compute hash
};

</code>
</pre>

</details>


### GOLD


String processing problem :

With commands : {R, L, n} respectivly meaning 'turn right', 'turn left', 'move n steps forward', the vacuum robot must cross the full maze. 

e.g: <code>R,10,R,12,L,6,L,10,R,6\n</code> sends the vacuum robot to the first intersection.

Finally, the command must be factorized into 3 repeated words: A, B, C, which cannot be longer than 20 characters, neither can the command after this transformation. 

e.g the previous command can be factorized into :
<pre>
  command:
    A,B,C\n         ( 6 chars )

  dictionary:
    A == R,10,R,12  ( 10 chars )
    B == L,6,L,10   ( 9 chars )
    C == R,6        ( 3 chars )
</pre>


<details>
  <summary>Choose a maze path</summary>

  Obviously, the command given to the vacuum robot will change depending on the path chosen at every intersections of the maze. 
  
  In particular, this maze can be split in 4 independant submazes, with 3 intersections each, that are connected by only two links to the rest of the maze. Let us call such a submaze a "Grove", like the groves of the Versailles garden. Each grove will be crossed one after another, thus summing every path of every groves gives every path on the full maze. Moreover, groves are sharing the same topology, and path can easily be computed for each of them.

  To split the maze in grove, a median point is identified : two groves are on its left, two groves are on its right, among each group one is above the median point, and one is below.  

  Inside a grove, intersections are ordered, according to their relative upstream or downstream position.

<pre>
<code> 
  template&lt;int h_median = H_MEDIAN, int v_median = V_MEDIAN&gt;
  class Grove {
    static constexpr point p_median = point(h_median, v_median);
    int n_cycle;
    static constexpr array&lt;bitset&lt;3&gt;,8&gt; cycle { 
      000,
      100,
      110,
      111,
      011,
      111,
      101,
      100
    };

  public:
    vector&lt;point&gt; nscts;
    direction ortn; 
    
    Grove( const vector&lt;point&gt; & intersection, const point &orientation):n_cycle(0),nscts(3),ortn(orientation)

    next_path()

    exit();
  }
</code>
</pre>

  The Grove class is a controller for the methods <code>BimapMaze::add_intersection( const point & )</code>, and <code>BimapMaze::remove_intersection( const point & )</code> and will call them to present every configurations of intersections of the maze inside the grove that <b>avoid cycles</b>. Hence, it get every path on that portion of maze.

  The BimapMaze class is derived from Maze structured with a boost::bimap.

__Why two methods are enough__

   In every intersection, we identify cardinal point relatively to the most upstream extremum of the intersection which is south, and to the most downstream, wich is east (note that east is not necessary the right side). If no intersection, configuration is :  
<pre>
          n
          ^
          | 
     w >--|--> e
          |
          ^
          s
</pre>

North leads to west, because their is no cycle in the grove.
To change direction with <code>add_intersection</code>, we have no choice but to swap the direction of the path between n and w :     
<pre>
         n
         v
          \ 
     w <-\ \-> e
          \
           ^
           s
</pre>

Then, remove_intersection is called to performed the opposite manipulation.  

Given that every grove in the maze has a topology equivalent to :
<pre>
       ^
     __|3_
    |  |  |
    |  |__|2_
    |     |  |
    |_____|1_|
          |
          ^
</pre>
    
We can get every configuration <code> 3 ^ 3 (directions) - 3 * C(1,3) - C(2,3) - C (3,3) (cycles) = 17 where C(a,b) is binomial coefficient.</code> with a regular use of above methods.

We can now compute every path in the full maze.

<pre>
<code>

  for ( Grove g : groves ) 
  {     // treat groves sequentially

    ...
    for ( int i = 0; i < 24; i++ ) 
    {   // number of manipulations to get every paths in a grove

      ...
      if ( g.next_path( *bm_maze ) ) // true if this path has not been before  
      { // cross the grove

        ...
      }
    } 
  }

</code>
</pre>

</details>


<details>
  <summary>Optimize the command of the robot</summary>

  Reading the path stored in BimapMaze into a command string is done by the <code>VacuumExplorer</code> class, derivated from MazeExplorer. Wiithin, the VacuumExplorer has a string processor that will attemps to optimize the string.  


__look for trivial solution__

In order to grasp the necessity of computational work, let us try to firgure out a trivial solution to the problem.   
With the high symetry of the maze we are presented, it is possible to recognised a largest portion of the maze that repeat itself. Identified with letters in following schema :

<pre>
                          AAAAAAAAAAA
                          A         A
                          A     AAAAAAAAAAAAA
                          A     A   A       #
                          A     A   A BBBBBBBBBBBBB
                          A     A   A B     #     B
                        AAAAAAAAAAAAA B     #     B
                        A A     A     B     #     B
                        A A     A     B     B     B
                        A A     A     B     B     B
                        A A     A     BBBBBBBBBBB B
                        A A     A           B   B B
                        A AAAAAAA           B   B B
                        A                   B   B B
                      AAAAAAA               BBBBBBB
                      A A   A                   B
                      A A   A           BBBBBBB B
                      A A   A           B     B B
^##########           A AAAAAAAAAAA     B     B B
          #           A     A     A     B     B B
          #     ######A#####A     A     B     B B
          #     #     A           A     B     B B
          #     #     A           A BBBBBBBBBBBBB
          #     #     A           A B   B     B
          #     #     AAAAAAAAAAAAA B   B     B
          #     #                   B   B     B
          #     #           BBBBBBBBBBBBB     B
          #     #           #       B         B
          #     #           #       BBBBBBBBBBB
          #     #           #
          #######           #
                            #
                            #
  
</pre>

If sequences A, and B are effectively the same in vacuum robot path cross the maze, it lead to the definition of 2 distinct words : a, b, respectively at the end of sequence A, and sequence B.  
<code>a = R,6,</code>
<code>b = L,6,</code>

let us check if the start of the maze, and if the repeated sequence can be factored with these two words :

sequence starting of the maze:
<pre>
  r,10,r,12,l,6,l,10,r,12
    <==>
  a,4,R,12,b,b,4,R,12,L
</pre>

repeated sequence:
<pre>
     6,L,6,L,10,L,12 ...
  or 6,L,4 ...
  or 2,L,4,R,4  ...
  or 2,R,6,R,6,R,12,R,12 ...
</pre>

if the starting sequence seems to work with the 3rd word <code>c = 4,R,12</code>, a good variant of the repeated sequence seems impossible to find, given previous a and b.

__conclusion from visual search__
An analysis based on repeated sequences on the command string may be relevant when working on a very large string with high rate of repetition, e.g. a genome sequence, because it rapidely provides information on the full string being processed. 

However, since this problem has set of constrains easy to test on a small sequence of the command string, I have rather done a simple back-tracking algorithm, testing eagerly every possible words from the start of the command string, for every possible path in the maze.

the back tracking algorithm
<pre>
<code>

  //  depth-first tree crossing
  //  a.k.a back-tracking algorithm 
  //  classic for regexp match
  //
  template<size_t size_word, size_t dictionary_size>
  void D17_StringOptimizer<size_word, dictionary_size>::optimize(){

    d17_string_type fw = first_word();

    while( fw.size() )
    {
      try 
      {
        factorize( fw );

        optimize();

        if ( success() ) break;

        else develop();

      } 
      catch ( const out_of_range & e )
      {
        break; // reaches the end of dictionary
      } 
      catch ( const exception & e) 
      {}

      fw.pop_back();
    }
  }

</code>
</pre>
</details>

__Results__

Results are negative for the moment. Probably because I implemented the <code>D17_StringOptimizer</code> class to split every word on a command "turn left" or "turn right", and that it is insufficient. I rather should take into account that a word like <code>C = 4,R,12</code> is possible.
