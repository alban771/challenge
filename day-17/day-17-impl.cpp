#include <iterator>
#include <exception>
# include <cmath>


using namespace std;

/* start Maze */

  void Maze::accept( MazeExplorer & e ) const{
    e.maze = weak_from_this();
  }

  const Layout & LayoutMaze::layout() const{
    return layout_data[0]; // todo : union of layout;
  }


  BimapMaze::BimapMaze(const LayoutMaze& lm, const oriented_point & origin ){

    vector<direction> _d = { NORTH, SOUTH, WEST, EAST }; 

    point curr = origin.first, next;

    //assert( lm.layout().contains( curr ) );

    char oritn;
    int x,y;
    multimap<col,row> vpixel;

    for_each( lm.layout().pixels->begin(), lm.layout().pixels->end(), [&vpixel]( auto p )
        {
          vpixel.insert( make_pair(p.second, p.first) );
        });

    // look for the furthest position contiguous to point (x, v), 
    // with x abcsisse in range eq_r
    const auto find_border = [] ( const auto& eq_r, int v ) -> int
    {
      int s, l = 0;
      for ( auto i = eq_r.first; i != eq_r.second; i++,l++ ); 

      vector<bool> adj( 2*l + 1, false );

      // invariant:
      // adj contains positions of visited contiguous neighbours of v
      for_each( eq_r.first, eq_r.second, [&adj, &v, &l]( auto p )
          {
            int rel = p.second - v;
            if ( rel <= l && -l <= rel ) 
              adj[ rel + l ]=true;
          });

      if ( adj[l+1] ) 
      {
        const auto _b = adj.begin() + l+1; 
        s = find( _b, adj.end(), false) - _b;
      }
      else
      {
        const auto _b = adj.rbegin() +l+1;
        s = find( _b, adj.rend(), false) - _b;
      }
      return (adj[l+1] ? 1 : -1) * (int) s; 
    }; 
   
    auto dir = find( _d.begin(), _d.end(), origin.second );
    // invariant : 
    // every segment of path have 2 adjacent segments, 
    // except the first and the last. 
    // Moreover, orientation of consecutive segment is opposite    
    while (
      dir = find_if( _d.begin(), _d.end(), [&dir, &lm, &curr]( const point& d ) 
          { 
            return d != -*dir && lm.layout().contains(d + curr);
          }),
      dir != _d.end())
    {

      oritn = *dir == EAST || *dir == WEST ? 'H' : 'V';
      assert( oritn != path.back().get<orientation>() );

      if ( oritn == 'H' )
      {
        y=0;
        auto r = lm.layout().pixels->equal_range(curr.imag());
        x=find_border(r, curr.real());
      }
      else
      {
        x=0;
        auto r = vpixel.equal_range(curr.real());
        y=find_border(r, curr.imag());
      }

      next = curr + point( x, y );
      path.push_back( segment( curr, next, oritn ) );
      curr = next;

    } //end while
  }

  BimapMaze::BimapMaze( const BimapMaze &t ):path(t.path){}


  bool BimapMaze::operator==( const BimapMaze &t ) const {
    return mismatch( path.begin(), path.end(), t.path.begin(), t.path.end() ).first == path.end();
  }


  constexpr auto sg_intersect = [] ( const point & nsct, const segment & sg ) {
    point c = ( nsct - sg.right ) / ( sg.left - sg.right );
    return c.imag() == 0 && c.real() >= 0 && c.real() <= 1;
  };

  constexpr auto flip = []( char & c ){
    return ( c == 'H' ? 'V' : 'H' );
  };

  /*
    change following intersection topology into the initial state of remove_intersection :

            ^(1)
       (2)  |
       >----|--->
            |
            ^ 
   */
  void BimapMaze::add_intersection( const point& nsct ){
  
    auto sg1 = find_if( path.begin(), path.end(), [&nsct] ( const auto & sg ) { return sg_intersect( nsct, sg ); } ); 
    auto sg2 = sg1; 
    sg2 = find_if( ++sg2, path.end(), [&nsct] ( const auto & sg ) { return sg_intersect( nsct, sg ); } );


    assert( sg1 != path.end() && sg2 != path.end() );
    assert( sg1->info != sg2->info );

    int k = distance(sg1, sg2);
    assert( k > 0); 

    point s = sg1->left, n = sg1->right, w = sg2->left, e = sg2->right;

    path.replace( sg1, segment( s, nsct, sg1->info) );
    path.insert( ++sg1, segment( nsct, w, sg2->info ) ); // insert before pos.

    path.replace( sg2, segment( nsct, e, sg2->info) );
    sg2 = path.insert( sg2, segment( n, nsct, flip(sg2->info) ) ).first; // return pos of inserted value.

    size_t l = distance(sg1, sg2);
    assert( l == (size_t)k-1 );

    list<segment> reverse_path;
    while ( sg1 != sg2 ) 
    {
      reverse_path.push_front( segment( sg1->right, sg1->left, sg1->info ));
      sg1 = path.erase( sg1 ); 
    };

    assert( reverse_path.size() == l );
    path.insert( sg1, reverse_path.begin(), reverse_path.end());
  }


  /*
    change following intersection topology into the initial state of add_intersection :

             v
       (1)   |
       <----\\--->
            |   (2)
            ^
      symetry with axe n/s is treated equally.
      other configuration would create a cycle.
   */
  void BimapMaze::remove_intersection( const point& nsct ){

    auto sg_o = path.left.equal_range(nsct);
    assert( distance( sg_o.first, sg_o.second ) == 2 );

    auto sg1 = path.project_up(sg_o.first);
    auto sg2 = path.project_up(++sg_o.first);
    if ( find( path.begin(), sg1, *sg2 ) != sg1 )
      swap(sg1, sg2);

    int k = distance( sg1, sg2 );
    assert( k > 0 );
    assert( sg2->info == sg1->info ); // avoid cycle

    auto w = sg1->right, s = (--sg1)->left; 
    auto e = sg2->right, n = (--sg2)->left;
    
    path.replace( sg1, segment( s, n, sg1->info) );
    sg1 = path.erase( ++sg1 );

    path.replace( sg2, segment( w, e, flip(sg2->info) )); 
    sg2 = path.erase( ++sg2 );


    size_t l = distance(sg1, --sg2);
    assert( l == (size_t)k-2 );
    list<segment> reverse_path;

    while ( sg1 != sg2 ){
      reverse_path.push_front( segment( sg1->right, sg1->left, sg1->info ));
      sg1 = path.erase( sg1 ); 
    }

    assert(reverse_path.size() == l);
    path.insert( sg1, reverse_path.begin(), reverse_path.end());
  }


/* end Maze */


/* start Explorer */ 

  
  void MazeExplorer::visit( const Maze & m ){
    m.accept(*this);
  }


  static constexpr auto is_intersection = [] ( const point & p, const Layout& l ) -> bool {
    //hyp p <: l
    vector<point> dir { NORTH, SOUTH, EAST, WEST };
    return all_of( dir.begin(), dir.end(), [&l, &p] (point d) { 
        return l.contains( p+d );
        });
  };
   
  void IntersectionFinder::visit( const LayoutMaze & lm ){

    for_each( lm.layout().pixels->begin(), lm.layout().pixels->end(), [&] (const pair<row,col> & v) {
        point p = point(v.second, v.first);
        if ( is_intersection(p, lm.layout()) )
          intersections.push_back( p );
        });
  } 



  const vector<point> & IntersectionFinder::get_intersections(){
    return intersections;
  }; 


    
  static constexpr auto alignment_p = [] ( const point p ) -> int {
    return p.real() * p.imag();
  };

  int IntersectionFinder::alignment_parameter(){
    
    return accumulate( intersections.begin(), intersections.end(), 0, [] ( int s, point p ) {
        return s + alignment_p(p);
        });
  }




  char orientation( const direction &d ){
    assert ( d.real() || d.imag() );

    if ( !d.real() )
      return 'V';

    else if ( !d.imag() )
      return 'H';

    throw domain_error( "orientation not found" );
  }


  void VacuumExplorer::after( const oriented_point & dest ){

    auto shared = maze.lock();
    const BimapMaze * bm = static_cast<const BimapMaze *>( shared.get() );
    partial=false;

    auto sg = bm->path.project_up( 
        bm->path.left.find(origin.first));
    assert( orientation(origin.second) != sg->info); // initial condition

    d17_string s;
    // anomaly : for_each loop is infinite 
    while( sg != bm->path.end() )
    {
      d17_char c;
      int l = sg->info == 'H' ? 
        sg->right.real() - sg->left.real() : 
        sg->right.imag() - sg->left.imag() ;

      if ( (l > 0) ^ ( origin.second == NORTH || origin.second == WEST ) )
      {
        c.first = 'L';
        origin.second *= 1i; // turn left
      }
      else
      {
        c.first = 'R';
        origin.second *= -1i; // turn right
      }
      c.second = abs(l);
      s += c;

      if ( sg_intersect( dest.first + dest.second, *sg ) ) 
      {
        partial=true;
        ++sg;
        break;
      }
      ++sg;
    };

    origin.first = (--sg)->right; 

    append(s);
  }


/* end Explorer */



/* start string type */


/* end string type */

/* start StringOptimizer */


  
  bool d17_char::operator==( const d17_char & o ) const { 
    return first == o.first && second == o.second;
  }

  bool d17_char::operator<( const d17_char & o ) const { 
    if ( is_command() ^ o.is_command() )
      return false;
    return first < o.first || ( first == o.first && second < o.second );
  }

  bool d17_char::operator<=( const d17_char & o ) const { 
    return first == o.first && second <= o.second;
  }
  
  bool d17_char::operator>=( const d17_char & o ) const { 
    return *this == o || ( first == 'O' && second <= o.second);
  }
  

  d17_char d17_char::operator-( const d17_char & o ) const {
    assert( first == o.first || o.first == 'O' );
    d17_char c;
    c.first = o.first == 'O' ? first : 'O';
    c.second = second - o.second;
    return c;
  }


  bool d17_char::is_command() const { 
    return first == 'L' || first == 'R' || first == 'O'; 
  };

  size_t d17_char::size() const{
    return ( first != 'O' ? 2 : 0 ) + ( second > 0 ? second > 9 ? 3 : 2 :0); 
  } 

  constexpr auto key_ = [] ( const char a ) { 
    d17_char c;
    c.first = a; 
    c.second = 0;
    return c; 
  }; 

  static d17_char key( const char a ) { return key_(a); };

  constexpr auto dict_ = [] ( int size_dictionary ) { 
    d17_dictionary tmp;
    for( int i = 0; i < size_dictionary; i++ )
    {
      tmp.insert( make_pair(key('A' + i), d17_string()) ) ;
    } 
    return tmp; 
  };


  constexpr auto to_string_ = [] ( const d17_string & s ) {
    stringstream ss;
    bool start = true;
    for ( const d17_string::value_type v : s ){
      if ( !start ) ss << ",";
      else start = false;

      if ( v.first != 'O' ) 
      {
        ss << v.first;
        if ( v.second ) ss << "," << v.second; 
      }
      else 
      {
        if ( v.second ) ss << v.second;
        else start = true;
      }
    }
    ss << endl;
    return ss.str();
  };


  ostream &operator<<(ostream & os, const d17_string & s){ return os << to_string_(s); }


  static string to_string( const d17_string & s ){ return to_string_(s); }


  template<size_t size_word, size_t dictionary_size>
  D17_StringOptimizer<size_word, dictionary_size>::D17_StringOptimizer( const d17_string & s ):sentence(s),dictionary(dict_(dictionary_size)),partial(true),init_length(s.size()){}
   

  
  constexpr auto only_end_with = [] ( const d17_string & main, const d17_string & suf ){ 
      return main.find( suf ) == main.size() - suf.size();
    };

  constexpr auto start_with = [] ( const d17_string & main, const d17_string & pre ) { 
      return mismatch( pre.begin(), pre.end(), main.begin(), main.end() ).first == pre.end(); 
    };

  constexpr auto d17_str_len = []( const d17_string & s ) -> size_t {
    return accumulate( s.begin(), s.end(), 0, [] ( size_t r, const d17_char & c) {
        return r + c.size();
        }) - 1;
    };


  template<size_t size_word, size_t dictionary_size>
  bool D17_StringOptimizer<size_word, dictionary_size>::success(){

    d17_string fw = first_word();

    return 
      (   
          fw.size() == 0 && 
          d17_str_len( sentence ) <= size_word + 1 
      ) 
        ||
      (   
          partial && 
          only_end_with( sentence, fw ) && 
          any_of( dictionary.begin(), dictionary.end(), [&fw] ( auto p ) 
          { 
            return start_with( p.second, fw ); 
          })
      );  
  }



  template<size_t size_word, size_t dictionary_size>
  const d17_string D17_StringOptimizer<size_word, dictionary_size>::first_word(){

    d17_string d_keys = accumulate( dictionary.begin(), dictionary.end(), d17_string(), [](d17_string s, auto de){
        return s + de.first;
        }); 

    size_t _b = sentence.find_first_not_of( d_keys );

    if ( _b > sentence.size() ) return d17_string();

    size_t l1 = 0, l2 = 0;
    auto k = d_keys.begin();
    size_t _e = find_if( sentence.begin() + _b, sentence.end(), [&d_keys,&l1,&l2,&k] (const d17_char & c ) { 
        l2 = l1;
        l1 += c.size(); 
        k = find( d_keys.begin(), d_keys.end(), c);
        return k != d_keys.end() || l1 > size_word+1; 
        }) - sentence.begin(); 

    if ( l1 > size_word+1 &&  k == d_keys.end() && l2 < size_word )
    {
      d17_string s = sentence.substr(_b, _e-_b+1 ); 
      s.back().second = 0;
      return s;
    }

    return sentence.substr(_b, _e-_b );
  }



  constexpr auto dict_full = [] ( const d17_dictionary & dictionary ) { 
    return find_if( dictionary.begin(), dictionary.end(), []( const auto & e ) {
          return !e.second.size();
          }) == dictionary.end();
  };
    

  template<size_t size_word, size_t dictionary_size>
  void D17_StringOptimizer<size_word, dictionary_size>::factorize( const d17_string & A){
    
    if( dict_full(dictionary) )
      throw out_of_range("dictionary full");

    factorize( find_if( dictionary.begin(), dictionary.end(), []( const auto & e ) {
          return !e.second.size();
          })->first, A );
  }



      




  template<size_t size_word, size_t dictionary_size>
  void D17_StringOptimizer<size_word, dictionary_size>::factorize( const d17_char &c, const d17_string & A){

    assert( A.size() );

    assert( A.back().first != 'O' );

    assert( d17_str_len(A) <= size_word );
      //throw invalid_argument("invalid word lengh");


    assert( all_of( A.begin(), A.end(), []( const d17_char & c ) { return c.is_command(); }) );
      //throw invalid_argument("invalid word");


    assert( dictionary.find(c) != dictionary.end() );
    dictionary[c] = A;
    
    for ( size_t i = 0; i + A.size() <= sentence.size(); i++  )
    {

      auto _mis = A.size() > 1 ? 
        mismatch( ++A.begin(), --A.end(), sentence.begin() + i+1 ):
        make_pair( A.begin(), sentence.begin()+i ); 

      if (  A.front() >= sentence[i] && _mis.first == --A.end() && A.back() <= *_mis.second )
      {
        d17_string s { sentence[i] - A.front() , c, *_mis.second - A.back() };

        if ( !s[2].second ) s.erase(2,1); 
        sentence.replace(i, A.size(), s);
        if ( !s[0].second && s[0].first == 'O' ) sentence.erase(i,1);
        else i++;
      }

      if ( A.size() == 1 )
      {
        if ( A.front() >= sentence[i] )
        {
          d17_string s { sentence[i] - A.front() , c};
          sentence.replace(i, 1, s);
          i++;
        }
        else if ( A.back() <= sentence[i] )
        {
          d17_string s {c,  sentence[i] - A.back() };
          sentence.replace(i, 1, s);
        }
      }
    }
  }



  template<size_t size_word, size_t dictionary_size>
  void D17_StringOptimizer<size_word, dictionary_size>::develop(){

    const auto _e = [] ( auto p ) { 
      return !p.second.size();   
    };

    auto f = find_if(dictionary.begin(), dictionary.end(), _e);

    if ( f == dictionary.begin() )
      throw out_of_range("dictionary is empty");

    develop( (--f)->first );
  }


  constexpr auto join_char = [] ( d17_string & s, size_t & i ) {
    if ( i && s[i-1].is_command() && s[i].first == 'O' ) { 
      s[i-1].second += s[i].second; 
      s.erase(i--,1); 
    }
  };

  template<size_t size_word, size_t dictionary_size>
  void D17_StringOptimizer<size_word, dictionary_size>::develop( const d17_char & c){


    assert( dictionary.find(c) != dictionary.end() );

    for ( size_t i = 0; i < sentence.size(); )
    {
      if ( sentence[i] == c )
      {
        sentence.erase(i,1);
        sentence.insert(i, dictionary[c]);

        join_char(sentence, i); 

        i+=dictionary[c].size();

        join_char(sentence, i);
      }
      else
        i++;
    }
    dictionary[c] = d17_string();
  }



  template<size_t size_word, size_t dictionary_size>
  void D17_StringOptimizer<size_word, dictionary_size>::append( const d17_string & s ){

    sentence += s;
    
    stack<d17_string> stack;

    const auto _push = [&] ( const auto & e ) { 
      if ( e.second.size() )
      {
        stack.push(e.second);
        develop(e.first);
      }
    };

    for_each( dictionary.rbegin(), dictionary.rend(), _push );   

    while( !stack.empty() ) 
    {
      factorize( stack.top() ); 
      stack.pop();
    }

    optimize();

    /*
     * case first fiece was too small
    if ( init_length < size_word * dictionary_size && !success() ) 
    {
      init_length = sentence.size();
      optimize();
    }
    */
  }




  //  depth-first tree crossing
  //  a.k.a back-tracking algorithm 
  //  classic for regexp match
  //
  template<size_t size_word, size_t dictionary_size>
  void D17_StringOptimizer<size_word, dictionary_size>::optimize( const d17_char & k ){
    
    d17_string fw = dictionary[k];

    if ( !fw.size() ) fw = first_word();

    else develop( k );

    while( fw.size() && fw.back().first != 'O' )
    {
      factorize( fw );

      auto next_key = ++dictionary.find(k);

      if (  next_key != dictionary.end()  ) optimize( next_key->first );

      if ( success() ) break; 

      develop();

      if ( fw.back().second ) fw.back().second -= 2;

      else fw.pop_back();
    } 
  }

  static d17_string base_cmd( const optmzr & opt ){
    optmzr opt_1 = opt;
    for( int i = 0; i < 3; i++ ){
      try{
        opt_1.develop();
      }
      catch( const out_of_range & )
      { 
        break; 
      }
    }
    return opt_1.sentence;
  }


  template<size_t size_word, size_t dictionary_size>
  ostream &operator<<( ostream & out, D17_StringOptimizer<size_word, dictionary_size> & so  ) {
    out << so.sentence;
    for( auto w : so.dictionary ) out << w.second;
    return out;
  }

/* end StringOptimizer */

