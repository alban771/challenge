#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <complex>
#include <numeric>
#include <chrono>
#include <thread>

#include "IntCode.h"
#include "Frame.h"
#include "day-17.h"


using namespace std;


#define INTCODE_TO 200
/* start acquisition */


  class ShipHull : public Frame {

  public:
    oriented_point robot;

  private:
    Buffer<33,51> tui_buf;

    Layout& scaffold(){
      return layout[0];
    }; 

    point& origin(){
      return *Frame::origin;
    }

  public:

    ShipHull( unique_ptr<Layout[]> l, const int lc, const shared_ptr<point> o, oriented_point rbt = make_pair(0,0)):Frame(o,std::move(l),lc),robot(rbt){}

    
    friend 
    void operator>>( string s, ShipHull& sh ){
      string l;
      stringstream ss(s);
      point cur = 32i;
      while( ss >> l, ss.good() ){
        cur.real( sh.origin().real() );
        for( char c : l ){
          switch (c) {
            case '#': 
              sh.scaffold().push_back(cur); 
              break; 
            case '<': 
              sh.robot.first = cur;  
              sh.robot.second = WEST;  
              break;
            case '>': 
              sh.robot.first = cur; 
              sh.robot.second = EAST; 
              break;
            case 'v':
              sh.robot.first = cur; 
              sh.robot.second = SOUTH;  
              break;
            case '^': 
              sh.robot.first = cur; 
              sh.robot.second = NORTH;  
              break;
            case 'X': 
              sh.robot.first = cur; 
              sh.robot.second = 0;  
              break;
            default: {}
          }
          cur+=1;
        }
        cur-=1i;
      }
    }

    friend 
    ostream &operator<<( ostream & out, ShipHull & sh ){
      sh.tui_buf << *static_cast<Frame *>(&sh) << sh.robot;
      return out << sh.tui_buf;
    }
  };


  static const auto shiphull_builder = [] () -> shared_ptr<ShipHull> {
    unique_ptr<Layout[]> layout( new Layout[1]{ Layout('#') });  
    shared_ptr<point> origin=make_shared<point>(0); 
    return make_shared<ShipHull>(move(layout),1,origin);
  };


  string operator>>( BasicIntCodeIO<int>& cpout, string& s ){
    char k = cpout.get();
    bool to;
    while( s.push_back(k), k = cpout.get( INTCODE_TO, to), !to );
    return s;
  };

  string operator>>( BasicIntCodeIO<int>& cpout, unsigned long& dust ){
    string s;
    while( dust = cpout.get(), dust < 128  ) {
      s.push_back((char) dust);
    };
    return s;
  }

  BasicIntCodeIO<int>& operator<<( BasicIntCodeIO<int>& cpin, const string& s ){
    for( char k : s ) { cerr << (int) k ; cpin.put((int) k); }
    return cpin;
  };

/* end acquisition */

#ifdef GOLD

#define H_MEDIAN 36
#define V_MEDIAN 14

/* start gold details */


  // Split the maze in part with exactly one entrance and one exit 
  // given this maze shape, it always has 3 intersections 
  // and 17 possible way to cross it. 
  //
  template<int h_median = H_MEDIAN, int v_median = V_MEDIAN>
  class Grove {

    static constexpr point p_median = point(h_median, v_median);

    int n_cycle;
    static constexpr array<bitset<3>,8> cycle { 
      000,
      100,
      110,
      111,
      011,
      111,
      101,
      100
    };


  public:
    vector<point> nscts;
    direction ortn; 

    Grove(){}
    Grove( const vector<point> &intersection, const point &orientation):n_cycle(0),nscts(3),ortn(orientation){

      copy_if( intersection.begin(), intersection.end(), nscts.begin(), [&](const point& p)
        {
          double c = arg(p - p_median) - arg(orientation); 
          return cos(c) < epsilon && sin(c) < -epsilon;
        });

      sort( nscts.begin(), nscts.end(), [&] ( const point &p1, const point &p2 ) 
        {
          point c = p2 - p1; ;
          return c.real()*ortn.real() + c.imag()*ortn.imag() > 0;
        });
    }


    bool next_path( BimapMaze & bm ){

      int i = n_cycle % cycle.size();
      int j = n_cycle++ / cycle.size();

      int m = (cycle[i] ^ cycle[ (i+1) % cycle.size()]).to_string().find('1');
      
      if ( ! cycle[i].test(2-m) ){  // builder & to_string reverse bit order
        bm.add_intersection( nscts[(m+j) % 3] );
      } else {
        bm.remove_intersection( nscts[(m+j) % 3] );
      }

      return find( cycle.begin(), cycle.end(), cycle[(i+1) % cycle.size()]) == cycle.begin() + (i+1) || n_cycle % 24 == 8;  
    }

    oriented_point exit() const {
      return make_pair(nscts[2], ortn);
    }
  };

/* end gold details */

#endif

int main(){

  shared_ptr<ShipHull> sh = shiphull_builder();

  IntCode<int> ASCII("day-17.input"); 

  ASCII.compute();

  string sinit;
  *ASCII.out >> sinit >> *sh;
  ASCII.join();

  cerr << *sh;

  auto l_maze = make_shared<LayoutMaze>( *static_cast<Frame *>(sh.get()) ); 

  IntersectionFinder nsct_f( *l_maze );

#ifndef GOLD

  cout << "solution silver: " << nsct_f.alignment_parameter();

#else

  auto bm_maze = make_shared<BimapMaze>( *l_maze, sh->robot );

  vector<Grove<>> groves(4);
  {
    direction _d = -1; 
    generate( groves.begin(), groves.end(), [&nsct_f, &_d]() {
        _d *= 1i;
        return Grove(nsct_f.get_intersections(), _d);
        });
  }

  list<VacuumExplorer> vacuum_e_pool { { *bm_maze, sh->robot } };
  for ( Grove g : groves ) 
  {       // treat groves sequentially

    list<VacuumExplorer> next_pool;
    for ( const VacuumExplorer v : vacuum_e_pool )
    {     // every previously successfull explorer

      for ( int i = 0; i < 24; i++ ) 
      {   // number of manipulations to get every paths in a grove

        if ( g.next_path( *bm_maze ) )
        {
          VacuumExplorer vv = v;
          vv.after( g.exit() ); 
          if ( vv.success() )
            next_pool.push_back( vv ); 
        }
      }
    }

    if ( next_pool.size() ) break;
    vacuum_e_pool = next_pool;
  }


  stringstream ss;

  ss << *find_if( vacuum_e_pool.begin(), vacuum_e_pool.end(), [&bm_maze] ( VacuumExplorer & v ) { 
      v.after();
      return v.success(); 
  });

  cout << ss.str();

  string q, main, a, b, c, cont_feed="n";
  unsigned long dust;

  ss >>> main >> a >> b >> c;

  IntCode<int> ASCII_2("day-17.input");
  ASCII_2.set(0,2);
  ASCII_2.compute();
  
      
  
  for ( string r : { main, a, b, c, cont_feed } )
  {
    *ASCII_2.out >> q;
    cerr << q;
    *ASCII_2.in << r << "\n";
  }

  q = *ASCII_2.out >> dust;
  cout << q;
  cout << "dust collected:" << dust;

  ASCII.join();
#endif

  return 0;
}
