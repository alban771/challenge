#ifndef D17_G
#define D17_G

#include <boost/bimap.hpp>
#include <boost/bimap/list_of.hpp>
#include <boost/bimap/unordered_multiset_of.hpp>

#include <string>
#include <map>
#include <vector>
#include <stack>
#include <memory>
#include <complex>

#include "Frame.h"


using namespace boost::bimaps;

struct origin{};
struct destination{};
struct orientation{};

/*  
 * Map of point accessible from both extrema 
 * oriented 'H' if horizontal and 'V' otherwise
 * ordered as a continuous line  
 **/
typedef bimap<  unordered_multiset_of< tagged< point, origin>>,
                unordered_multiset_of< tagged< point, destination>>,
                list_of_relation,
                with_info< tagged< char, orientation>>
              > path_map;


typedef path_map::value_type segment;


using namespace std;



class MazeExplorer;

/*! \class Maze
 *  \brief Polymorphic data model for a 2D maze 
 *
 *  Agnostic on the process being performed.
 *  Object in the Visitor design pattern, gives a shared pointer of itself to visiting a MazeExplorer.
 */
// Note : public inheritance is necessary to share pointer 
// from devrivated classes
class Maze : public enable_shared_from_this<Maze>{
protected:

  friend class MazeExplorer;
  virtual void accept( MazeExplorer & ) const;
};


/*!
 *  \class LayoutMaze
 *  \brief Dense maze accessible positions 
 */
class LayoutMaze : public Maze {

  shared_ptr<Layout[]> layout_data;

public:
  LayoutMaze( const Frame& f ):layout_data(f.layout){}

  const Layout &layout() const; // merge all layouts in one
};



class BimapMaze : public Maze {

public:
  oriented_point origin;
  path_map path;

  /*
public:
  std::shared_ptr<const Maze> get_shared() const {
    return shared_from_this();
  } 
  */

public:

  // cast from LayoutMaze
  BimapMaze( const LayoutMaze &, const oriented_point & );
  BimapMaze( const BimapMaze & );

  bool operator==( const BimapMaze &) const;

  void add_intersection( const point& );

  void remove_intersection( const point& );
};




/*!
 * \class MazeExplorer 
 * \brief Polymorphic processor on maze
 *
 * May handle several maze representation
 * Visitor class in the visitor design pattern, stores a weak_ptr of visited maze, that will be lock on concrete computation. 
 * This way, MazeExplorer is not owner of any Maze, the latter can then be wiped out once computation is performed.    
 */
class MazeExplorer {
protected:
  weak_ptr<const Maze> maze;
  friend class Maze;
  virtual void visit( const Maze &);

public:
  MazeExplorer(){}
  MazeExplorer( const MazeExplorer & me ):maze(me.maze){}
};



/*!
 * \class IntersectionFinder
 * \brief Identifies intersections and perform hash on the list 
 */
class IntersectionFinder : public MazeExplorer {

protected:
  vector<point> intersections;

public:
  IntersectionFinder(){}
  IntersectionFinder( const LayoutMaze & lm){ visit( lm );}

  virtual void visit( const LayoutMaze & ); // find intersections
  const vector<point> & get_intersections(); // getter
  int alignment_parameter(); // compute hash
};



struct d17_char { 
  char first;
  size_t second;

  bool operator==(const d17_char & o) const;

  bool operator<(const d17_char & o) const;

  bool operator<=( const d17_char & o) const; // is prefix
  bool operator>=( const d17_char & o) const; // is suffix

  d17_char operator-(const d17_char & ) const;

  size_t size() const;

  bool is_command() const;
};

// for gdb to access dictionary
static d17_char key( const char a );

typedef basic_string<d17_char> d17_string;

typedef map<d17_char, d17_string> d17_dictionary;

ostream &operator<<(ostream &, const d17_string & );

// for gdb to print d17_strings
static string to_string( const d17_string & );

/*! \class D17_StringOptimizer
 *  \brief Refactor string with a templated number of words, 
 *    and maximum length
 */
template<size_t size_word, size_t size_dictionary>
class D17_StringOptimizer {

public:

  d17_string sentence;
  d17_dictionary dictionary;
  bool partial;

private:
  size_t init_length;
  void optimize( const d17_char & k = key('A') );

  /*
   * get the first word not yet factorized in the sentence 
   */
  const d17_string first_word();

  /* 
   * factorize a word from the sentence into the dictionary
   * throw out_of_bound exception if dictionary is full
   * throw illegal_argument exception if word is not in sentence
   */
  void factorize( const d17_string & ); 
  void factorize( const d17_char &, const d17_string & ); 

  /*
   * remove a word from dictionary 
   * and restore the sentence accordingly 
   * throw out_of_bound exception if dictionary is empty 
   */
  void develop( const d17_char & ); 

public:

  D17_StringOptimizer( const d17_string &cmd = d17_string()); 
  D17_StringOptimizer( const D17_StringOptimizer & o ):sentence(o.sentence),dictionary(o.dictionary),partial(o.partial){} 

  void append( const d17_string & cmd);

  bool success(); // every condition are verified

  void develop(); 
 };


#define SIZE_WORD 20
#define SIZE_DICTIONARY 3
typedef D17_StringOptimizer<SIZE_WORD, SIZE_DICTIONARY> optmzr;


// debug puposes
// copy and develop current optmzr
// shall be invariant 
static d17_string base_cmd( const optmzr & );


template<size_t size_word, size_t size_dictionary>
ostream &operator<<( ostream &, const D17_StringOptimizer<size_word, size_dictionary> & );


/*
 *  Maze explorer,
 *  compute command for the vacuum robot   
 */
class VacuumExplorer : public MazeExplorer, public optmzr {
  oriented_point origin;

public:
  VacuumExplorer( const BimapMaze & bm, oriented_point origin):origin(origin),optmzr(){ 
    visit(bm);
  }
  VacuumExplorer( const VacuumExplorer & ve ):origin(ve.origin),optmzr(ve),MazeExplorer(ve){}

  void after( const oriented_point & p = make_pair(0,0) );
};



#include "day-17-impl.cpp"
#endif
