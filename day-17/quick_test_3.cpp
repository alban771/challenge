using namespace boost::numeric::ublas;


static const auto build_m_trim = [] ( const char[] move_f_name, const string& main ) -> matrix
{ 
  matrix m_trim( move_f_capacity, move_f_capacity );
  int i=0,j=0;
  smatch m;
  string pat, act;

  for ( char c1 : move_f_name )
  { 
    j=0;
    for ( char c2 : move_f_name )
    {
      pat = c1 + "\\d*" + c2;
      regex_search( main, m, pat );

      if ( !m.empty() )
      { 
        act = m[0].str();

        if ( !all_of( m.begin(), m.end(), [&act]( auto mm ) 
          {
            return mm.str() == act;
          })) 
          throw std::exception("Mismatch trim coefficient.");  

        m_trim(i,j) = stoi( act.substring(1, act.size()-2) );
      }
      j++
    }
    i++;
  } // end build

  return m_trim;
};


template<size_t length_max, size_t move_f_capacity>
bool D17_ConstrainedMobileDriver::trim_move_f(){

  try 
  {
    matrix m_trim = build_m_trim( move_f_name, main );
  } 
  catch (...) 
  {
    return false; // parsing error
  }

  matrix_column<trim_mat> preA ( m_trim, 0 );

  std::transform( preA.begin(), preA.end(), move_f.begin(), move_f.begin(), [&move_f_name] ( int v, string& mf )
      {
        size_t pos = mf.rend() - std::find_first_of( mf.rbegin(), mf.rend, move_f_name.begin(), 
            move_f_name.end() );
        int d = std::stoi( mf.substr(pos) ); 
        mf.replace( pos , mf.size()-pos, std::to_string( d+v ) );
        return mf;
      }); 

  matrix<bool> mask(m_trim);
  trim_mat -= element_prod( outer_prod( preA, scalar_vector(move_f_capacity, 1) ), mask ); 

}

