#ifndef FRAME
#define FRAME

#include <numeric>
#include <complex>
#include "../lib/IntCode/IntCodeIO.h"

typedef complex<double> point; 
typedef complex<double> direction; 
typedef pair<point, direction> oriented_point;

constexpr direction NORTH = direction( 0, 1 ); 
constexpr direction SOUTH = direction( 0, -1 ); 
constexpr direction EAST = direction( 1, 0 ); 
constexpr direction WEST = direction( -1, 0 ); 

constexpr double Pi = 3.14159;
constexpr double epsilon = pow( 10, -5);

//typedef int col;
//typedef int row;
//
//template<size_t height, size_t width>
//struct Buffer {
//
//  std::array<std::string,height> dsply;
//  atomic<size_t> i;
//
//  point offst;
//
//  Buffer():i(0){
//    generate(dsply.begin(),dsply.end(),[](){ return std::string(width, ' '); });
//  }; 
//
//  /* load the string into the buffer, 
//   * stores on line indicated by offst.r and increment offst.r 
//   * performs padding to the value offst.c 
//   * expect positiv value for offst
//   * overflow is discarded
//   * */
//  Buffer<height, width>& operator<<( const string& );
//};
//
//template<size_t height, size_t width>
//ostream& operator<<( ostream&, const Buffer<height,width>& );
//
//template<size_t height, size_t width>
//Buffer<height, width>& operator<<( Buffer<height, width>&, const oriented_point & );
//
//template<size_t height, size_t width>
//Buffer<height, width>& operator<<( Buffer<height, width>&, const pair<char, point> & );
//
//
//
///*
// * Contains fixed point or map of pixels
// **/
//struct Layout {
//
//  const char color;
//  const bool is_map;
//  
//  union {
//    unique_ptr<std::multimap<row,col>> pixels;
//    unique_ptr<point> fixed_pt;
//  };
//
//  // Single point constructor
//  Layout(const char c, unique_ptr<point>& pt):color(c),is_map(false),fixed_pt(std::move(pt)){};
//  Layout(const char c, point f):color(c),is_map(false),fixed_pt(make_unique<point>(f)){};
//
//  // Map constructor
//  Layout(const char c = ' '):color(c),is_map(true),pixels(make_unique<std::multimap<row,col>>()){};
//  Layout(const char c, unique_ptr<std::multimap<row,col>>& px):color(c),is_map(true),pixels(std::move(px)){};
//  Layout(const char c, std::vector<point>& pix );
//
//  ~Layout();
//
//  Layout(const Layout& l):color(l.color),is_map(l.is_map){
//    if ( is_map ){
//      pixels = make_unique<std::multimap<row,col>> ( *l.pixels.get());
//    } else {
//      fixed_pt = make_unique<point>( *l.fixed_pt.get());
//    }
//  }
//
//  void push_back( const point& );
//  bool contains ( const point& ) const;
//};
//
//
//template<size_t _h, size_t _w>
//Buffer<_h,_w> & operator<<( Buffer<_h,_w> &, const Layout & ); 
//
//
///*
// * Aggregate several Layout
// **/
//class Frame {
//  static constexpr char blank = ' ';
//
//  struct Line {
//     const point offst;
//     std::map<int,char> line;
//
//     Line( const point& o, const Layout& l = Layout(Frame::blank));
//     Line( const Line& );           // used for aggregate alg 
//     Line operator=( const Line& ); // used for aggregate alg
//
//     /* assert lines of same origin 
//      **/
//     void operator+=( const Line& );
//  };
//
//protected:
//  const std::shared_ptr<point> origin; 
//  const int lc;
//
//public:
//  const std::shared_ptr<Layout[]> layout; 
//
//protected:
//  template<size_t h, size_t w>
//  friend Buffer<h,w>& operator<<( Buffer<h,w>&, const Frame::Line& );
//
//  template<size_t h, size_t w>
//  friend Buffer<h,w>& operator<<( Buffer<h,w>&, const Frame& );
//
//public:
//  Frame( const std::shared_ptr<point> o, std::shared_ptr<Layout[]> l, const int lc = 1):origin(o),lc(lc),layout(l){};
//
//
//  /* cast operator : 
//   * allow Argument Dependent Lookup for operatior <<
//  Frame( const Frame& f ){
//    this = &f;
//  }
//   **/
//
//  //void set( const std::shared_ptr<point> o, const std::shared_ptr<Layout[]> l, const int lc);
//
//};
//
///*
//template<int h, int w>
//Buffer<h,w>& operator<<( Buffer<h,w>&, const Frame::Line& );
//
//template<int h, int w>
//Buffer<h,w>& operator<<( Buffer<h,w>&, const Frame& );
//*/
//
//
//
//#include "Frame.cpp"
#endif
//
//
//
