#ifndef MAZE_VISITOR
#define MAZE_VISITOR

#include <cstdlib>

/**
 * First introduced in day-17
 */

using namespace std;


template< class maze_t>
class MazeExplorer;

/*
 * enable_shared_from_this<Maze>() permits any derivated class to return a shared_ptr<Maze> reference of itself
 * Note : <u>public</u> inheritance is necessary to inherit the shared_from_this() method
 */
class Maze : public enable_shared_from_this<Maze>{
public:

  template<class maze_t>
  void accept( MazeExplorer<maze_t> & ) const;

};


template< class maze_t >
void Maze::accept( MazeExplorer<maze_t> & e ) const {
  e.maze = weak_from_this();
  e.visit( *static_cast<const maze_t *>(this));
}



/*
 * class template allows a specialization on Maze type  
 * inheritance can then be performed on one or several abstract MazeExplorer, in order to target specific Maze implementations
 * Todo : enable only maze_t derivated from Maze 
 *
 * A shared pointer will keep the maze alive during visit(), do_visit() perform the actual visitor behavior. 
 * Main process shall not care about the maze lifecycle. 
 */
template< class maze_t >
class MazeExplorer {
protected:

  weak_ptr<const Maze> maze;

  virtual void visit( const maze_t &);  

  virtual void do_visit( const maze_t &) = 0;

  friend class Maze;

};




template<class maze_t>
void MazeExplorer<maze_t>::visit( const maze_t & m ){

  //cerr << "Intersection finder visiting" << endl;
  shared_ptr<const Maze> maze_lck;

  if ( maze.expired() ) 
  {
    // maybe maze reference in argument has expired 
    cerr << "The maze has expired" << endl;
    return;
  } 
  else
    maze_lck = maze.lock();

  do_visit( m );
}


//  the number of step performed
template<class explorer_t, size_t sample_sz>
static struct stats {

  int qty, score = -1;
  explorer_t best, sample[sample_sz];

  void operator<<( const explorer_t & );
};

template<class explorer_t, size_t sample_sz>
ostream & operator<<( ostream & out, const stats<explorer_t, sample_sz> & );



template<class maze_t>
class DepthFirstExplorer : public MazeExplorer<maze_t>{

  typedef DepthFirstExplorer<maze_t> explorer_t;

  stats<explorer_t, 10> stats;

protected:
  virtual void do_visit(); 

  virtual void get_stats();

  // choose one path and return an 
  // explorer without this choice if there are more choices
  virtual optional<explorer_t> next_step() = 0; 

  virtual void do_step() = 0;

  virtual void swap( explorer_t & ) = 0;

public:
  // pass one maze. 
  DepthFirstExplorer( weak_ptr<maze_t> m ) : maze( m ) {}
  DepthFirstExplorer( const explorer_t & v ) : maze( v.maze ) {}

  // how good was this way 
  virtual int score() const = 0;

  // found the exit
  virtual bool success() const = 0; 

};


namespace std 
{
  template<class maze_t>
  struct less<DepthFirstExplorer<maze_t>>
  {
    bool operator()( const DepthFirstExplorer<maze_t> & v1, const DepthFirstExplorer<maze_t> & v2 )
    {
      return v1.score() < v2.score();
    }
  };
}


template<class explorer_t, size_t sample_sz>
bool stats<explorer_t, sample_sz>::operator<<( const explorer_t & exp )
{

  if ( qty < sample_sz || randint( 0, qty ) < sample_sz )
    sample[qty % sample_sz] = exp;

  ++qty;

  if ( score <= 0 || score > exp.score() )
  {
    score = exp.score();
    best = exp;
    return true;
  }
  return false;
}


template<class explorer_t, size_t sample_sz>
ostream & operator<<( ostream & out, const stats<explorer_t, sample_sz> & )
{
  out << "score: " << score << " effectif: " << qty << endl; 
  out << "solution: " << best << endl;
} 



template<class maze_t>
void DepthFirstExplorer<maze_t>::do_visit()
{

  list<explorer_t> next_visitor;

  const auto push_if_value = [&next_visitor] ( const optional<explorer_t> & s ){
    if ( s.has_value() )
      next_visitor.push_back( s.value() );
  };

  while ( !success() )
  { 
    push_if_value( next_step() );
    do_step();
  }

  if( stats << *this ) 
    cerr << stats;

  for_each( next_visitor.rbegin(), next_visitor.rend(), 
      []( explorer_t & v1 ){
          v1.do_visit();
      });

  const auto mv = min_element( next_visitor.begin(), next_visitor.end() );

  if (  next_visitor.size() && score() < mv->score() )
      swap( *mv ); 
}




#endif
