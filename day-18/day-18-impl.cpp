#include "day-18.h"
#include <iostream>

constexpr int INPUT_HEIGHT=81;
constexpr int INPUT_WIDTH=81;

using namespace std;

istream &operator>>(istream & in, LayoutVault & v)
{
  string s;
  point _pos(0,INPUT_HEIGHT);
  while( in >> s, s.size() ) 
  {
    _pos -= 1i;
    _pos.real(0);
    for( char c : s )
    {
      _pos += 1;
      if ( c != '#' ) 
        v.tunnel.push_back(_pos);
      if ( 'A' <= c && c <= 'Z')
        v.doors.emplace(c,_pos);
      else if ( 'a' <= c && c <= 'z') 
        v.keys.emplace(c,_pos);
      else if ( c == '@' ) 
        v.position == _pos;
    }
  }

  return in;
}


ostream &operator<<( ostream & out, const LayoutVault & v )
{
  Buffer<INPUT_WIDTH,INPUT_HEIGHT> b;

  b << v.tunnel;  
  for ( auto p : v.keys )  b << p;
  for ( auto p : v.doors )  b << p;
  b << make_pair<const char, point>('@',v.position);

  return out << b;
}


vector<point> find_branch( const LayoutVault & v )
{
  return IntersectionFinder<Layout>(v.tunnel).intersections;
}


