#include <fstream>
#include <memory>

#define TOTAL_KEY 26
#ifndef INPUT
#define INPUT "day-18-input.alt" 
#endif

#include "day-18.h"


struct grove // subtree of the root of the maze
{
  std::shared_ptr<VaultMaze> maze;
  vrtx_d root;
  key_set_t keys;
  
  grove(){};
  grove( std::shared_ptr<VaultMaze> m, vrtx_d r):maze(m),root(r){ 
    keys = get_keys(); 
  };

  key_set_t get_requirement( const key_set_t & );
private:
  key_set_t get_keys();
};





using namespace std;


typedef map<string, key_set_t> req_map;


static vector<grove> groves;

ostream& operator<<( ostream& out, const req_map& reqm )
{
  for ( auto p : reqm )
  {
    out << p.first << " :\t" << p.second;
    if ( p.first.size() > 2 )
    {
      grove g = groves[atoi(&p.first.back())];
      key_set_t res = reqm.at(p.first.substr(0, p.first.size()-2));
      for( auto ir = res.begin(); ir != res.end(); )
      {
        if ( g.keys.find(*ir) == g.keys.end())
          ir = res.erase(ir);
        else 
          ++ir;
      }
      out << "\t-> " << res << endl;
    }
    else
      out << endl;
  }

  return out;
}

const inline void print_parameters(){
  cout << "INPUT: " << INPUT << endl; 
  cout << "TOTAL_KEY: " << TOTAL_KEY << endl; 
#ifdef ASSERT
  cout << "ASSERT: " << boolalpha << ASSERT << endl; 
#endif
}


int main()
{

  print_parameters();

  vault_layout lv;

  fstream(INPUT) >> lv;

  cerr << lv;
  

  std::shared_ptr<VaultMaze> maze = make_shared<VaultMaze>(lv);

  VaultVisitor visitor( maze );

  auto ier = out_edges( *maze->init.pos, maze->graph ); 
  for( auto out_e = ier.first; out_e != ier.second;  ++out_e ){
      groves.push_back( grove( maze, target( *out_e, maze->graph ) ));
      };


  req_map reqm;
  
  for ( int i = 0; i < groves.size(); ++i ) 
  {
    vector<string> ind = {"r"};
    ind[0].append(to_string(i));
    reqm[ind[0]] = groves[i].get_requirement( groves[i].keys );

    while( ind.size() )
    {
      string _i = ind.back();
      ind.pop_back();
      for ( int k =0; k < groves.size(); ++k )
      {
        key_set_t rk = groves[k].keys;
        for ( auto c = rk.begin(); c != rk.end(); )
        {
          if ( reqm[_i].find(*c) == reqm[_i].end() )
            c = rk.erase(c); 
          else
            ++c;
        };

        if ( !rk.size() )
          continue;

        string _ii = _i;
        _ii.append("r");
        _ii.append(to_string(k));
        reqm[_ii] = groves[k].get_requirement(rk); 
        if ( reqm[_ii].size() ) ind.push_back(_ii);
      }
    }
  }

  cerr << reqm;

  vector<int> grv_order, merge;

  /*
  const auto req_null = []( const auto & p )
      {
        return !p.second.size();
      };

  auto first_null = find_if( reqm.begin(), reqm.end(), req_null ), second_null = first_null; 
  for( int i = first_null->first.size()-1; i>0; --i,--i )
    grv_order.push_back( atoi( &first_null->first[i] ) ); 

  int dummy;
  while ( 1 )
  {
    second_null = find_if( ++second_null, reqm.end(), req_null );
    if ( second_null == reqm.end() )
      break;

    merge.clear();
    for( int i = second_null->first.size()-1; i>0; --i,--i )
      merge.push_back( atoi( &second_null->first[i] ) ); 

    auto it1 = grv_order.begin(), it2 = merge.begin(), it3 = it1; 
    while ( it2 != merge.end() )
    {
      while( it1 != grv_order.end() )
      {
        if ( *it1 == *it2 )
        {
          ++it1;
          ++it2;
          it3 = it1;
        }
        else
          ++it1;
      }
      if ( it2 != merge.end() )
      {
        // subsequence is not contained in previous order
        // choice to insert end of sequence between positions it3 and 0
        it3 = grv_order.insert( it3, *it2 );
        it1 = it3;
      }
    }

  }


  cout << "suggested grv_order #" << grv_order.size() << ": ";
  copy( grv_order.begin(), grv_order.end(), ostream_iterator<int>(cout," ") );
  cout << endl;

  */


  //visitor.visit(); 

  /*while (1)
  {
    string i;
    cin >> i;
    // G 
    
    cout << visitor.score(i) << endl;
  }
  */
}



key_set_t grove::get_keys()
{
  key_set_t s;
  vector<vrtx_d> l = {root};
  const auto vpm = get(vault_vertex_property_tag(), maze->graph );
  do 
  {
    auto oer = out_edges( l.back(), maze->graph );
    l.pop_back();
    for_each( oer.first, oer.second, 
        [&]( auto out_e )
        {
          vrtx_d t = target(out_e, maze->graph);
          l.push_back( t );
          if ( vpm[t].is_key ) s.insert( maze->get_key(t) );
        });
  }
  while (l.size());

  return s;
}


/**
 *  Returns requirement of given key set 
 *    if the keys are found in this grove
 *    if the requirement is not already owned
 */
key_set_t grove::get_requirement( const key_set_t & ks )
{
  key_set_t s;
  for ( char c : ks )
  {
    if ( keys.find(c) == keys.end() )
      continue;

    for( char d : maze->key_pos[c].drs )
    {
      if ( ks.find(tolower(d)) == ks.end() )
        s.insert(tolower(d));
    }
  }
  return s;
}

