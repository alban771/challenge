#ifndef DAY18
#define DAY18

#include <vector>
#include <list>
#include <map>
#include <iostream>
// ref https://www.boost.org/doc/libs/1_72_0/libs/graph/example/adjacency_list.cpp
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/property_map/property_map.hpp>
#include "../include/Layout.h"
#include "../include/Visitor.h"


#ifdef TOTAL_KEY
static constexpr int total_key = TOTAL_KEY;
#else
static constexpr int total_key = 26;
#endif



// constexpr constructor, ref https://en.cppreference.com/w/cpp/language/constexpr
template<int nb_key>
class path_colors_t
{
  const size_t sz = 2*(nb_key + 1);
  char tot_p[2*(nb_key + 1) + 1];

public:
  constexpr path_colors_t() : tot_p() 
  {
    tot_p[0] = '.';
    tot_p[1] = '@';
    for ( int i = 0; i < nb_key; ++i )
    {
      tot_p[2+i] = (char)(97 + i);
      tot_p[2+nb_key+i] = (char)(65 + i);
    }
    tot_p[sz] = '\0';
  }

  constexpr const char* tot() const { return tot_p; }

  constexpr const char * keys() const { return &tot_p[2]; }

  constexpr const char * doors() const { return &tot_p[2+nb_key]; }
};

static constexpr path_colors_t<total_key> pc;
static constexpr TextArg vault_color = { pc.tot() };
typedef FixeLayout<vault_color> vault_layout;

static const string path_colors = pc.tot();
static const string kys( pc.keys(), total_key );
static const string drs = pc.doors();



/* 
 * graph vertices properties
 */

struct vault_vertex_property_tag {
  typedef boost::vertex_property_tag kind;
};


struct vault_vertex_property_value {

  bool is_key, open = true;   // note: bool default to false
  vault_vertex_property_value( bool o = true ):open(o){}

#ifdef ASSERT
  /*
   *  For debug purposes,
   *  keep the position on the frame.
   */
  point pos = point(-1,-1);
#endif
};


/* 
 * choose listS for vertices and edges structure
 * because we have to clean up dead-ends and to orient edges dynamicly.
 */ 
typedef boost::adjacency_list <   
                          boost::listS,             // EdgeList
                          boost::listS,             // VertexList
                          boost::bidirectionalS,    
                          boost::property<vertex_index_t, unsigned_int,
                            boost::property<vault_vertex_property_tag, vault_vertex_property_value> >,
                          boost::property<boost::edge_weight_t, unsigned int>
                      > vault_graph;


typedef boost::property_map<vault_graph, vault_vertex_property_tag>  v_property_map;
typedef boost::property_map<vault_graph, boost::edge_property_tag>  e_property_map;
typedef property_map< vault_graph, vertex_index_t >::type index_map;

typedef vault_graph::vertex_descriptor vrtx_d;
typedef vault_graph::edge_descriptor edge_d;

typedef boost::graph_traits<vault_graph>::vertex_iterator vrtx_i;
typedef boost::graph_traits<vault_graph>::edge_iterator edge_i;
typedef boost::graph_traits<vault_graph>::out_edge_iterator edge_out;
typedef boost::graph_traits<vault_graph>::in_edge_iterator edge_in;

typedef std::set<char> key_set_t; 
struct pos_t {
  const vrtx_i pos;
  key_set_t drs;
  pos_t(){} //default -> map operator[]
  pos_t( vrtx_i p ):pos(p){}
};

typedef std::map<char, pos_t> key_pos_t; 


class VaultVisitor;

/*  allow fast access to:
 *  least common ancestor between two vertices, 
 *    root is initial position
 *    vertices are : keys, doors and intersections of the map
 *  every key vertices,
 *  every locked doors upstream a vertex, ( bottom-up )
 *  every accessible keys from a position, ( top-down )
 * */
class VaultMaze : public Maze
{
  template<typename comp>
  void add_edges_by_line( const Layout & l, const std::map<point, vrtx_d, comp> & m );
  void simplify_edges();
  void flip_in_edges( const vrtx_d & n, const vrtx_d & ori );
  void simplify_origin();
  std::list<vrtx_d> order_graph();
  void set_key_requirement();

public:
  vault_graph graph;
  key_pos_t key_pos;
  pos_t init;

public:
  VaultMaze( const vault_layout & );

  key_set_t available_keys();

  char get_key( const vrtx_d );

  char get_door( const vrtx_d );

  key_set_t get_deepest(const vrtx_d &);

};




template<int dim>
struct path_length_cache
{ 
  int arr[dim][dim];

  path_length_cache( const VaultVisitor &, const VaultMaze & );

  int get(const char, const char);
};



struct avbl_key_cache
{
  map<char, key_set_t> reverse;
  map<char, key_set_t> normal;

  avbl_key_cache(const VaultMaze & );

  key_set_t get( const char, const key_set_t & );
};



class VaultVisitor : public DepthFirstExplorer<VaultMaze,VaultVisitor> 
{

  typedef path_length_cache<total_key+1> cache_path_t;
  typedef avbl_key_cache cache_key_t;

  std::vector<char> path, scout;
  key_set_t key_set, avbl;
  char _next_key;

  std::shared_ptr<cache_path_t> plc;
  std::shared_ptr<cache_key_t> akc;

public:
  VaultVisitor( std::shared_ptr<VaultMaze> m ): path({'@'}),avbl(m->available_keys())
  {
    scout.resize(avbl.size());
    copy(avbl.begin(), avbl.end(), scout.begin());
  }

  VaultVisitor( const VaultVisitor& v ): DepthFirstExplorer<VaultMaze,VaultVisitor>(v)
                                        ,path(v.path),key_set(v.key_set),avbl(v.avbl),scout(v.scout)
                                        ,plc(v.plc),akc(v.akc){}

  /*
  VaultVisitor( VaultVisitor& v ):Visitor<VaultMaze,VaultVisitor>(v),path(v.path),key_set(v.key_set)
  {
    _o.swap( v._o );
  };
  */

  virtual int score() const;

  virtual bool success() const;

  int score( const string& ) const;

  int score( const vector<char>& ) const;

  virtual string show_path() const;

  bool operator<( const VaultVisitor & );

  key_set_t get_key_set() const;

  int go_from_to( const vrtx_d, const vrtx_d, const VaultMaze & ) const;
protected:

  virtual std::optional<VaultVisitor> next_step( const VaultMaze & );

  virtual void do_step( const VaultMaze & );

  virtual void swap( VaultVisitor & );
};



std::ostream & operator<<( std::ostream &, const key_set_t & );

std::ostream & operator<<( std::ostream &, const VaultVisitor & );



/*
 * Divide input graph in 4 quarters, and figure out the
 * best order to visit them.
 */


typedef boost::adjacency_list < boost::listS, boost::listS,
                          boost::directedS,    
                          boost::property< boost::vertex_rank_t, unsigned int,        // the quarter nb 
                              boost::property< boost::vertex_potential_t, key_set_t,  // the minimal keyset required 
                                boost::property< boost::vertex_color_t, boost::default_color_type,    // used in dfs algorithm
                              > > > > quarter_graph;


typedef property_map< quarter_graph, vertex_potential_t > potential_map;
typedef property_map< quarter_graph, vertex_rank_t > rank_map;


class QuarterMaze : public Maze {

  // eases the access of elements in the quarter_graph  
  typedef quarter_graph::vertex_descriptor node;
  typedef quarter_graph::edge_descriptor edge;
  typedef graph_traits< quarter_graph >::vertex_iterator  node_it;
  typedef graph_traits< quarter_graph >::edge_iterator  edge_it;

  QuarterMaze( const VaultMaze & );

private:
  quarter_graph quarters;

};


/*
 * vault_graph visitor
 */
struct record_keys: public boost::base_visitor< record_keys >
{
  typedef boost::on_examine_vertex() event_filter;

  record_keys( const vault_graph& g, key_set_t& ks ): index( get( vertex_index, g ) ), keys(ks) {}

  template < class Vertex, class Graph > void operator()(Vertex e, Graph& g);

private:
  const index_map index;
  key_set_t& keys;
}







#include "day-18.impl.cpp"
#endif
