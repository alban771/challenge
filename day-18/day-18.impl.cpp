#include "day-18.h"
#include <iostream>

constexpr int INPUT_HEIGHT=81;
constexpr int INPUT_WIDTH=81;

using namespace std;
using namespace boost;


/*
istream &operator>>(istream & in, LayoutVault & v)
{
  string s;
  point _pos(0,INPUT_HEIGHT);
  while( in >> s, s.size() ) 
  {
    _pos -= 1i;
    _pos.real(0);
    for( char c : s )
    {
      _pos += 1;
      if ( c != '#' ) 
        v.tunnel.push_back(_pos);
      if ( 'A' <= c && c <= 'Z')
        v.doors.emplace(c,_pos);
      else if ( 'a' <= c && c <= 'z') 
        v.keys.emplace(c,_pos);
      else if ( c == '@' ) 
        v.position == _pos;
    }
  }

  return in;
}


ostream &operator<<( ostream & out, const LayoutVault & v )
{
  Buffer<INPUT_WIDTH,INPUT_HEIGHT> b;

  b << v.tunnel;  
  for ( auto p : v.keys )  b << p;
  for ( auto p : v.doors )  b << p;
  b << make_pair<const char, point>('@',v.position);

  return out << b;
}


vector<point> find_branch( const LayoutVault & v )
{
  return IntersectionFinder<Layout>(v.tunnel).intersections;
}

*/


#ifdef ASSERT

// assert vertex descriptors are still valid
void _assert_pos( const vault_graph & g, const key_pos_t & key_pos, const pos_t & init ){

  vrtx_i rg, rg_e, casse;
  tie( rg, rg_e ) = vertices(g);
  assert( rg == init.pos  );
  for ( const auto pos : key_pos )
  {
    assert( pos.second.pos == ++rg);
    ++rg;
  }
}

// assert doors property are correct
void _assert_opensmap( const vault_graph & g, const key_pos_t & key_pos, const key_set_t & keys ){

  vrtx_i casse;
  auto opens = get(vault_vertex_property_tag(), g);  
  for ( auto pos : key_pos )
  {
    bool has_key = find( keys.begin(), keys.end(), pos.first ) != keys.end();
    casse = pos.second.pos;
    assert( !has_key ^ opens[ *++casse ].open );
  }
}

// assert edges are completes
void _assert_edge( const vault_graph & g, const Layout & l ){

  Layout le(".+");
  le.height = l.height;
  edge_i rg, rg_e;
  auto vpm = get(vault_vertex_property_tag(), g);

  for ( tie( rg, rg_e ) = edges(g); rg != rg_e; ++rg ) 
  {
    point p1 = vpm[source(*rg, g)].pos, p2 = vpm[target(*rg, g)].pos; 
    point dlta = p2 - p1;
    assert( dlta != point() );
    assert( dlta.real() == 0 || dlta.imag() == 0 );
    point dir = ( dlta.real() ? dlta.real() > 0 ? 1 : -1 : dlta.imag() > 0 ? 1i : -1i);
    while( p1 += dir, p1 != p2 )
    {
      le.insert(p1);
    }
  }
  
  cerr << "dots inserted : " << le.size('.') << endl;

  vrtx_i vrg, vrg_e;
  for ( tie(vrg, vrg_e) = vertices(g) ; vrg != vrg_e; ++vrg )
  {
    if ( vpm[*vrg].pos != point(-1,-1) )
      le.insert( vpm[*vrg].pos, '+');
  }


  cerr << "angles inserted : " << le.size('+') << endl;

  cerr << le;
  assert( le <= l );

  vector<point> k_pos = l["@" + kys + drs], nodes = le["+"]; 
  assert( all_of( k_pos.begin(), k_pos.end(), 
        [&nodes](const point & p){
          return find(nodes.begin(), nodes.end(), p) != nodes.end();
        }));

  cerr << "assertion success" << endl;
};

#endif




template<typename comp>
void VaultMaze::add_edges_by_line( const Layout & l, const map<point, vrtx_d, comp> & m )
{
  cerr << "detect edges with layout : " << l.size('.') << endl;
  std::map<point, vrtx_d>::const_iterator a_e, a_b = m.begin(), a_tmp; 

  for( int i = 0; i < l.nr_lines() ; ++i )
  {
    for( auto seg : l.get_line_segments(i, path_colors) )
    {

      if ( m.key_comp()( seg.second, a_b->first ) ) continue; // segment avant le premier noeud

      a_b = m.lower_bound( seg.first ); // first point not lesser than the start of segment
      a_e = m.upper_bound( seg.second ); // first point greater than the end of the segment 

      while ( a_tmp = a_b, ++a_b != a_e )
      {
#ifndef ASSERT
        add_edge( a_tmp->second, a_b->second, abs(a_tmp->first - a_b->first), graph );
#else
        point dlta = a_b->first - a_tmp->first;
        assert(dlta != point() && ( !dlta.real() || !dlta.imag() ));
        pair<edge_d, bool> pr = add_edge( a_tmp->second, a_b->second, abs(a_tmp->first - a_b->first), graph );
        assert(pr.second);
#endif
      }
    }
  }
}


void VaultMaze::simplify_edges()
{
  cerr << "simplify graph." << endl;
  cerr << "#vertices:" << num_vertices(graph) << " #edges:" << num_edges(graph) << endl;

  vrtx_i vr, vr_e;
  auto weight = get(edge_weight_t(), graph);  
  for( tie(vr, vr_e) = vertices(graph), vr = key_pos[kys.back()].pos, ++vr, ++vr; vr != vr_e; ++vr )
  {
    auto oer = out_edges( *vr, graph );
    auto ier = in_edges( *vr, graph );
    if ( distance( oer ) + distance( ier ) == 2 )
    {
      int d1 = distance( oer );
      int d2 = distance( ier );
      edge_d e1 = ( distance(oer) > 0 ) ? *oer.first : *ier.first;
      edge_d e2 = ( distance(ier) > 0 ) ? *(--ier.second)++ : *(--oer.second)++;

      int _w = weight[e1]+weight[e2];
      vrtx_d _s = ( distance(oer) > 0 ) ? target( e1, graph) : source(e1, graph);
      vrtx_d _t = ( distance(ier) > 0 ) ? source( e2, graph) : target(e2, graph); 
#ifdef ASSERT
      //size_t _do= distance(oer), di=distance(ier); 
      //vrtx_d s1 = source(e1, graph), s2 = source(e2, graph), t1 = target(e1, graph), t2 = target(e1, graph);
      assert( (distance(oer) == 0 ) ^ ( source(e1, graph) == *vr ) );
      assert( (distance(ier) == 0 ) ^ ( target(e2, graph) == *vr ) );
      auto vx_map = get( vault_vertex_property_tag() , graph);
#endif
      add_edge( _s, _t, _w, graph );
      remove_edge(e1, graph);
      remove_edge(e2, graph);
      vrtx_d _v = *vr;
      --vr;
      remove_vertex(_v, graph);
    }
  }

  cerr << "#vertices:" << num_vertices(graph) << " #edges:" << num_edges(graph) << endl;

}


void VaultMaze::flip_in_edges( const vrtx_d & n, const vrtx_d & ori )
{
  const auto pred = [&]( const edge_d & in ) -> bool {
    return ori != source( in, graph );
  };

  edge_in ir, ir_e;
  const auto weight = get(edge_weight_t(), graph);
  for ( tie(ir, ir_e) = in_edges( n, graph); ir != ir_e; ++ir )
    if ( pred(*ir) )
      add_edge( target(*ir, graph), source(*ir, graph), weight[*ir], graph );

  remove_in_edge_if( n, pred, graph );


#ifdef ASSERT
  /* verbose */
  auto vx_map = get( vault_vertex_property_tag(), graph );
  auto ier = in_edges( n, graph);
  auto oer = out_edges( n, graph);
  if ( distance(ier) != 1 )
  {
    cout << "in edges nb: " << distance(ier) << endl;
    cout << "out edges nb: " << distance(oer) << endl;
    for ( ; ier.first != ier.second; ++ier.first )
    {
      cout << "in edge from:" << vx_map[source(*ier.first, graph)].pos;
      cout << " to:" << vx_map[target(*ier.first, graph)].pos << endl;
    }
    cout << "position:" << vx_map[n].pos << endl;
  }
  if ( vx_map[n].pos == point(29,36) )
  {
    cout << "lettre a" << endl;
    assert( get_key(n) == 'a');
  }
  if (get_key(n) == 'a')
  {
    cout << "lettre a" << endl;
  }
  //*/
  assert( distance(in_edges(n, graph)) == 1 || n == *init.pos );
#endif
}


void VaultMaze::simplify_origin()
{
#ifdef ASSERT
  size_t li_ = distance(in_edges(*init.pos, graph));
  size_t lo_ = distance(out_edges(*init.pos, graph));
#endif

  flip_in_edges( *init.pos, *init.pos );
  vector<vrtx_d> r1(4);
  set<vrtx_d> r2;
  edge_out oe, oe_e;
  tie(oe, oe_e) = out_edges(*init.pos, graph);

#ifdef ASSERT
  size_t li = boost::distance(in_edges(*init.pos, graph));
  size_t lo = boost::distance(oe, oe_e);
  assert( boost::distance(oe, oe_e) == 4 );
#endif

  transform( oe, oe_e, r1.begin(), [&](const edge_d & e){
        vrtx_d t = target(e, graph);
        flip_in_edges( t, *init.pos );
        edge_out ooe, ooe_e;
        for( tie( ooe, ooe_e) = out_edges(t, graph); ooe != ooe_e; ++ooe )
          r2.insert( target(*ooe, graph ) );
        return t;
  });

#ifdef ASSERT
  assert( r2.size() == 4 );
#endif

  for( vrtx_d v : r1 )
  {
    clear_vertex(v, graph);
    remove_vertex(v, graph);
  }
  
#ifdef ASSERT
  assert( distance( out_edges( *init.pos, graph )) == 0  );
#endif

  for( vrtx_d v : r2 )
    add_edge( *init.pos, v, 2, graph );
}


list<vrtx_d> VaultMaze::order_graph()
{
  flip_in_edges( *init.pos, *init.pos );


  list<vrtx_d> visited, to_visit = {*init.pos};
  edge_out oe, oe_e;
  while( to_visit.size() )
  {
    vrtx_d s = to_visit.back();
    to_visit.pop_back();

    for( tie( oe, oe_e ) = out_edges(s, graph); oe != oe_e; ++oe )
    {
      vrtx_d v = target(*oe, graph);
      flip_in_edges( v, s );
      to_visit.push_back(v); 
    }

    visited.push_back(s);
  }

  return visited;
}

char VaultMaze::get_door( const vrtx_d n )
{
  auto s = find_if( key_pos.begin(), key_pos.end(), 
      [&n]( const auto & p )
      {
        vrtx_i casse = p.second.pos;
        return *++casse == n;  
      });
  return s == key_pos.end() ? '.' : s->first;
}

void VaultMaze::set_key_requirement()
{
  const auto vx_map = get( vault_vertex_property_tag(), graph );

  for( key_pos_t::value_type & key : key_pos )
  {
    vrtx_d n = *key.second.pos; 
    auto r = in_edges(n, graph);
    while( distance(r) )
    {
      if ( ! vx_map[n].open )
        key.second.drs.insert( get_door(n) );

      n=source( *r.first, graph );
      r=in_edges(n, graph);
    }
  }
}



VaultMaze::VaultMaze( const vault_layout & l ):graph(2*total_key+1),init(vertices(graph).first){
  
  vrtx_i rg = init.pos;
  auto opens = get(vault_vertex_property_tag(), graph);  
  for ( int i = 0 ; i < total_key; ++i )
  {
    key_pos.emplace( kys[i], ++rg ); 
    opens[*rg].is_key = true;
    opens[*++rg].open = false ;
  }

#ifdef ASSERT
  _assert_pos(graph, key_pos, init);
  _assert_opensmap( graph, key_pos, {} );

  opens[*init.pos].pos = l['@'][0];
  for ( auto pos : key_pos )
  {
    vrtx_i casse = pos.second.pos;
    ++casse;
    if ( l[pos.first].size() )
      opens[*pos.second.pos].pos = l[pos.first][0];
    if ( l[toupper(pos.first)].size() )
      opens[*casse].pos = l[toupper(pos.first)][0];
    cerr << pos.first << " :: " << opens[*pos.second.pos].pos << endl;
    cerr << (char)toupper( pos.first ) << " :: " << opens[*casse].pos << endl;
  }
#endif

  { 
    // todo :: use boost multi index map
    map<point, vrtx_d, less_comp_ornted<'h'>> h_map; 
    map<point, vrtx_d, less_comp_ornted<'v'>> v_map; 

    h_map.emplace(l['@'][0], *init.pos);
    v_map.emplace(l['@'][0], *init.pos);

    for ( const char c : kys )
    {
      auto casse = key_pos[c].pos;
      if ( l[c].size() )
      {
        h_map.emplace(l[c][0], *key_pos[c].pos);
        v_map.emplace(l[c][0], *key_pos[c].pos);
      }

      if ( l[toupper(c)].size() )
      {
        h_map.emplace(l[toupper(c)][0], *++casse);
        v_map.emplace(l[toupper(c)][0], *casse);
      }
    }

    for( const point p : detect_angle( l, path_colors ) )
    {
      if(  h_map.find(p) != h_map.end() ) continue; 

      vrtx_d vt = add_vertex(graph);
      h_map.emplace(p, vt);
      v_map.emplace(p, vt);
#ifdef ASSERT
      opens[vt].pos = p;
#endif
    }

#ifdef ASSERT
    _assert_pos( graph, key_pos, init );
#endif

    add_edges_by_line<less_comp_ornted<'h'>>( l, h_map );
    add_edges_by_line<less_comp_ornted<'v'>>( l.mirror(), v_map );
  }

#ifdef ASSERT
  _assert_edge( graph, l );
  cerr << "assert edges correct" << endl;
#endif

  simplify_edges();

#ifdef ASSERT
  _assert_pos( graph, key_pos, init );
#endif

#ifdef SQUARE_ORIGIN
  simplify_origin();
#endif

  list<vrtx_d> visited = order_graph();

#ifdef ASSERT
  size_t nvr = distance(vertices(graph)), nvs = visited.size();
  
  Layout l__(".01234v");
  l__.height = l.height;
  const auto map_pos = get( vault_vertex_property_tag(), graph); 
  vrtx_i vr, vr_e;
  for ( tie(vr, vr_e) = vertices(graph); vr != vr_e ; ++vr )
    l__.insert(map_pos[*vr].pos, "01234"[distance(out_edges(*vr, graph))]);
  for ( vrtx_d v : visited )
    l__.insert(map_pos[v].pos,'v');
  l__.insert(l['.'],'.');
  cerr << l__;

 assert( visited.size() == distance( vertices( graph )) );
#endif

  set_key_requirement();

#ifdef ASSERT
  _assert_opensmap( graph, key_pos, {} );
  key_set_t f_a = available_keys();
  assert( f_a.size() && f_a.size() < total_key );
  cerr << "first available keys : " << f_a << endl;
#endif
}


key_set_t VaultMaze::available_keys() 
{
  key_set_t owned, avbl;
  auto open = get( vault_vertex_property_tag(), graph );

  const auto is_owned = [&owned]( const char c )
  { 
    return find( owned.begin(), owned.end(), c ) != owned.end(); 
  };

  for ( auto key : key_pos )
  {
    vrtx_i casse = key.second.pos;
    if ( open[*++casse].open ) owned.insert(key.first);
  }

  for ( auto key : key_pos )
  {
    if ( all_of( key.second.drs.begin(), key.second.drs.end(), is_owned ) )
      avbl.insert(key.first);
  }

  return avbl;
}


char VaultMaze::get_key(const vrtx_d d)
{
  auto kp = find_if( key_pos.begin(), key_pos.end(), 
      [&d](const auto & p){ 
              return *p.second.pos == d;
              });
  return ( kp == key_pos.end() ? '.' : kp->first ); 
}


/**
 *  A key a is deepest than a key b, if and only if b is between a and the origin.   
 *
 */
key_set_t VaultMaze::get_deepest( const vrtx_d & p )
{
  list<vrtx_d> node_list = {p};
  key_set_t s; 
  auto vpm = get( vault_vertex_property_tag(), graph ); 
  auto i = node_list.begin();
  edge_out oe, oee;
  vrtx_d d;

  while ( i != node_list.end())
  {
    for( tie(oe, oee) = out_edges( *i, graph); oe != oee; ++oe )
    {
      d = target( *oe, graph );
      if ( !vpm[d].open ) continue;
      if ( vpm[d].is_key ) s.insert( get_key(d) );
      node_list.push_back(d);
    }
    ++i;
  }

  return s;
}







int VaultVisitor::score() const
{
  return score( path );
}

int VaultVisitor::score( const string & _path) const
{
  vector<char> p(_path.size());
  copy(_path.begin(), _path.end(), p.begin());
  return score(p);
}

int VaultVisitor::score( const vector<char> & _path ) const
{
  vector<pair<char, size_t>> plen(_path.size());

  const auto gen_plen = []( const char & n ){
    return make_pair(n, 0);
  };

  const auto diff_plen = [&]( const auto p1, const auto p2 ){
    return make_pair( p1.first, plc->get(p1.first, p2.first) );
  };

  const auto acc_plen = []( int s, const auto p ){
    return s + p.second;
  };

  /* slow
  const auto weight = get(edge_weight_t(), maze->graph);  

  // TODO : case we cross origin
  const auto diff_plen = [&]( const auto p1, const auto p2 ){
    auto r = in_edges(p1.first, maze->graph );

    if ( distance(r) && source( *r.first, maze->graph ) == p2.first )
      return make_pair( p1.first, weight[*r.first] );

    r = in_edges(p2.first, maze->graph );
#ifdef ASSERT
    assert( distance(r) && source( *r.first, maze->graph ) == p1.first );
#endif
    return make_pair( p1.first, weight[*r.first] );
  };
  */

  
  transform( _path.begin(), _path.end(), plen.begin(), gen_plen );

  adjacent_difference( plen.begin(), plen.end(), plen.begin(), diff_plen );

  return accumulate( plen.begin(), plen.end(), 0, acc_plen );
}




bool VaultVisitor::success() const
{
  return key_set.size() == total_key;
}


void VaultVisitor::swap( VaultVisitor & v )
{
  path.swap( v.path );
  key_set.swap( v.key_set );
}


int VaultVisitor::go_from_to( const vrtx_d from, const vrtx_d to, const VaultMaze & maze ) const
{

  const auto weight = get(edge_weight_t(), maze.graph);  
  vector<pair<vrtx_d, int>> to_list = {{to,0}}, from_list = {{from,0}};
  auto ier = in_edges( to, maze.graph );
  const auto find_ancestor = [&from_list]( auto & to_p ) {
        return to_p.first == from_list.back().first;
      };
  const auto acc_weight = []( int s, auto & p ){ return s + p.second; };

#ifdef ASSERT
  auto vx_map = get( vault_vertex_property_tag(), maze.graph );
#endif 


  while( to_list.back().first != *maze.init.pos )
  {
    ier = in_edges( to_list.back().first, maze.graph );
#ifdef ASSERT
    if(distance(ier) != 1 )
    {
      cout << "err go from: " << vx_map[from].pos << " to: " << vx_map[to].pos << endl; 
      cout << "in edges nb: " << distance(ier) << endl;
      for ( ; ier.first != ier.second; ++ier.first )
      {
        cout << "in edge from:" << vx_map[source(*ier.first, maze.graph)].pos;
        cout << " to:" << vx_map[target(*ier.first, maze.graph)].pos << endl;
      }
      cout << "position:" << vx_map[to_list.back().first].pos << endl;
    }
    assert( distance(ier) == 1);
#endif
    to_list.push_back( make_pair( source( *ier.first, maze.graph ),
          weight[ *ier.first ]));
  }
  
  auto anc = find_if( to_list.begin(), to_list.end(), find_ancestor );
      
  while( anc == to_list.end() )
  {
    ier = in_edges( from_list.back().first, maze.graph );
#ifdef ASSERT
    size_t _d = distance(ier);
    point p = vx_map[from_list.back().first].pos;
    assert( _d == 1);
#endif
    from_list.push_back( make_pair( source( *ier.first, maze.graph ), 
          weight[*ier.first] ));
    anc = find_if( to_list.begin(), to_list.end(), find_ancestor );
  }


  return accumulate( from_list.begin(), from_list.end(), 
         accumulate( to_list.begin(), ++anc, 0, acc_weight ),
         acc_weight);
}

template<int dim>
path_length_cache<dim>::path_length_cache( const VaultVisitor & v, const VaultMaze & maze )
{
  for( int i = 0; i < dim; ++i )
  {
    for ( int j = i; j < dim; ++j)
    {
      arr[i][j] = v.go_from_to( i ? *maze.key_pos.at(path_colors[i+1]).pos : *maze.init.pos, 
                                j ? *maze.key_pos.at(path_colors[j+1]).pos : *maze.init.pos,
                                maze );
      arr[j][i] = arr[i][j];
    }
  }
}


template<int dim>
int path_length_cache<dim>::get( const char from, const char to)
{
  return arr[ path_colors.find(from)-1 ][ path_colors.find(to)-1 ];
}


avbl_key_cache::avbl_key_cache( const VaultMaze & maze )
{
  for ( char k : kys )
  {
    reverse.emplace(k, key_set_t({}));
    normal.emplace(k, key_set_t({}));
  }

  for ( char k : kys )
  {
    for ( char D : maze.key_pos.at(k).drs )
    {
      char d = std::tolower(D);
      reverse[d].insert(k);
      normal[k].insert(d); 
    }
  }
}


/**
 * Quick available keys search
 * expect a sorted set
 * remove keys owned
 */
key_set_t avbl_key_cache::get( const char nc, const key_set_t & ks)
{
  key_set_t s;
  for( char r : reverse[nc] )
  {
    if ( ks.find(r) != ks.end() ) 
      continue;

    auto i=ks.begin();
    if ( all_of( normal[r].begin(), normal[r].end(), 
          [&]( const char n)
          {
            while ( *i < n ) ++i;
            return *i == n;
          })) 
      s.insert(r);
  }
  return s;
}


void VaultVisitor::do_step( const VaultMaze & maze )
{
  path.push_back(_next_key); // keep order
  key_set.insert(_next_key); // sort for cache

  avbl.erase(_next_key);
  avbl.merge( akc->get(_next_key, key_set) );

  scout.resize(avbl.size());
  copy(avbl.begin(), avbl.end(), scout.begin());
}


ostream & operator<<( ostream & out, const key_set_t & ks )
{
  if ( !ks.size() ) return out << '/';
  out << "{ ";
  copy( ks.begin(), --ks.end(), ostream_iterator<char>(out, ",") );
  out << *--ks.end() << " }";
  return out;
}



std::optional<VaultVisitor> VaultVisitor::next_step( const VaultMaze & maze )
{

  if ( !akc )
    akc = make_shared<cache_key_t>(maze);

  if ( !plc )
    plc = make_shared<cache_path_t>(*this, maze);

  cerr << "step, path: " << show_path() << endl;
  cerr << "step, avbl: " << avbl << endl;
  cerr << "step, scout: "; 
  copy( scout.begin(), scout.end(), ostream_iterator<char>(cerr) );
  cerr << endl;

  /* slow
  maze->accept(this);
  key_set_t avbl = maze->available_keys();
  */

  /* false
  key_set_t dpst = maze->get_deepest(path.back());
  key_set_t dpst_no; // deepest not owned
  set_difference( dpst.begin(), dpst.end(), key_set.begin(), key_set.end(), inserter(dpst_no, dpst_no.begin()) );
  */

#ifdef ASSERT
  /*
  _assert_opensmap( maze->graph, maze->key_pos, key_set );
  */
  assert( scout.size() );
#endif

  _next_key = scout.back();
  scout.pop_back();

  return scout.size() ? std::make_optional(*this) : nullopt;
}

/*
namespace std 
{
  template<>
  struct less<VaultVisitor>
  {
    bool operator()( const VaultVisitor & v1, const VaultVisitor & v2 )
    {
      return v1.score() < v2.score();
    }
  };
}
*/

bool VaultVisitor::operator<( const VaultVisitor & v )
{
  return score() < v.score();
}


key_set_t VaultVisitor::get_key_set() const
{
  return key_set;
}


string VaultVisitor::show_path() const
{
  /* deprecated
  stringstream ss;
  ss << "< ";
  for_each( path.begin(), path.end(), [&]( const vrtx_d v )
  { 
    const char c = maze->get_key(v);
    if ( c != '.' && ss.str().find(c) == string::npos ) ss << c;
  });
  ss << " >";
  return ss.str();
  */

  string s(total_key+5, ' ');
  s[0]='<';
  s[s.size()-1]='>';
  copy( path.begin(), path.end(), ++s.begin() );
  return s;
}

std::ostream & operator<<( std::ostream & out, const VaultVisitor & v )
{
  return out << v.show_path();
}



/*
 *  Quarters
 */


/*
 * visit vault_graph and record keys
 */
template < class Vertex, class Graph > 
void record_keys::operator()(Vertex e, Graph& g);
{
  unsigned int ind = index[e];
  // vertex order in vault_graph is : @aAbBcCdD...
  if ( ind < 2*total_key + 1 && ind % 2 ) 
    keys.insert( kys[ ind ] );
}




QuarterMaze::QuarterMaze( const VaultMaze & vm )   
{

  // initialize a quarter with potential of every key  
  // contained in the graph downstream the vertex given
  // in argument 
  const auto init_quarter = [&g = vm.graph, rank=0] ( const vrtx_d& root ) 
  {
    key_set_t omg;
    breadth_first_search( g, root, 
        visitor( make_bfs_visitor( record_keys( g, omg ))));
    quarters.add_vertex( rank++, omg );
  };

  {
    edge_i e, end_e;
    tie( e, end_e ) = out_edges( *vm.init.pos, vm.graph );

    // r contains every nodes adjacent to inital position in vault_graph
    vector< vrtx_d > r;
    for_each( r.begin(), transform( e, end_e, back_inserter(r),
          [ &g = vm.graph ]( const edge_d& out_edge ) { return target( out_edge, g ); }), 
        init_quarter ); 
  }


  // range of initial nodes in quarter_graph, hopefully references stay valid
  // whenever a vertex is added
  const node_it full_quarter, end_full_quarter;
  tie( full_quarter, end_full_quarter ) = quarters.vertices();
  
  potential_map potentials = get( vertex_potential_t, quarters );
  rank_map potentials = get( vertex_rank_t, quarters );


  // join the sets of doors before every keys in a potential 
  // is called potential's requirement
  // todo: change the key_pos storage for some vertex property 
  const auto join_requirement = [&vm] ( const key_set_t & ptl )
  {
    key_set_t req;
    for( const char key : ptl )
    {
      const key_set_t& drs = vm.key_pos[key].drs;
      copy( drs.begin(), drs.end(), inserter(req, req.begin()) );
    }
    return req;
  };


  // divide a key set into subsets each contained in a single quarter. 
  const auto split_objectiv = [&] ( const key_set_t & obj )
  {
    vector< pair< unsigned int, key_set_t > > new_vert;

    for_each( full_quarter, end_full_quarter, 
        [&] ( const node & n )
        {
          key_set_t inter;
          set_intersection( potentials[n].begin(), potentials[n].end(), 
              obj.begin(), obj.end(), inserter(inter, inter.begin()) );
          if ( inter.size() )
            new_vert.push_back( make_pair( rank_map[n], inter ) );
        });

    return new_vert;
  };


  list<node> buf;   // contains nodes with requirement not met with children potential 
  copy( full_quarter, end_full_quarter, back_inserter(buf) );
  unordered_map< key_set_t, node > reverse_potentials; // avoid duplicated potentials

  while ( !buf.empty() )
  {
    node curr = buf.front();
    buf.pop_front();
    vector< pair< unsigned int, key_set_t > > new_vert =  split_objectiv( join_requirement( potentials[curr] )); 

    for ( const auto& n : new_vert ) 
    {
      if ( reverse_potentials.find( n.second ) == reverse_potentials.end() )
      {
        node = quarters.add_vertex( n.first, n.second );
        reverse_potentials.emplace( n.second , node );
        buf.push_back( node );
        quarters.add_edge( ancestor, node );
      }
      else
      {
        quarters.add_edge( ancestor, reverse_potentials[ n.second ] );
      }
    }
  }
}






