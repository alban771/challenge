b Layout.h:operator>>(std::istream&, Layout& )

define set_map
  set $node = $arg0._M_t._M_impl._M_header._M_left
  set $end = $arg0._M_t._M_impl._M_header
  set $tree_size = $arg0._M_t._M_impl._M_node_count
  set $map_i = 0
end


# rb-tree, left -> right, ancestors hold keys  
# ref :https://stackoverflow.com/questions/18755530/gdb-cast-memory-address-to-an-stl-object 
define nextnode_map
  if $node._M_right != 0
      set $node = $node._M_right
      while $node._M_left != 0
        set $node = $node._M_left
      end
  else
    set $tmp_node = $node._M_parent
    while $node == $tmp_node._M_right
      set $node = $tmp_node
      set $tmp_node = $tmp_node._M_parent
    end
# test trivial
    if $tmp_node._M_right != $node
      set $node = $tmp_node
    end
  end

  set $map_i = $map_i + 1
  if $map_i == $tree_size
    print "end of map reached"
  end
end
