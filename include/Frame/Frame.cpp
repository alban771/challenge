#include <iterator>
#include <algorithm>
#include <cassert>

using namespace std;


template<size_t h, size_t w>
Buffer<h,w>& Buffer<h,w>::operator<<( const string& s ){
  if ( 
      offst.imag() < (int)h 
      && offst.real() < (int)w 
      && offst.imag() >= 0
      && offst.real() >= 0
      ) 
  {  
    auto se =  (int)s.size() < (int)w - offst.real() ? s.end() : s.begin() + (int)w - offst.real();
    copy( s.begin(), se, dsply[offst.imag()].begin() + offst.real() ); 
  } 
  offst += 1i;
  return *this;
}


template<size_t h, size_t w>
ostream& operator<<( ostream& out, const Buffer<h,w>& buf ){
  for(int i = h-1; i>=0; i--) out << buf.dsply[i] << endl;
  return out << endl;
}


template<size_t height, size_t width>
Buffer<height, width>& operator<<( Buffer<height, width>& b, const oriented_point & op ){
  char ori = 'X';
  if ( op.second.imag() > 0 ){
    ori = '^';
  }
  else if ( op.second.real() > 0 ){
    ori = '>';
  }
  else if ( op.second.imag() < 0 ){
    ori = 'v';
  }
  else if ( op.second.real() < 0 ){
    ori = '<';
  }
  b.dsply[op.first.imag()][op.first.real()] = ori;
  return b;
}



Layout::Layout(const char c, std::vector<point>& pix):color(c),is_map(true){
  for( point p : pix ){
    pixels->insert( make_pair( p.imag(), p.real() ) );
  }
};
  

Layout::~Layout(){}

/*
  if ( is_map ) {
    pixels->~multimap();
  } else 
    fixed_pt->~point();
  }
}
*/


/*
Layout& Layout::operator=( const Layout& other ){
  swap(*this, other);
  return *this;
}
*/

void Layout::push_back( const point& p ) {

  if ( !is_map ) {
    *fixed_pt = p;

  } else {
    pixels->insert( make_pair(p.imag(), p.real()));
  }
};


bool Layout::contains ( const point& p ) const {

    if ( !is_map ) {
      return *fixed_pt == p;
    } 

    auto r = pixels->equal_range( p.imag() );
    return find_if( r.first, r.second, [&p]( auto v){ 
        return v.second == p.real(); 
    }) != r.second;
};


Frame::Line::Line( const point& o , const Layout& l):offst(o){
  if ( !l.is_map )
  {
    if ( o.imag() == l.fixed_pt->imag() && o.real() < l.fixed_pt->real() )
    {
      line[l.fixed_pt->real() - o.real()]=l.color;
    }
  } 
  else 
  {
    auto r = l.pixels->equal_range(o.imag());
    for_each( r.first, r.second, [&] (pair<row,col> p) { 
        line[p.second - offst.real()]=l.color;
        });
  }
};


Frame::Line::Line( const Line& ol ):offst(ol.offst){
  *this += ol;
}

Frame::Line Frame::Line::operator=( const Line& ol){
  return Line(ol);
}

void Frame::Line::operator+=( const Line& ol ) {
  assert( offst == ol.offst );
  for( pair<col,char> p : ol.line ) line.insert(p);
};




template<size_t h, size_t w>
Buffer<h,w>& operator<<( Buffer<h,w>& buf, const Frame::Line& l ){
  if ( !l.line.size() ) return buf;

  string s( l.line.rbegin()->first+1, Frame::blank ); 
  for( pair<col,char> p : l.line ){
    s[p.first] = p.second; 
  }
  return buf << s;
}


template<size_t h, size_t w>
Buffer<h,w>& operator<<( Buffer<h,w>& buf, const Frame& f ){
  buf.offst=0;
  point o = *f.origin;
  for( size_t i = 0; i < h; i++) {
    Frame::Line ss = accumulate( f.layout.get(), f.layout.get()+f.lc, Frame::Line(o), [&o] (Frame::Line& s, const Layout& ly) {
        s += Frame::Line( o, ly); return s; });
    buf << ss;
    o += 1i;
  }
  return buf;
};


template<size_t height, size_t width>
Buffer<height, width>& operator<<( Buffer<height, width>& b, const pair<const char, point> & p )
{
  b.dsply[p.second.imag()][p.second.real()] = p.first;
  return b;
}

template<size_t _h, size_t _w>
Buffer<_h,_w> & operator<<( Buffer<_h,_w> & b, const Layout & l )
{
  assert( l.is_map );
  point o(0,l.pixels->lower_bound(-500)->first);
  return b << Frame{make_shared<point>(o), make_shared<Layout[]>(l)};
} 


