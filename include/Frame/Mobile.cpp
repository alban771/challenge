
using namespace std;

void Mobile::step( const Point& p ){
  *origin-=p;
}

char Mobile::get_cursor() const {
  return cursor;
}


template<size_t h, size_t w>
Buffer<h,w>& operator<<( Buffer<h,w>& buf, const Mobile& m ){
  buf.offst = *m.pos;
  return buf << string({m.get_cursor()});
};


void MobileOriented::step( const Point& v){
  *origin-=*vel;
  *vel=v;
}

char MobileOriented::get_cursor() const {
  return orntn.at(*vel);
}
