#ifndef MOBILE
#define MOBILE 

#include "Frame.h"

class Mobile {
  
protected:
  const char cursor;

  template<size_t h, size_t w>
  friend Buffer<h,w>& operator<<( Buffer<h,w>&, const Mobile& );

public:
  const std::unique_ptr<Point> pos;
  const std::shared_ptr<Point> origin;

  Mobile( shared_ptr<Point> o,  unique_ptr<Point> p,
      const char c = 'C' ):cursor(c),pos(std::move(p)),origin(o){};


  virtual void step( const Point& );
  virtual char get_cursor() const;
};




class MobileOriented : public Mobile {

  const std::map<Point, char> orntn = {{ NORTH, '^'}, { SOUTH, 'v'}, 
    { EAST, '>'}, { WEST, '<'}, { 0 ,'X'}};


public:
  std::shared_ptr<Point> vel;

  MobileOriented(shared_ptr<Point> o, unique_ptr<Point> p, unique_ptr<Point> v):Mobile(o,std::move(p)), vel(std::move(v)){};

  virtual void step( const Point& );
  virtual char get_cursor() const;
};


#include "../common/Graph/Mobile.cpp"
#endif
