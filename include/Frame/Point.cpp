#include <iostream>
#include <tuple>
#include <cmath>


using namespace std;


/* deprecated : exists in STL */
int pgcd( const int aa, const int bb){
  int a,b,r;
  a = max(abs( aa),abs( bb));
  b = a == aa ? bb : aa;
  if ( b == 0 ) return a;
  while( r=a%b, r>0){ a=b; b=r; }
  return b;
}

void Point::operator+=(  const Point& other){
  r += other.r;
  c += other.c;
}

void Point::operator-=(  const Point& other){
  r -= other.r;
  c -= other.c;
}


void Point::turnLeft(){
  swap(c,r);
  r*=-1;
}

void Point::turnRight(){
  swap(c,r);
  c*=-1;
}

tuple<float, float> Point::radial(){
  float l, th;
  l = sqrt( c*c + r*r );
  if ( c == 0 &&  r == 0 ){ return make_tuple(0,0); }
  th = r > 0 ? 
    c > 0 ? asin( r/l ) : PI - asin( r/l )
    : c > 0 ? 2*PI-asin( -r/l) : PI+asin( -r/l );  
  return make_tuple(l,th);
}

int Point::norme(){
  return abs(c) + abs(r); 
}

Point operator-(const Point& p){
  return Point(-p.c,-p.r);
}

Point operator+( const Point& p1, const Point& p2){
  return Point(p1.c+p2.c, p1.r+p2.r);
}

Point operator-( const Point& p1, const Point& p2){
  return Point(p1.c-p2.c, p1.r-p2.r);
}
/*
Point operator-( const Point& p1, const Point& other){
    int a,b,d;
    a = p1.c - other.c;
    b = p1.r - other.r; 
    d = pgcd(a,b);
    if ( d == 0 ) return Point(0, 0);
    return Point( a/d, b/d);
  }
  */


Point module( const Point& p ){
  return Point( abs( p.c), abs(p.r) );
}

bool operator<( const Point& first, const Point& other){
  return  first.r < other.r && first.c < other.c;
}


bool closerTo( const Point& first, const Point& second, const Point& center){
  Point p1 = first, p2 = second;
  p1 -= center;
  p2 -= center;
  return  p1 < p2;
}

bool Point::operator==( const Point& other) const{
  return r==other.r && c==other.c;
}
bool Point::operator!=( const Point& other) const{
  return ! ( *this == other );
}






ostream& operator<<( ostream& out, const Point& p ){
  out << "(" << p.c << "," << p.r << ")";
  return out;
}


bool operator>>( const shared_ptr<BasicIntCodeIO<int>> cpin, Point& p ){
  bool to;
  p.c = cpin->get( 200, to);
  if ( ! to ) {
    p.r = cpin->get( 200 ,to);
  }
  return !to;
}


