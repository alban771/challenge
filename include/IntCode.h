#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <memory>


#include "IntCode/IntCodeIO.h"


#ifndef INTCODE
#define INTCODE


struct th_d { 
  void operator()(thread * t) { 
    cerr << "thread deleted" << endl;
  };
};

template<typename T = int>
class IntCode{

  int next_operation( auto& i );

  std::vector<T> heap;

  template<typename N>
  friend istream& operator>>( istream&, IntCode<N>&);

  T r_base = 0, t_value = 0;

public:

  std::shared_ptr<BasicIntCodeIO<T>> in, out;
  std::unique_ptr<thread, th_d> compute_th;

  int v_lvl = 0;

  IntCode( std::shared_ptr<BasicIntCodeIO<T>> in = make_shared<BasicIntCodeIO<T>>(), std::shared_ptr<BasicIntCodeIO<T>> out = make_shared<BasicIntCodeIO<T>>() ):in(in),out(out){};

  IntCode( const string& inputFile, std::shared_ptr<BasicIntCodeIO<T>> in = make_shared<BasicIntCodeIO<T>>(), std::shared_ptr<BasicIntCodeIO<T>> out = make_shared<BasicIntCodeIO<T>>() );

  IntCode( const vector<T>&, std::shared_ptr<BasicIntCodeIO<T>> in = make_shared<BasicIntCodeIO<T>>(), std::shared_ptr<BasicIntCodeIO<T>> out = make_shared<BasicIntCodeIO<T>>() );

  IntCode( const IntCode<T>& other, std::shared_ptr<BasicIntCodeIO<T>> in = make_shared<BasicIntCodeIO<T>>(), std::shared_ptr<BasicIntCodeIO<T>> out = make_shared<BasicIntCodeIO<T>>() );

  ~IntCode();

  T get(int) const;

  T get_r_base() const;

  T get_t_value() const;

  int size() const;

  auto begin() const;

  auto end() const;

  bool operator==( IntCode<T>& other ) const;

  void set(int, T);

  void incr_r_base(T);

  void set_t_value(T);

  void join();

  int compute(); 
};


template<typename N>
std::istream & operator>> ( std::istream & in,  IntCode<N>& cp );

#include "IntCode/IntCode.cpp"

#endif
