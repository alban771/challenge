#include <string>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <exception>

#include "IntCode.h"

using namespace std;



template<typename T>
istream & operator>> ( istream & in, IntCode<T>& cp){
  T k;
  while( in >> k, in.ignore() ) cp.heap.push_back(k);
  return in;
}



template<typename T>
IntCode<T>::IntCode( const string& inputFile, shared_ptr<BasicIntCodeIO<T>> in,  shared_ptr<BasicIntCodeIO<T>> out):in(in),out(out){
  ifstream ifs(inputFile);
  ifs >> *this;
};


template<typename T>
IntCode<T>::IntCode( const vector<T>& v, shared_ptr<BasicIntCodeIO<T>> in, shared_ptr<BasicIntCodeIO<T>> out):heap(v.size()),in(in),out(out){
  copy(v.bagin(), v.end(), heap.begin());
}



template<typename T>
IntCode<T>::IntCode( const IntCode<T>& cp,  shared_ptr<BasicIntCodeIO<T>> in,  shared_ptr<BasicIntCodeIO<T>> out):heap(cp.heap),in(in),out(out){}


template<typename T>
IntCode<T>::~IntCode(){
 cerr << "intCode delete" << endl; 
}


template<typename T>
T IntCode<T>::get(int a) const{
  if ( a > size() ){
    throw domain_error("heap overflow");
  }
  return heap[a];
}


template<typename T>
T IntCode<T>::get_t_value() const{
  return t_value;
}


template<typename T>
T IntCode<T>::get_r_base() const{
  return r_base;
}

template<typename T>
int IntCode<T>::size() const{
  return heap.size();
}

template<typename T>
auto IntCode<T>::begin() const{
  return heap.begin();
}


template<typename T>
auto IntCode<T>::end() const{
  return begin() + size();
}

template<typename T>
bool IntCode<T>::operator==(IntCode<T>& other) const{
  return mismatch( begin(), end(), other.begin(), other.end() ).first == end();
}

template<typename T>
void IntCode<T>::incr_r_base(T v) {
  r_base+=v;
}


template<typename T>
void IntCode<T>::set(int a, T v) {
  if (  size() <= a ){
    heap.resize(a+1);
  } 
  heap[a]=v;
}

template<typename T>
void IntCode<T>::set_t_value(T v) {
  t_value=v;
}


template<typename T>
void IntCode<T>::join(){

  if ( compute_th != nullptr && compute_th->joinable()  ) compute_th->join();

}



/* default
 * specializations are found in IntCodeOperation.cpp
 * */
template<typename T, int opcode>
static constexpr auto operation = [] (IntCode<T>& cp, auto& i ) constexpr {
   throw domain_error("unknown op code " + to_string(opcode));
};


#include "IntCodeOperation.cpp"



// TODO : find a better structure than this switch
template<typename T>
int IntCode<T>::next_operation( auto& i){

  try  {
    switch (*i%100) { 
      case 1:
        operation<T,1>(*this, i);
        break;
      case 2:
        operation<T,2>(*this, i);
        break;
      case 3:
        operation<T,3>(*this, i);
        break;
      case 4:
        operation<T,4>(*this, i);
        break;
      case 5:
        operation<T,5>(*this, i);
        break;
      case 6:
        operation<T,6>(*this, i);
        break;
      case 7:
        operation<T,7>(*this, i);
        break;
      case 8:
        operation<T,8>(*this, i);
        break;
      case 9:
        operation<T,9>(*this, i);
        break;
      case 99:
        operation<T,99>(*this, i);
    	return true;
      default:
        operation<T,0>(*this, i);
    }
  } catch(...) {
	return true;
  }
  return false;
}



template<typename T>
int IntCode<T>::compute() {
  
  compute_th = unique_ptr<thread, th_d>( new thread ( [&] () { 
      auto i = begin();
      while ( !next_operation(i) );
    }));
  return t_value;     // ensure compatibility with day_2
}


