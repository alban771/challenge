#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <mutex>
#include <condition_variable>


#ifndef INTCODE_CHANNEL
#define INTCODE_CHANNEL


template<typename T>
class AbstractIntCodeIO {

public: 

  virtual void clear() = 0;

  virtual T get() = 0;
  virtual T get( long timeout, bool& isto ) = 0;

  virtual void put( T val ) = 0;
  virtual void put( T val, long timeout, bool& isto ) = 0;
};




template<typename T>
class BasicIntCodeIO : public AbstractIntCodeIO<T> {
  T val; 
  bool set;
  std::mutex c;
  std::condition_variable cv;

public:

  BasicIntCodeIO():set(false){};
  BasicIntCodeIO(T val):val(val),set(true){};

  void clear();

  T get();
  T get( long timeout, bool& isto );

  void put( T val );
  void put( T val, long timeout, bool& isto );
};


#include "IntCode/IntCodeIO.cpp"
#endif
