//#include <cstdarg>
#include <iterator>
#include <algorithm>

/*  This file holds specializations of IntCode<T>::operation<> template
 * */

using namespace std;



template<typename T, typename... A>
static constexpr void verbose( const IntCode<T>& cp,  int lvl, A&&... a ){
  if ( cp.v_lvl >= lvl ) {
    int dummy[sizeof...(A)] {(cerr << a ,0)...};
    cerr << endl;
  }
}



template<typename T>
void constexpr one_arg( const IntCode<T>&cp,  auto& i, int& modes, T* a){
  /* Determine argument with first digit of modes:
   *  if 0: address mode
   *  if 1: direct mode
   *  if 2: relative mode
   */
  *a = ( modes % 10 ? (modes-1) % 10 ? cp.get( *i + cp.get_r_base() ) : *i : cp.get(*i ));
  modes/=10;
  i++;
};


// ref: https://stackoverflow.com/questions/12515616/expression-contains-unexpanded-parameter-packs/12515637#12515637
template<typename T, typename... A>
int constexpr args( const IntCode<T>& cp,  auto& i, A&... a ){

  int modes = *i/100;
  i++; // go to first arg
  int dummy[sizeof...(A)] {(one_arg(cp, i, modes, &a), 0)...};
  return modes;
}



// C style does not allow zero parameters a.
template<typename T, typename... A>
void constexpr args_set( const IntCode<T>& cp,  auto& i, int& b, A&... a ){
  /* When a result has to be set, last position is returned,
   * instead of the current value of the slot
   */
  const int m = args(cp,  i, a...);
  b = ( m == 2 ) ? cp.get_r_base() + *i : *i;
  i++;
}







template<typename T> 
static constexpr auto operation<T,1> = [] ( IntCode<T>& cp, auto& i ) constexpr{
  T a,b;
  int c;
  args_set(cp, i, c, a, b);
  cp.set(c,a+b);

  verbose(cp,  2, a, " + ", b, " to:", c);   
};


template<typename T>
static constexpr auto operation<T,2> = [] ( IntCode<T>&cp,  auto& i ) constexpr{
  T a,b;
  int c;
  args_set(cp, i, c, a, b);
  cp.set(c,a*b);

  verbose(cp,  2, a, " * ", b, " to:", c);   
};


template<typename T>
static constexpr auto operation<T,3> = [] ( IntCode<T>&cp,  auto& i ) constexpr{
  verbose(cp, 1, "get");

  int a;
  args_set(cp, i, a);
  cp.set(a, cp.in->get());

  //verbose(cp, 2, cp.get(a), " to:", a);
  verbose(cp, 2, a, " to:", a);
};


template<typename T>
static constexpr auto operation<T,4> = [] ( IntCode<T>&cp,  auto& i ) constexpr{

  T a; 
  args(cp, i, a);
  cp.out->put(a);

  verbose(cp, 1, " show:", a);
};


template<typename T>
static constexpr auto operation<T,5> = [] ( IntCode<T>&cp,  auto& i ) constexpr{
  T a, b; 
  args(cp, i, a, b);

  if ( a ) { 
    i = i - (i-cp.begin()) + b; 
  }
  
  verbose(cp, 2, "jumpTo:", b, " if:", a);  
};


template<typename T>
static constexpr auto operation<T,6> = [] ( IntCode<T>&cp,  auto& i ) constexpr{
  T a, b;
  args(cp, i, a, b);

  if ( !a ) { 
    i = i - (i-cp.begin()) + b; 
  }

  verbose(cp, 2, "jumpTo:", b, " ifNot:", a);  
};

template<typename T>
static constexpr auto operation<T,7> = [] ( IntCode<T>&cp,  auto& i) constexpr{
  T a,b;
  int c;
  args_set(cp, i, c, a, b);
  cp.set(c,a < b);

  verbose(cp, 2, a, " < ", b, " result:", cp.get(c), " in:", c);  
};


template<typename T>
static constexpr auto operation<T,8> = [] ( IntCode<T>&cp,  auto& i) constexpr{
  T a,b;
  int c;
  args_set(cp, i, c, a, b);
  cp.set(c,a == b);

  verbose(cp, 2, a, " == ", b, " result:", cp.get(c), " in:", c);  
};
 

template<typename T>
static constexpr auto operation<T,9> = [] ( IntCode<T>&cp,  auto& i) constexpr{
  T a; 
  args(cp, i, a);
  cp.incr_r_base(a);

  verbose(cp, 2,"update r_base with:", a, " res:", cp.get_r_base());  
};
 

template<typename T>
static constexpr auto operation<T,99> = [] ( IntCode<T>&cp,  auto& i) constexpr{
  cp.set_t_value(cp.get(0)); 
  verbose(cp, 1, "terminal value:", cp.get(0));  
};

