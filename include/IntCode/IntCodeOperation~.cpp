#include <cstdarg>
#include <exception>
#include <iterator>
#include <algorithm>

/*  This file holds specializations of IntCode<T>::operation<> template
 * */

using namespace std;




template<typename T, typename... A>
void verbose( const IntCode<T>& cp, int lvl,  A&... a ){
  if ( cp.v_lvl >= lvl ) {
    int dummy[sizeof...(A)] {(cerr << a ,0)...};
    cerr << endl;
  }
}



// ref: https://stackoverflow.com/questions/12515616/expression-contains-unexpanded-parameter-packs/12515637#12515637
template<typename T>
int args( const IntCode<T>& cp, auto& i, T& a... ){

  int modes = *i/100;
  i++; // go to first arg

  va_list aas;
  va_start(aas, a);
  one_arg( i, modes, &a );
  for( T c : aas ) {
    one_arg( cp, i, modes, &c );
  }
  va_end(aas);

  //int dummy[] {(one_arg(i, modes, &a), 0)...};
  return modes;
}



// C style variadic param
template<typename T>
void args_set( const IntCode<T>& cp, auto& i, int& b, T& a...  ){
  /* When a result has to be set, last position is returned,
   * instead of the current value of the slot
   */
  va_list aas;
  va_start(aas, a);
  const int m = args( cp, i, a, aas);
  va_end(aas);
  b = ( m == 2 ) ? cp.get_r_base() + *i : *i;
  i++;
}



template<typename T>
void one_arg( const IntCode<T>& cp, auto& i, int& modes, T* a){
  /* Determine argument with first digit of modes:
   *  if 0: address mode
   *  if 1: direct mode
   *  if 2: relative mode
   */
  *a = ( modes % 10 ? (modes-1) % 10 ? cp.get( *i + cp.get_r_base() ) : *i : cp.get(*i ));
  modes/=10;
  i++;
}


template<typename T, typename N>
void operation( IntCode<T>& cp, N& i ){
  //throw domain_error("unknown op code");
}


template<typename T> 
void operation<int>( IntCode<T>& cp, int& i ){
  T a,b;
  int c;
  args_set(cp, i, c, a, b);
  cp.set(c, a+b);
  //heap[c]=a+b;

  verbose(cp, 2, a, " + ", b, " to:", c);   
}

/*
template<typename T>
void IntCode<T>::operation<2>( auto& i ){
  T a,b;
  int c;
  args_set(i, c, a, b);
  heap[c]=a*b;

  verbose( 2, a, " * ", b, " to:", c);   
}


template<typename T>
void IntCode<T>::operation<3>( auto& i ){
  verbose(1, this, "get");

  int a;
  args_set(i, a);
  heap[a] = in->get();

  verbose(2, this, heap[a], " to:", a);
}


template<typename T>
void IntCode<T>::operation<4>( auto& i ){

  T a; 
  args(i, a);
  out->put(a);

  verbose(1, this, " show:", a);
}


template<typename T>
void IntCode<T>::operation<5>( auto& i ){
  T a, b; 
  args(i, a, b);

  if ( a ) i = heap.begin() + b; 
  
  verbose(2, this, "jumpTo:", b, " if:", a);  
}


template<typename T>
void IntCode<T>::operation<6>( auto& i ){
  T a, b;
  args(i, a, b);

  if ( !a ) i = heap.begin() + b; 

  verbose(2, this, "jumpTo:", b, " ifNot:", a);  
}

template<typename T>
void IntCode<T>::operation<7>( auto& i){
  T a,b;
  int c;
  args_set(i, c, a, b);
  heap[c] = a < b;

  verbose(2, this, a, " < ", b, " result:", heap[c], " in:", c);  
}


template<typename T>
void IntCode<T>::operation<8>( auto& i){
  T a,b;
  int c;
  args_set(i, c, a, b);
  heap[c] = a == b;

  verbose(2, this, a, " == ", b, " result:", heap[c], " in:", c);  
}
 
template<typename T>
void IntCode<T>::operation<9>( auto& i){
  T a; 
  args(i, a);
  r_base+=a;

  verbose(2, this,"update r_base with:", a, " res:", r_base);  
}
 

template<typename T>
void IntCode<T>::operation<99>( auto& i){
  t_value = heap[0]; 
  verbose(1, this, "terminal value:", t);  
}
*/

