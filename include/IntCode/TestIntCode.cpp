#include <cassert>
#include <fstream>
#include <memory>
#include <thread>

#include "IntCode.h"


static const string AoC_dir="/home/me/workspace/C++/adventofcode/";

#define EXAMPLES_D2 {1,0,0,0,99}, {2,3,0,3,99}, {2,4,4,5,99,0}, {1,1,1,4,99,5,6,0,99},{1,9,10,3,2,3,11,0,99,30,40,50} 
#define EXAMPLES_FINISH_D2 {2,0,0,0,99}, {2,3,0,6,99}, {2,4,4,5,99,9801}, {30,1,1,4,2,5,6,0,99}, {3500,9,10,70,2,3,11,0,99,30,40,50} 



const void test_d2(){

  static const vector<vector<int>> eg { EXAMPLES_D2 };
  static const vector<vector<int>> eg_finish { EXAMPLES_FINISH_D2 };

  for( size_t i = 0; i < eg.size(); i++ ){

    IntCode<int> cp = (vector<int>) eg[i];
    IntCode<int> cp_finish = (vector<int>) eg_finish[i];
    cp.compute();
    cp.join();
    cp_finish.join();
    assert( cp == cp_finish );
  }

  cout << "test day-2 ok" << endl;
}



#define EXAMPLES_D5_1 {3,9,8,9,10,9,4,9,99,-1,8}, {3,9,7,9,10,9,4,9,99,-1,8}, {3,3,1108,-1,8,3,4,3,99}, {3,3,1107,-1,8,3,4,3,99}, {3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99}
#define EXAMPLES_D5_1_INPUT {7},{8},{9} 
#define EXAMPLES_D5_1_OUTPUT {0,1,0},{1,0,0},{0,1,0},{1,0,0},{999,1000,1001}


#define EXAMPLES_D5_2 {3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9}, {3,3,1105,-1,9,1101,0,0,12,4,12,99,1} 
#define EXAMPLES_D5_2_INPUT {0},{5} 
#define EXAMPLES_D5_2_OUTPUT {0,1},{0,1}


const void test_d5(const vector<vector<int>>& eg, const vector<vector<int>>& in, const vector<vector<int>>& out){


  for( size_t i = 0; i < eg.size(); i++ ){
    for( size_t j = 0; j < out[i].size(); j++){
      IntCode<int> cp(eg[i]);
      cp.compute();
      cp.in->put(in[j][0]);
      assert( cp.out->get() == out[i][j] );
    }
  }

  cout << "test1 day-5 ok" << endl;
}

#define EG_DAY_9_QUINE 109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99
#define EG_DAY_9_LL1 1102,34915192,34915192,7,4,7,99,0
#define EG_DAY_9_LL2 104,1125899906842624,99

const void test_d9(){

  static const vector<int> Quine { EG_DAY_9_QUINE }; 

  { 
    IntCode q(Quine);
    q.compute();
    for( int v : Quine ) {
      assert( q.out->get() == v );
    }
  }

  static const vector<long long> ll1 { EG_DAY_9_LL1 }; 
  static const vector<long long> ll2 { EG_DAY_9_LL2 }; 

  {
    IntCode cp1(ll1), cp2(ll2);
    cp1.compute();
    cp2.compute();
    assert(  1LL << 49 < cp1.out->get() );
    assert( ll2[1] == cp2.out->get() );
  }
}


int main(){

  
  test_d2();

  test_d5({ EXAMPLES_D5_1 }, { EXAMPLES_D5_1_INPUT }, { EXAMPLES_D5_1_OUTPUT });

  test_d5({ EXAMPLES_D5_2 }, { EXAMPLES_D5_2_INPUT }, { EXAMPLES_D5_2_OUTPUT });
  
  test_d9();


}

