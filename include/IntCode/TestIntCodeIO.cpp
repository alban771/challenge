#include <exception>
#include <cassert>
#include <thread>
#include "IntCodeIO.h"


#define TIMEOUT 100 //ms

typedef unsigned int type_test;  



void test_channel_put( AbstractIntCodeIO<type_test> & c ) {
   
  static const type_test a = 12, b = 41;
  c.clear();

  // standard put and get
  c.put( a );
  assert( c.get() == a );


  // data race 
  c.put( a );
  thread t1 = thread( [&] () { c.put(b); });
  thread t2 = thread( [&] () { c.put(b); });

  assert( c.get() == a );
  assert( c.get() == b );

  const type_test bb = c.get();

  t1.join();
  t2.join();
  assert( bb == b );


  // timeout 
  bool to = false;
  c.put( a );
  t1 = thread( [&] () { c.put( b, TIMEOUT, to); } );
  t1.join();
  assert(to);
  assert( c.get() == a );
}



void test_channel_get( AbstractIntCodeIO<type_test> & c){
  
  static const type_test a = 27, b = 8;
  c.clear();

  // standard put and get
  c.put( a );
  assert( c.get() == a );


  // data race 
  type_test a1 = b, a2 = b;
  thread t1 = thread( [&] () { a1 = c.get(); });
  thread t2 = thread( [&] () { a2 = c.get(); });

  assert( a1 == b && a2 == b);

  c.put( a );
  c.put( a );

  t1.join();
  t2.join();

  assert( a1 == a && a2 == a);


  // timeout 
  bool to = false;
  t1 = thread( [&] () { c.get( TIMEOUT, to); } );
  t1.join();
  assert( to );
}




int main() {
  
  static const type_test a=5, b=11;
  BasicIntCodeIO<type_test> c, d(a), e(b);

  test_channel_get(c);
  test_channel_put(c);

  test_channel_get(d);
  test_channel_put(d);

  assert(e.get() == b );


  cout << "unit test intcode io ok." << endl;
}
