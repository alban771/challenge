#include <fstream>
#include <cassert>
#include <memory>
#include <thread>
#include "IntCode.h"


typedef  unsigned int type_test;


string AoC_dir="/home/me/workspace/C++/AdventOfCode/";



class TestIntCodeOperation : public IntCode<type_test> {

    BasicIntCodeIO< pair<int, type_test> > err;

  public:

    TestIntCodeOperation():IntCode(){};

    template<int opcode>
    void test_op(string inputFile, vector<pair<int, type_test>> expected, vector<type_test> feed = {}) {

      ifstream ifs(inputFile);
      ifs >> *this;

      compute_th = thread( [&] () { 
        auto j = feed.begin();
        auto i = heap.begin();
        auto diff = begin();
        vector<type_test> before;
          do {
            while ( *i%100 == opcode ) {
              before = heap;
              next_operation(i);
              diff = mismatch( heap.begin(), heap.end(), before.begin());
              err.put( make_pair(diff - heap.begin(), *diff) );
            }
            if ( *i%100 == 3 ) {
              assert( feed.size() > 0 );
              if ( j == feed.end() ) j = feed.begin; 
              in->put( *j++ );
            }
            if ( *i%100 == 4 ) {
              out->clear();
            }
          } while ( !next_operation(i) ); 
        });


      for ( type_test e : expected ){
        assert( err.get().second == e  );
      }
    }
};




int main(){

  TestIntCodeOperation cp_test;

  return 0;
}

