#include <iostream>

#include "IntCodeOperation.cpp"

using namespace std;




template<typename T>
class IntCode{

  static constexpr Op<T,1> op1 {}; 



public:
 
  void show(int i = 0);

};





template<typename T>
void IntCode<T>::show(int i){
  if ( i == 1 )
    op1.show();
  else if ( i == 2 )
    Op_l<T,2>();
  else 
    Op<T,0>().show();
}





int main(){

  IntCode<int> cp;

  cp.show(2);
}

