#include <chrono>
#include "IntCodeIO.h"


using namespace std;


template<typename T>
void BasicIntCodeIO<T>::clear(){
  lock_guard<mutex> lck(c);
  if ( set ) {
    set = false;
    cv.notify_one();
  }
}




template<typename T>
T BasicIntCodeIO<T>::get(){

  unique_lock<mutex> lck(c);
  cv.wait(lck, [&] { return set; });
  set=false;
  cv.notify_one();
  return val;
}



template<typename T>
T BasicIntCodeIO<T>::get( long mstimeout, bool& isto){
  
  unique_lock<mutex> lck(c);
  isto = !cv.wait_for(lck, chrono::milliseconds(mstimeout) , [&] { return set; });
  if ( set ){
    set=false;
    cv.notify_one();
    return val;
  }
  return 0;
}



template<typename T>
void BasicIntCodeIO<T>::put(T v){
  unique_lock<mutex> lck(c);
  cv.wait(lck, [&] { return !set; });
  val=v;
  set=true;
  cv.notify_one();
}



template<typename T>
void BasicIntCodeIO<T>::put(T v, long mstimeout, bool& isto){
  unique_lock<mutex> lck(c);
  cv.wait_for(lck, chrono::milliseconds(mstimeout), [&] { return !set; });
  if ( !set ){
    val=v;
    set=true;
    cv.notify_one();
  } 
  else isto = true;
}
