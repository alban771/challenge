using namespace std;


template<typename T, int opcode>
struct Op {
  static void show(){
    cerr << 0;
  }
};



template<typename T>
struct Op<T,1> {

  constexpr Op(){};

  static constexpr void show(){
    cerr << 1;
  }
};



template<typename T, int opcode>
static constexpr auto Op_l = [] () { cerr << 0; };


template<typename T>
static constexpr auto Op_l<T,2> = [] () { cerr << 2; };





//template<typename T>
//static const Op<T,1> op1;



