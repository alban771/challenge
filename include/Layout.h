#ifndef LAYOUT
#define LAYOUT

#include "PointAoc.h"

#include <map>
#include <memory>
#include <iostream>
#include <string>
#include <numeric>



typedef const int row;
typedef int col;
typedef std::multimap<row, col> pixel_map;
typedef std::pair<point,point> segment;



static constexpr auto pixtopoint( const char ortn, const int height ){ 
    return [ height, ortn ]( const std::pair<col,row> & p ){
      return ( ortn == 'h'? 
          point(p.second, height-1-p.first):
          point(p.first, height-1-p.second) ); 
    };
}


static constexpr auto pointtopix( const char ortn, const int height ){ 
    return [ height, ortn ]( const point & p ) -> std::pair<row,col> {
      return ( ortn == 'h'? 
          std::make_pair(height - 1 - p.imag(), p.real()):
          std::make_pair(p.real(), height - 1 - p.imag()));
    };
}





class Layout
{
  enum orientation : char { H = 'h', V = 'v' } ortn = Layout::orientation::H;
  std::string colors;
  std::map<char, std::shared_ptr<pixel_map>> map;


public:
  int height, width;

  Layout( const char * );

  Layout( const Layout & );

  Layout mirror() const;


  bool operator<=(const Layout &) const;
  bool operator==(const Layout &) const;

  friend std::istream & operator>>( std::istream &, Layout & ); 
  friend std::ostream & operator<<( std::ostream &, const Layout & ); 


  bool contains( const point & ) const;
  bool contains( const point &, const char ) const;
  bool contains( const point &, const std::string & ) const;

  void insert( const std::vector<point> &);
  void insert( const std::vector<point> &, const char c);
  void insert( const point &);
  void insert( const point &, const char c );

  void erase( const std::vector<point> &);
  void erase( const std::vector<point> &, const char c);
  void erase( const point &);
  void erase( const point &, const char c);

  std::size_t size(const char) const;
  std::size_t nr_lines() const;

  std::vector<point> operator[]( const char ) const;
  std::vector<point> operator[]( const std::string & ) const;

  std::shared_ptr<pixel_map> get( const char ) const;

  std::vector<point> get_line( const int i, const char ) const;
  std::vector<point> get_line( const int i, const std::string & ) const;
  std::vector<segment> get_line_segments( const int i, const std::string & ) const;
  
};


// day-18
std::vector<point> detect_angle( const Layout &, const std::string & );


// ref : https://stackoverflow.com/questions/3773378/passing-const-char-as-template-argument
struct TextArg { const char * text; };


template<const TextArg &char_color>
class FixeLayout : public Layout
{
public:
  FixeLayout();
};



using namespace std;

template<const TextArg &char_color>
FixeLayout<char_color>::FixeLayout():Layout(char_color.text){}


Layout::Layout(const char * col):height(0),width(0)
{
  colors = string( col );
  for ( char c : colors )
  {
    map.emplace( c, make_shared<pixel_map>() );
  }
}

Layout::Layout( const Layout & l ):height(l.height),width(l.width),ortn(l.ortn)
{
  colors = l.colors;
  for ( auto pr : l.map )
  {
    map[pr.first] = pr.second;
  }
}

Layout Layout::mirror() const
{
  string _s = ""; 
  _s = accumulate( map.begin(), map.end(), _s,
        []( string s, const auto & pr ){
          s.push_back(pr.first);
          return s;
        });

  Layout l( _s.c_str() );

  const auto mirror_pm = [&l] ( const pair< const char, shared_ptr<pixel_map>> & pm )
  {
    const char c = pm.first;
    for_each( pm.second->begin(), pm.second->end(), 
        [&l,&c]( const pair<col,row> & px ){
          l.map[c]->emplace(px.second, px.first);
        });
  };

  for_each( map.begin(), map.end(), mirror_pm );

  l.ortn = Layout::orientation::V;
  l.height = height;
  l.width = width;

  return l;
}



//istream & load( std::istream & in, Layout<char_color> & l )
istream & operator>>( std::istream & in, Layout & l )
{
  string line;
  l.height=0; 
  while ( getline(in, line) )
  {
    for ( int i = 0; i < line.size(); ++i )
    {
      auto f = l.map.find( line[i] ); 
      if ( f != l.map.end() )
      {
        f->second->emplace(l.height, i);
        l.width = max( i+1, l.width );
      }
    }
    ++l.height;
  }
  return in;
}




ostream & operator<<( ostream & out, const Layout & l )
{
  string line;
  int i = -1;
  const auto fill = [&line, &i, &l]( const char c ) 
  {
    auto r = l.map.at(c)->equal_range(i);
    for_each(r.first, r.second, [&](const auto & pix)
    {
      line[pix.second] = c;
    });
  };

  while ( ++i < l.height )
  {
    line = string(l.width, ' ');
    for_each( l.colors.begin(), l.colors.end(), fill ); 
    out << line << endl;
  }
  return out;
}



bool Layout::contains( const point & p ) const
{ 
  return contains(p, colors );
}

bool Layout::contains( const point &p, const string & s) const
{
  return any_of(s.begin(), s.end(), [&](const char c)
  {
    return contains(p, c);
  });
}

bool Layout::contains( const point & p, const char c ) const
{
  auto r = map.at(c)->equal_range( height - 1 - p.imag());
  return find(r.first, r.second, pointtopix(ortn,height)(p)) 
    != r.second;
}


void Layout::insert( const vector<point> & vp )
{
  insert(vp, colors[0]);
}

void Layout::insert( const vector<point> & vp, const char c )
{
  for( point p : vp ) insert(p, c);
}

void Layout::insert( const point & p )
{
  insert(p, colors[0]);
}

void Layout::insert( const point & p, const char c )
{
  map[c]->insert( pointtopix(ortn,height)(p) );
  width = max(width, (int)p.real() + 1);
}


void Layout::erase( const vector<point> & vp ) 
{
  erase( vp, colors[0] );
}

void Layout::erase( const vector<point> & vp, const char c ) 
{
  for( point p : vp ) erase(p, c);
}

void Layout::erase( const point & p ) 
{
  erase( p, colors[0] );
}

void Layout::erase( const point & p, const char c ) 
{
  auto r = map[c]->equal_range(p.imag());
  map[c]->erase(find(r.first, r.second, pointtopix(ortn,height)(p)));
}



std::size_t Layout::size( char c ) const 
{
  return map.at(c)->size();
}


static const auto extend_vect = []( vector<point> v1, vector<point> v2 )
{ 
  v1.resize(v1.size() + v2.size());
  copy(v2.begin(), v2.end(), v1.end() - v2.size());
  return v1;
};


vector<point> Layout::operator[]( const string & s ) const
{
  return accumulate( s.begin(), s.end(), vector<point>{}, 
      [&](vector<point> s, const char c) {
        return extend_vect(s, this->operator[](c));
      });
}
   
vector<point> Layout::operator[]( const char c ) const
{
  vector<point> s( map.at(c)->size() );
  transform( map.at(c)->begin(), map.at(c)->end(), s.begin(), pixtopoint(ortn, height) );
  return s;
}


shared_ptr<pixel_map> Layout::get( const char c ) const
{
  return map.at(c);
}


vector<point> Layout::get_line( const int i, const string & s ) const
{
  return accumulate( s.begin(), s.end(), vector<point>{}, 
      [&](vector<point> s, const char c){
        return extend_vect(s, get_line(i, c));
      });
}


vector<point> Layout::get_line( const int i, const char c) const
{
  int lnr = ortn == 'h' ? height - 1 - i : i;
  auto r = map.at(c)->equal_range(lnr);
  vector<point> _li(map.at(c)->count(lnr));
  transform( r.first, r.second, _li.begin(), pixtopoint(ortn, height) ); 
  return _li;
}


vector<segment> Layout::get_line_segments( const int i, const string & s ) const
{
  
  vector<point> _li = get_line(i, s), _ld(_li.size());
  sort(_li.begin(), _li.end(), ortn == 'h' ? lesser_h : lesser_v );
  adjacent_difference(_li.begin(), _li.end(), _ld.begin() );

  vector<segment> seg;
  int seg_b=0;
  for ( int seg_e = 0; seg_e <=_li.size(); ++seg_e )
  {
    if ( seg_e == _li.size() || _ld[seg_e].real() + _ld[seg_e].imag() > 1 )
    {
      if ( seg_b < seg_e-1 )
        seg.emplace( seg.end(), _li[seg_b], _li[seg_e-1] );
      seg_b = seg_e;
    }
  }

  return seg;
}

size_t Layout::nr_lines() const 
{
  return ortn == 'h' ? height : width;
}


vector<point> detect_angle( const Layout & l, const string & s )
{
  vector<point> dirs { NORTH, SOUTH, EAST, WEST };
  vector<point> p = l[s];
  p.erase( remove_if(p.begin(), p.end(), 
        [&l, &s, &dirs]( const point & _p) 
        {
          return  ( ! l.contains(_p + NORTH, s)   
                &&  ! l.contains(_p + SOUTH, s) )
                || 
                  ( ! l.contains(_p + EAST, s)  
                &&  ! l.contains(_p + WEST, s) );
        }), p.end());
  return p;
};


bool Layout::operator<=( const Layout & other ) const 
{

  const auto covered = [&] ( const pair<row,col> & pr ){
    return other.contains( pixtopoint(ortn,height)(pr) );
  };

  return all_of( map.begin(), map.end(), 
      [&covered]( const auto & pr ){
        return all_of( pr.second->begin(), pr.second->end(), covered );
      });
}


bool Layout::operator==( const Layout & other ) const
{ 
  return operator<=(other) && other <= *this;
}




#endif



