#ifndef POINT_AOC
#define POINT_AOC


#include <complex>


typedef std::complex<double> point; 
typedef std::complex<double> direction; 
typedef std::pair<point, direction> oriented_point;

constexpr direction NORTH = direction( 0, 1 ); 
constexpr direction SOUTH = direction( 0, -1 ); 
constexpr direction EAST = direction( 1, 0 ); 
constexpr direction WEST = direction( -1, 0 ); 

constexpr double Pi = 3.14159;
constexpr double epsilon = pow( 10, -5);


constexpr auto lesser_h = []( const point & p1, const point & p2 )
{
  return p1.imag() < p2.imag() || 
    ( p1.imag() == p2.imag() && p1.real() < p2.real() );
};


constexpr auto lesser_v = []( const point & p1, const point & p2 )
{
  return p1.real() < p2.real() || 
    ( p1.real() == p2.real() && p1.imag() < p2.imag() );
};


struct less_comp {
  virtual bool operator()( const point & p1, const point & p2 ) const = 0;
};

template<char ortn>
struct less_comp_ornted : less_comp {
  bool operator()( const point & p1, const point & p2 ) const {
    return ortn == 'h' ? lesser_h(p1, p2) : lesser_v(p1, p2);
  }
};




#endif
