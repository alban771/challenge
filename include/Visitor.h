#ifndef MAZE_VISITOR
#define MAZE_VISITOR

#include "Layout.h"
#include <optional>

template<class maze_t, class derivated>
class Visitor;



// Note : public inheritance is needed and 
// allow sharing pointer of derivated classes
template<class maze_derive_t, class visitor_derive_t>
class Maze 
{

public:

  // adapt state of the maze
  // raw pointer for Visitor polymorphism
  virtual void accept( const Visitor<maze_derive_t, visitor_derive_t> * ){} 
  
};


static int max_score = 0;
static int ind = 0;


// Idea : facilitate exploration 
// by considering submaze
// by depth-first algorithm
//
template<class maze_t, class derivated>
class Visitor
{


protected:
  std::shared_ptr<maze_t> maze;

public:
  // pass one maze. 
  Visitor( std::shared_ptr<maze_t> m ):maze(m){}
  Visitor( const Visitor<maze_t, derivated> & v ):maze( v.maze ){}

  // exploration algorithm 
  virtual void visit(); 

  // show accomplished path 
  virtual string show_path() const = 0;

  // how good was this way 
  virtual int score() const = 0;


protected:
  // found the exit
  virtual bool success() const = 0; 

  // choose one path, progress and 
  // return the Visitor immediatly following
  // or nullptr if no other choice   
  virtual std::optional<derivated> step() = 0; 

  // perform the step
  virtual void go_to() = 0;

  virtual void swap( derivated & ) = 0;
};


// TODO : not inherited
namespace std 
{
  template<class maze_t, class visitor_derive_t>
  struct less<Visitor<maze_t, visitor_derive_t>>
  {
    bool operator()( const Visitor<maze_t, visitor_derive_t> & v1, const Visitor<maze_t, visitor_derive_t> & v2 )
    {
      return v1.score() < v2.score();
    }
  };
}



template<class maze_t, class derivated>
void Visitor<maze_t, derivated>::visit()
{
  ++ind;
  maze->accept(this); // get back to state saved in this Visitor 

  list<derivated> next_visitor;

  const auto push_nv = [&next_visitor] ( const optional<derivated>&& s ){
    if ( s.has_value() )
      next_visitor.push_back( s.value() );
  };

  while ( !success() )
  { 
    /*
    optional<derivated> s = step(); 
    if ( s.has_value()  ) 
      next_visitor.push_back( s.value() );
      */

    push_nv( step() );
    go_to();
  }

  //cerr << "success" << endl;

  for_each( next_visitor.rbegin(), next_visitor.rend(), 
      []( derivated & v1 ){
          v1.visit();
      });

  if (  next_visitor.size() )
  {

    const auto mv = min_element( next_visitor.begin(), next_visitor.end() );

    if ( score() > mv->score() )
      swap( *mv ); 
  }

  if ( max_score <= 0 || max_score > score() )
  {
    max_score = score();
    cout << "score:" << score() << "  ind:" << ind << endl;
    cout << "path:" << show_path() << endl; 
  }

}





#endif
