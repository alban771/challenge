#include <algorithm>
#include <iostream>
#include "Dixon.h"



int main(){

  Crible<50,1> c;

  std::cerr << "primes under 50: ["; 
  std::copy( c.prime_base.begin(), c.prime_base.end(), std::ostream_iterator<int>(std::cerr, ", ") );
  std::cerr << "]" << std::endl;

}
