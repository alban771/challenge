#include <iostream>
#include <cstdio>
#include <iterator>
#include <algorithm>
#include <vector>
#include "Perm.h"



using namespace std;

Perm::Perm( vector<int> *v ):pos(v->begin()),v(v){
  Perm P(v, v->begin());
  p = P.p;
  i = P.i;
  P.~Perm();
}

Perm::Perm( vector<int> *v, vector<int>::iterator pp ):v(v),pos(pp){
    i = (v->end() - pos) - 1;
    if ( i > 0 ){
      p = new Perm(v, ++pp);
    }
}

int factorial(int n){
  return n == 0 ? 1 : n * factorial(n-1);
}

int Perm::card(){
  return (i * factorial( v->end() - pos - 1 )) + (i>0 ? p->card():1);
}


bool Perm::next_perm(){
    
    // base
    if ( v->end() - pos == 1 ){
      return false;
    }

    // case every sub-permutations were performed. action on this level.
    if ( !( p->next_perm() ) ){
      
      int n = v->end() - pos;

      if ( n == 2 ){
        if ( i ){
          iter_swap( pos, --v->end() );

        } else {
          return false;
        }
      } else {

      // invariant:   
      //
      // (*)
      // before any permutation:         [ xn, xn-1, ... x2, x1 ]
      // position after permutations:    [ xn-1, xn, ... x2, x1 ]
      //
      // recursive algorithm ( n >= 3 ):
      //   if P(n-1) verifies (*)  
      //   define P(n) : 
      //       initial state as in (*)
      //       P(n-1) + xn <-> xn-2 : [xn-2, xn, xn-1, ... x2, x1]
      //          (A)
      //       for k from n-2 to 2: 
      //          with a = n if  
      //          P(n-1) + xk <-> xk-1 : [xk-1, xa, xb, xn-2, ..., xk, xk-2, ..., x1]
      //                                 where | xa, xb = xn, xn-1 if n-k % 2 == 1
      //                                       | xa, xb = xn-1, xn otherwise 
      //          (B)
      //       [ x1, xa, xb, xn-2, ..., x2] 
      //                          where | xa, xb = xn, xn-1 if n % 2 == 1
      //                                | xa, xb = xn-1, xn otherwise 
      //       P(n-1) 
      //          + ( xn <-> xn-1 ) if n % 2 == 0 
      //          + ( x1 <-> xn-1 )    
      //          (C)
      //       P(n-1)
      //       [ xn-1, xn, x1, xn-2, ..., x2] 
      //          + shift_left( [ x1, xn-2 ..., x2 ] )
      //          (D)
    
        vector<int>::iterator pos1 = pos, pos2 = pos; 
        p->~Perm();
        p = new Perm(v, ++pos1);

        // first swap (A)
        if ( i == n-1 ){
          iter_swap(pos, pos1 );

        // swap (C)
        } else if ( i == 1 ) {
          if ( n % 2 == 0 ){
            iter_swap(pos1, pos2+2); 
          }
          iter_swap(pos, pos2+1);
        
        // shift (D)
        } else if ( !i ) {
          rotate(++pos1, pos2+3, v->end());
          return false;

        // swap (B)
        } else {
          iter_swap(pos, pos1+ (n-i) );
        } 

      }
      i--;
      
    }

    return true;
}


ostream& operator<<(ostream& out, Perm* p){
  out << "permutation: (";
  copy( p->v->begin(), --p->v->end(), ostream_iterator<int>( out, ", " )); 
  out << *p->v->rbegin() << ")" << endl;
  return out;
}



#ifndef MAIN
#define MAIN
#define N 7

void main2(){

  vector<int> v(N);
  int a=0;

  generate( v.begin(), v.end(), [&a] () ->int { return a++; });

  Perm p(&v);


  vector<vector<int>> l(p.card()); 
  l[0]=v;
 

  //vector<int> debug(N);
  //copy(p.v->begin(), p.v->end(), debug.begin());
  //copy(debug.begin(), debug.end(), ostream_iterator<int>( cerr, " ")); 

  generate( l.begin()+1, l.end(), [&p] () ->vector<int> { 
      //copy(p.v->begin(), p.v->end(), debug.begin());
      p.next_perm();
      return *(p.v);
    });

  auto i = l.begin();
  bool s = find_if(l.begin(), l.end(), [&l, &i] (vector<int>& a) -> bool 
      {
        auto a2 = find_if( ++i, l.end(), [&a] (vector<int>& b) -> bool 
        {
          return equal( b.begin(), b.end(), a.begin() );
        });
        bool k = a2 == l.end();
        
        if ( !k ) {

          cerr << "nok " << endl;
          cerr << "i1:" << i - l.begin()  << endl; 
          cerr << "i2:" << a2 - l.begin()  << endl; 

          for ( auto b = i; b != a2; b++){
            cerr << "a: [";
            copy( b->begin(), b->end(), ostream_iterator<int>(cerr, ","));
            cerr << "]" << endl; 
          }
        }
        return !k;

      }) == l.end();


  cout << ( !s ? "some permutations are identical" : "permutations are all differents") << endl;
}


#endif


