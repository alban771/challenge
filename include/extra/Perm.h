#include <iostream>
#include <vector>




class Perm {

  const std::vector<int>::iterator pos;
  Perm *p;

public:
  int i; 
  std::vector<int> *v;

  Perm( std::vector<int> *v);
  Perm( std::vector<int> *v, std::vector<int>::iterator pp );


  bool next_perm();     

  int card();
};

std::ostream& operator<<( std::ostream& out , Perm* p);


