#define BOOST_TEST_MODULE Test Layout
#include <boost/test/included/unit_test.hpp>

#include "Layout.h"
#include <sstream>
#include <iostream>

using namespace std;

const inline string eg1 = "#########\n"
                          "#b.A.@ a#\n"
                          "#### ####";


static constexpr TextArg wall = { "#" };

                          
BOOST_AUTO_TEST_CASE(brick_wall)
{
  istringstream iss(eg1);
  FixeLayout<wall> l;
  iss >> l;

  BOOST_TEST( l.height == 3 ); 
  
  for ( int i = 0; i < 9; ++i )  
  {
    BOOST_TEST( ( i == 4 || l.contains(point(i,0))) ); 
    BOOST_TEST( l.contains(point(i,2)) );
  }
  for ( int i = 1; i < 7; ++ i )
  {
    BOOST_TEST( !l.contains(point(i,1)) );
  }
}


static constexpr TextArg alpha = { "aA@" };


BOOST_AUTO_TEST_CASE(alphabet)
{
  istringstream iss(eg1);
  FixeLayout<alpha> l;
  iss >> l;

  BOOST_TEST( l['a'].size() == 1 & l.size('A') == 1 );
  BOOST_TEST( l['@'][0] == point(5,1) );
}

static constexpr TextArg line = { ".aA@b" };

BOOST_AUTO_TEST_CASE(get_line)
{
  istringstream iss(eg1);
  FixeLayout<line> l;
  iss >> l;

  BOOST_TEST( l.get_line(1,".aA@").size() == 5 );
  BOOST_TEST( l.get_line_segments(1,".a@b").size() == 2 );
}


BOOST_AUTO_TEST_CASE(mirror)
{
  istringstream iss(eg1);
  FixeLayout<line> l;
  iss >> l;
  Layout lm = l.mirror();


  vector<point> n = l[line.text], m = lm[line.text];
  /*
  cerr << m.size() << endl;
  copy( m.begin(), m.end(), ostream_iterator<point>(cerr, " ") );
  */

  auto mis = mismatch(n.begin(), n.end(), m.begin(), m.end());
  BOOST_TEST( (mis.first == n.end()) & (mis.second == m.end()) );
  BOOST_TEST( lm.get_line(3,'A').size() == 1 );
  BOOST_TEST( lm.get_line(3,'A')[0] == point(3,1) );
}



BOOST_AUTO_TEST_CASE(angle)
{
  istringstream iss(eg1);
  FixeLayout<wall> l;
  iss >> l;

  vector<point> a = detect_angle(l, "#");
  /*
  cerr << a.size() << endl;
  copy( a.begin(), a.end(), ostream_iterator<point>(cerr, " ") );
  */

  BOOST_TEST( a.size() = 4 );
  BOOST_TEST( ( find(a.begin(), a.end(), point(0,2)) != a.end() ) );
}


BOOST_AUTO_TEST_CASE(equality)
{
  istringstream iss1(eg1), iss2(eg1);
  FixeLayout<alpha> la;
  FixeLayout<line> ll;
  iss1 >> la;
  iss2 >> ll;

  cerr << la;
  cerr << ll;

  BOOST_TEST( ( la <= ll  && !( ll <= la )));

  vector<point> vdiff { {1,1}, {2,1}, {4,1} };
  la.insert(vdiff);
  //copy(vdiff.begin(), vdiff.end(), ostream_iterator<point>(cerr));
  BOOST_TEST( la == ll );
}
