DAY=$(shell basename $$(pwd))

CC=g++

CFLAGS?=-Wall -std=c++17 -Wno-reorder -Wno-sign-compare -fdiagnostics-color=always $(INCLUDE) $(GLD_FLAG)
DEBUG_FLAGS?=-g -O0 -fkeep-inline-functions -fsanitize=address
INTCODE_FLAGS?=-pthread -Wno-unused-variable -fconcepts 
SPEED_FLAGS?=-O2
GLD_FLAG?=1
HEAD=20


INCLUDE?=-I../include
FILES?=$(DAY).cpp
INPUT?=



gold: GLD_FLAG=-DGOLD
gold: $(DAY).gld.o gold-pre 
	@./$< 2>/dev/null 
	$(gold-post)


silver: GLD_FLAG=
silver: $(DAY).slv.o silver-pre
	@./$< 2>/dev/null 
	$(silver-post)


ifneq (,$(GLD_FLAG))
#ifdef GLD_FLAG
.DEFAULT_GOAL=gold
else
.DEFAULT_GOAL=silver
endif



gold-pre:
	@printf "\\nresult for $(DAY) gold puzzle:\\n\\n"

silver-pre:
	@printf "\\nresult for $(DAY) silver puzzle:\\n"


define gold-post
	@printf "\\n\\nto see silver use: make silver.\\n"
endef

define silver-post
	@printf "\\n\\nto see gold use: make [gold].\\n"
endef


time:
	time -f "\n time to perform : %E seconds" 


# Following target-specific var does not propagate to their parent 
#
#speed: 		CFLAGS+=$(SPEED_FLAGS)	
#intcode:  	CFLAGS+=$(INTCODE_FLAGS)
#debug: 		CFLAGS+=$(DEBUG_FALGS)	
#.gld: 		CFLAGS+=$(GLD_FLAGS)	

$(DAY)%.o: $(FILES) 
	$(CC) $(CFLAGS) -o $@ $(FILES) 2>&1 | head -n$(HEAD) 


.PHONY: clean


clean: 
	rm -f *.o

