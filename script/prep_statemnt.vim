let s:spoil_start="<details>"
let s:spoil_end="</details>"
let s:spoil_summary='<summary>spoil: Part two</summary>'



function! IsPreformatted() abort 
	let line = getline('.')  

	let avoid = []
	call add(avoid, '^\b*$')	"blank line
	call add(avoid, '</\?[a-z]\+>')	"balise
	call add(avoid, '-{3}$')	"title
	
	for a in avoid
		if line =~ a 
			return 0
		endif
	endfor

	"traditional scheme
	if line =~# '^[ _|.#@ABCDO]\+$'
		return 1
	else 
		"with unique letters (day-18)
		let letters = filter(sort(split( line, '\zs' )), 'v:val =~? "[a-z]"')	
		return len( letters ) == len(uniq( letters ))
	endif
endfunction


function! PrepareStatement() 
	"title
	%s/^---/#/

	"hide spoil P2
	if search("details>") == 0 
		call append( search("Part Two")-1,[s:spoil_start, s:spoil_summary])
		call append("$",s:spoil_end)
	endif

	"highlight result
	for i in range(2)
		call setline( '.', "<code>" . getline(search("answer")). "</code>" ) 
		+1
	endfor


	
	"format schemas
	let b:prefmtd = 0
	function! PreformattedLineAdapter() abort
		if xor( IsPreformatted(), b:prefmtd ) 
			let b:prefmtd = ! b:prefmtd 
			if b:prefmtd 
				let z = append(line('.')-1, '<pre>')
				+1
			else
				let z = append(line('.')-1, '</pre>')
				+1
			endif
		endif
	endfunction

	" leave lines unhandled when appending
	"%call PreformattedLineAdapter()
	0
	while line('.') < line('$')
		call PreformattedLineAdapter()
		+1
	endwhile
		
	

endfunction

